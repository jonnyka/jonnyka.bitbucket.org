<?php
header("Access-Control-Allow-Origin: *");

require_once('db.php');

$q = mysqli_query($mysql, "SELECT * FROM sglol");

$return = array();
while ($r = mysqli_fetch_assoc($q)) {
    $return[$r['sgname']] = array(
        'id' => $r['id'],
        'server' => $r['server'],
        'summoner' => $r['summoner'],
        'tier' => $r['tier'],
        'division' => $r['division'],
        'point' => $r['point'],
        'win' => $r['win'],
        'loss' => $r['loss']
    );
}

echo json_encode($return);

?> 