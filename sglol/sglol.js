function setSetting(name, value) {
    var testObject = { 'value': value };
    localStorage.setItem(name, JSON.stringify(testObject));
}

function getSetting(name) {
    var retrievedObject = localStorage.getItem(name);
    if (!retrievedObject) {
        return 1;
    }

    var jsonObject = JSON.parse(retrievedObject);
    return jsonObject.value;
}

var baseUrl = '//sg.hu/chat/sglol/';

var head = document.getElementsByTagName('head')[0];
var link = document.createElement('link');
link.id = 'sglolcss';
link.rel = 'stylesheet';
link.type = 'text/css';
link.href = baseUrl + 'sglol.css';//?rand=' + Math.random();
link.media = 'all';
head.appendChild(link);

var url = baseUrl + 'sglol.php';

$.getJSON(url, function(data, textStatus, xhr) {
    if (xhr.status == 200) {
        $('header a.name').each(function() {
            var dis = $(this),
                t = dis.html().trim(),
                name = t;
            if (t.indexOf('img') >= 0) {
                name = $(t).attr('title');
            }
            name = name.replace(' - VIP', '');

            if (name in data) {
                var tier = data[name]['tier'],
                    division = data[name]['division'],
                    point = data[name]['point'],
                    server = data[name]['server'],
                    summoner = data[name]['summoner'],
                    win = data[name]['win'],
                    loss = data[name]['loss'],
                    li = dis.parent(),
                    userLink = 'http://' + server + '.op.gg/summoner/userName=' + summoner.replace(/ /g, '+');

                li.next('.body').prepend('<span class="leagueInfoBox ' + tier + '"><span class="win">' + win + ' w</span><span class="loss">' + loss + ' l</span><span class="reward">' + tier + ' ' + division + '</span></span>');
                li.next('.body').append('<div style="clear: both;"></div>');

                var borders = '';
                if (getSetting('sgLolBorder') == 1) {
                    borders = 'postWrap' + tier;
                }
                li.parent().wrap('<div id="postWrap' + tier + '" class="postWrap ' + borders + '"></div>');

                //var userLink = 'http://www.lolking.net/summoner/' + server + '/' + summId;
                dis.after('<a class="summonerInfo" href="' + userLink + '" target="_blank"><span>' + summoner + ' - ' + server + '</span></a>');
            }
        });
    }
});

var openClose = 'sgLolBorderOpen';
if (getSetting('sgLolBorder') == 1) {
    openClose = 'sgLolBorderClose';
}
$('#forum-posts-list').prepend('<a class="btn btn-sm btn-default ' + openClose + '" id="sgLolBorder">Keretek</a><div style="clear: both;"></div>');

var sgLolBorder = $('#sgLolBorder');
sgLolBorder.click(function() {
    if (getSetting('sgLolBorder') == 1) {
        $('.postWrap').each(function() {
            $(this).removeClass();
            $(this).addClass('postWrap');
            sgLolBorder.removeClass('sgLolBorderClose');
            sgLolBorder.addClass('sgLolBorderOpen');
        });

        setSetting('sgLolBorder', 0);
    }
    else {
        $('.postWrap').each(function() {
            $(this).addClass($(this).attr('id'));
            sgLolBorder.removeClass('sgLolBorderOpen');
            sgLolBorder.addClass('sgLolBorderClose');
        });

        setSetting('sgLolBorder', 1);
    }
});

/*
var statusUrl = baseUrl + 'sglol_status.php';

$.getJSON(statusUrl, function(data, textStatus, xhr) {
    if (xhr.status == 200) {
        console.log(data);
    }
});
*/