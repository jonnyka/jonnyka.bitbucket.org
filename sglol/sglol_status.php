<?php

header("Access-Control-Allow-Origin: *");

$return = array(
    'eune' => array(),
    'euw' => array(),
);

$shards = array('eune', 'euw');

foreach ($shards as $shard) {
    $ch = curl_init();
    if (!$ch) {
        echo ('cURL para');
        die("Couldn't initialize a cURL handle");
    }
    //$ret = curl_setopt($ch, CURLOPT_HEADER,         1);
    //$ret = curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    $ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $ret = curl_setopt($ch, CURLOPT_TIMEOUT,        30);

    $url = 'http://status.leagueoflegends.com/shards/' . $shard;
    $ret = curl_setopt($ch, CURLOPT_URL, $url);

    $ret = curl_exec($ch);

    if (empty($ret)) {
        curl_close($ch);
        echo(curl_error($ch));
    }
    else {
        $info = curl_getinfo($ch);
        //echo $shard . ' - ' . $info['http_code'] . '<br />';

        if (empty($info['http_code'])) {
            die("No HTTP code was returned");
        }
        else {
            if ($info['http_code'] == 200) {
                $data = json_decode($ret, true);
                $return[$shard] = $data;
            }
        }

        curl_close($ch);
    }
}

echo json_encode($return);

?> 