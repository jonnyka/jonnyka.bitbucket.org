//TODO fix háttérnek goNb
//TODO linkeknek opció, vagy külön blokk?
//TODO a:hover megoldás...

var time = new Date().getTime();
var head = document.getElementsByTagName('head')[0];
var baseUrl = 'http://beta.linedesign.hu/localstorage/';

var link = document.createElement('link');
link.id = 'CustomCSS';
link.rel = 'stylesheet';
link.type = 'text/css';
link.href = baseUrl + 'cssmaker_main.css?' + time;
link.media = 'all';
head.appendChild(link);

$('head').append('<style type="text/css" id="css_maker_style"></style>');
var style = document.getElementById('css_maker_style');

var separator = '<div class="separator"></div>';

var body_style_set = 0;
var forumbg_style_set = 0;
var banner_style_set = 0;
var textarea_style_set = 0;
var smallButton_style_set = 0;
var smallButton_style_set_hover = 0;
var smallButton_js_set = 0;
var hsz_style_set = 0;
var hszheader_style_set = 0;
var usernevbefore_style_set = 0;
var userlogodel_js_set = 0;
var hsztxt_style_set = 0;
var hszfoot_style_set = 0;

function css_maker() {
    var topic_inf = $('#topic-information');
    $('body').append('<aside id="menu_container" class="show_custom_menu"></aside>');
    //$('nav#breadcrumb #topic-panel').prepend('<button class="btn btn-danger" id="custom_clear">clr</button>');
    $('nav#breadcrumb #topic-panel').prepend('<button class="btn btn-success" id="custom_menu">menü</button>');
    $('nav#breadcrumb #topic-panel').prepend('<button class="btn btn-warning" id="custom_kodok">kódok</button>');
    $('nav#breadcrumb #topic-panel').prepend('<button class="btn btn-info" id="custom_info">info</button>');
    

    $('#menu_container').append('<div class="designmaker_container"></div>');
    var container = $('.designmaker_container');

    container.append('<header class="main_title">Custom Design</header>');
    container.append('<div class="body_css_container"></div>');
    container.append('<div class="banner_css_container"></div>');
    container.append('<div class="textarea_css_container"></div>');
    container.append('<div class="smallButton_css_container"></div>');
    container.append('<div class="hsz_css_container"></div>');
    container.append('<div class="hszheader_css_container"></div>');
    container.append('<div class="hsztxt_css_container"></div>');
    container.append('<div class="hszfoot_css_container"></div>');
    
    topic_inf.append('<div id="custom_code_container" class="show_custom_code"></div>');
    
    var custom_code_container = $('#custom_code_container');
    custom_code_container.append('<div class="css_container"></div>');
    custom_code_container.append('<div class="js_container"></div>');
    if(localStorage.getItem('kuldes_class-smallButton') || localStorage.getItem('kuldes_fel-smallButton') || localStorage.getItem('userlogodel')){
        $('.js_container').append('<p id="js_set">$(document).ready(function(){<br><span class="tab"></span>});</p>');
    }
    
    var body_css_container = $('.body_css_container');
    body_css_container.append('<header class="tab-head">Body</header>')
    body_css_container.append('<section id="body_set" class="sets hide"></section>');
    
    var banner_css_container = $('.banner_css_container');
    banner_css_container.append('<header class="tab-head">Banner</header>')
    banner_css_container.append('<section id="banner_set" class="sets hide"></section>');

    var textarea_css_container = $('.textarea_css_container');
    textarea_css_container.append('<header class="tab-head">Textarea</header>')
    textarea_css_container.append('<section id="textarea_set" class="sets hide"></section>');

    var smallButton_css_container = $('.smallButton_css_container');
    smallButton_css_container.append('<header class="tab-head">Gombok</header>')
    smallButton_css_container.append('<section id="smallButton_set" class="sets hide"></section>');

    var hsz_css_container = $('.hsz_css_container');
    hsz_css_container.append('<header class="tab-head">HSZ</header>')
    hsz_css_container.append('<section id="hsz_set" class="sets hide"></section>');

    var hszheader_css_container = $('.hszheader_css_container');
    hszheader_css_container.append('<header class="tab-head">HSZ fejléc</header>')
    hszheader_css_container.append('<section id="hszheader_set" class="sets hide"></section>');

    var hsztxt_css_container = $('.hsztxt_css_container');
    hsztxt_css_container.append('<header class="tab-head">HSZ szöveg</header>')
    hsztxt_css_container.append('<section id="hsztxt_set" class="sets hide"></section>');

    var hszfoot_css_container = $('.hszfoot_css_container');
    hszfoot_css_container.append('<header class="tab-head">HSZ lábléc</header>')
    hszfoot_css_container.append('<section id="hszfoot_set" class="sets hide"></section>');


////////////
    //BODY//////
////////////
    //ami egyszerű, azt loopoljuk (valid css névvel)
    var body_css_values = ['background-color', 'padding'];
    for (var i = 0; i < body_css_values.length; i++) {
        var bcv = body_css_values[i];
        $('section#body_set').append('<div id="' + bcv + '_body_form"></div>');
        $("#" + bcv + "_body_form").append('<input id="' + bcv + '_set" name="' + bcv + '_set" type="text" placeholder=' + bcv + ' /><button id="' + bcv + '_add" class="set-clear addset">+</button><button id="' + bcv + '_clr" class="set-clear hide delset">-</button>');

        if (localStorage.getItem(bcv + "-body")) {
            var body_css = document.createTextNode("body{" + bcv + ": " + localStorage.getItem(bcv + "-body") + ";}");
            style.appendChild(body_css);
            $('#' + bcv + '_body_form #' + bcv + '_set').attr('disabled', true);
            $('#' + bcv + '_body_form #' + bcv + '_add').addClass("hide");
            $('#' + bcv + '_body_form #' + bcv + '_clr').removeClass("hide");
            $('#' + bcv + '_body_form #' + bcv + '_clr').addClass("show");
            body_style_set = 1;
        }

        var addgoNb = $('#' + bcv + '_body_form #' + bcv + '_add');
        addgoNb.attr('keyname', bcv);
        addgoNb.attr('ii', 'body');


        var delgoNb = $('#' + bcv + '_body_form #' + bcv + '_clr');
        delgoNb.attr('delname', bcv);
        delgoNb.attr('ii', 'body');
    }

    //ami nem megoldható loop-al, azt egyesével
    //background-image
    $('#background-color_body_form').after('<div id="bgurl_body_form"></div>');
    $('#bgurl_body_form').append('<input id="bgurl_set" name="bgurl_set" type="text" placeholder="background-image" /><button id="bgurl_add" class="set-clear addset">+</button><button id="bgurl_clr" class="set-clear hide delset">-</button>');
    $('#bgurl_body_form #bgurl_add').attr('keyname', 'bgurl');
    $('#bgurl_body_form #bgurl_add').attr('ii', 'body');
    $('#bgurl_body_form #bgurl_clr').attr('delname', 'bgurl');
    $('#bgurl_body_form #bgurl_clr').attr('ii', 'body');

    if (localStorage.getItem("bgurl-body")) {
        var body_css = document.createTextNode("body{background-image: url(" + localStorage.getItem("bgurl-body") + ");}");
        style.appendChild(body_css);
        $('#bgurl_body_form #bgurl_set').attr('disabled', true);
        $('#bgurl_body_form #bgurl_add').addClass('hide');
        $('#bgurl_body_form #bgurl_clr').removeClass('hide');
        $('#bgurl_body_form #bgurl_clr').addClass('show');
        body_style_set = 1;
        console.log(banner_style_set);
    }

    //background-image repeat
    $('#bgurl_body_form').after('<div id="bgrepeat_body_form"></div>');
    $('#bgrepeat_body_form').append('<select id="bgrepeat_set" name="bgrepeat_set" size="1"></select><button id="bgrepeat_add" class="set-clear addset">+</button><button id="bgrepeat_clr" class="set-clear hide delset">-</button>');
    $('#bgrepeat_set').append('<option value="empty" selected="selected" disabled="disabled">background-repeat</option>');
    $('#bgrepeat_set').append('<option value="no-repeat">no-repeat</option>');
    $('#bgrepeat_set').append('<option value="repeat">repeat</option>');
    $('#bgrepeat_set').append('<option value="repeat-x">repeat-x</option>');
    $('#bgrepeat_set').append('<option value="repeat-y">repeat-y</option>');
    $('#bgrepeat_body_form #bgrepeat_add').attr('keyname', 'bgrepeat');
    $('#bgrepeat_body_form #bgrepeat_add').attr('ii', 'body');
    $('#bgrepeat_body_form #bgrepeat_clr').attr('delname', 'bgrepeat');
    $('#bgrepeat_body_form #bgrepeat_clr').attr('ii', 'body');

    if (localStorage.getItem("bgrepeat-body")) {
        var body_css = document.createTextNode("body{background-repeat: " + localStorage.getItem("bgrepeat-body") + ";}");
        style.appendChild(body_css);
        $('#bgrepeat_body_form #bgrepeat_set').attr('disabled', true);
        $('#bgrepeat_body_form #bgrepeat_add').addClass('hide');
        $('#bgrepeat_body_form #bgrepeat_clr').removeClass('hide');
        $('#bgrepeat_body_form #bgrepeat_clr').addClass('show');
        body_style_set = 1;
    }

    //background-position
    $('#bgrepeat_body_form').after('<div id="bgpos_body_form"></div>');
    $('#bgpos_body_form').append('<select id="bgpos_set" name="bgpos_set" size="1"></select><button id="bgpos_add" class="set-clear addset">+</button><button id="bgpos_clr" class="set-clear hide delset">-</button>');
    $('#bgpos_set').append('<option value="empty" selected="selected" disabled="disabled">background-position</option>');
    $('#bgpos_set').append('<option value="left top">left top</option>');
    $('#bgpos_set').append('<option value="left center">left center</option>');
    $('#bgpos_set').append('<option value="left bottom">left bottom</option>');
    $('#bgpos_set').append('<option value="right top">right top</option>');
    $('#bgpos_set').append('<option value="right center">right center</option>');
    $('#bgpos_set').append('<option value="right bottom">right bottom</option>');
    $('#bgpos_set').append('<option value="center top">center top</option>');
    $('#bgpos_set').append('<option value="center center">center center</option>');
    $('#bgpos_set').append('<option value="center bottom">center bottom</option>');

    $('#bgpos_body_form #bgpos_add').attr('keyname', 'bgpos');
    $('#bgpos_body_form #bgpos_add').attr('ii', 'body');
    $('#bgpos_body_form #bgpos_clr').attr('delname', 'bgpos');
    $('#bgpos_body_form #bgpos_clr').attr('ii', 'body');

    if (localStorage.getItem("bgpos-body")) {
        var body_css = document.createTextNode("body{background-position: " + localStorage.getItem("bgpos-body") + ";}");
        style.appendChild(body_css);
        $('#bgpos_body_form #bgpos_set').attr('disabled', true);
        $('#bgpos_body_form #bgpos_add').addClass('hide');
        $('#bgpos_body_form #bgpos_clr').removeClass('hide');
        $('#bgpos_body_form #bgpos_clr').addClass('show');
        body_style_set = 1;
    }

    //fórum háttérszín
    $('section#body_set').append(separator);
    $('section#body_set').append('<div><p class="sub-tab">Fórum háttérszín</p></div>');
    $('section#body_set').append('<div id="forumbg_body_form"></div>');
    $('#forumbg_body_form').append('<input id="forumbg_set" name="forumbg_set" type="text" placeholder="background-color" /><button id="forumbg_add" class="set-clear addset">+</button><button id="forumbg_clr" class="set-clear hide delset">-</button>');
    $('#forumbg_body_form #forumbg_add').attr('keyname', 'forumbg');
    $('#forumbg_body_form #forumbg_add').attr('ii', 'body');
    $('#forumbg_body_form #forumbg_clr').attr('delname', 'forumbg');
    $('#forumbg_body_form #forumbg_clr').attr('ii', 'body');

    if (localStorage.getItem("forumbg-body")) {
        var body_css = document.createTextNode("#menu-family, #content{background-color: " + localStorage.getItem("forumbg-body") + ";}");
        style.appendChild(body_css);
        $('#forumbg_body_form #forumbg_set').attr('disabled', true);
        $('#forumbg_body_form #forumbg_add').addClass('hide');
        $('#forumbg_body_form #forumbg_clr').removeClass('hide');
        $('#forumbg_body_form #forumbg_clr').addClass('show');
        forumbg_style_set = 1;
    }

    //behúzzuk stílusokat, és kiírjuk
    if (body_style_set === 1){
        var body_style_set_txt = '<p id="body_style_set">body{<br><span class="tab"></span>}</p>';
        $('.css_container').append(body_style_set_txt);

        //loopolt stílus
        for (var i = 0; i < body_css_values.length; i++) {
            var bcv = body_css_values[i];
            if (localStorage.getItem(bcv + '-body')){
               $('#body_style_set span').append(bcv + ': ' + localStorage.getItem(bcv + '-body') + ';');
            }
        }

        //egyéb stílusok aminek nem lehet loopolni
        //bg-url
        if (localStorage.getItem('bgurl-body')){
                $('#body_style_set span').append('background-image: ' + localStorage.getItem('bgurl-body') + ';');   
            }
        //bg-repeat
        if (localStorage.getItem('bgrepeat-body')){
                $('#body_style_set span').append('background-repeat: ' + localStorage.getItem('bgrepeat-body') + ';');   
            }
        //bg-pos
        if (localStorage.getItem('bgpos-body')){
                $('#body_style_set span').append('background-position: ' + localStorage.getItem('bgpos-body') + ';');   
            }

        $('.css_container #body_style_set span').each(function() {
            $(this).html($(this).html().replace(/;/g, ';\n'));
        });
    }
    
    //fórum háttérszín
    if (forumbg_style_set === 1){
        var forumbg_style_set_txt = '<p id="forumbg_style_set">#menu-family,<br>#content{<br><span class="tab"></span>}</p>';
        $('.css_container').append(forumbg_style_set_txt);

        
        if (localStorage.getItem('forumbg-body')){
                $('#forumbg_style_set span').append('background-color: ' + localStorage.getItem('forumbg-body') + ';');   
            }

        $('.css_container #body_style_set span').each(function() {
            $(this).html($(this).html().replace(/;/g, ';\n'));
        });
    }


////////////
    //BANNER//////
////////////

    var banner_css_values = ['background-color','padding', 'width', 'height', 'color', 'border', 'border-top', 'border-right', 'border-bottom', 'border-left', 'border-radius'];
    for (var i = 0; i < banner_css_values.length; i++) {
        var bacv = banner_css_values[i];
        $('section#banner_set').append('<div id="' + bacv + '_banner_form"></div>');
        $("#" + bacv + "_banner_form").append('<input id="' + bacv + '_set" name="' + bacv + '_set" type="text" placeholder=' + bacv + ' /><button id="' + bacv + '_add" class="set-clear addset">+</button><button id="' + bacv + '_clr" class="set-clear hide delset">-</button>');

        if (localStorage.getItem(bacv + "-banner")) {
            var banner_css = document.createTextNode("#topic-information{" + bacv + ": " + localStorage.getItem(bacv + "-banner") + ";}");
            style.appendChild(banner_css);
            $('#' + bacv + '_banner_form #' + bacv + '_set').attr('disabled', true);
            $('#' + bacv + '_banner_form #' + bacv + '_add').addClass("hide");
            $('#' + bacv + '_banner_form #' + bacv + '_clr').removeClass("hide");
            $('#' + bacv + '_banner_form #' + bacv + '_clr').addClass("show");
            banner_style_set = 1;
        }

        var addgoNb = $('#' + bacv + '_banner_form #' + bacv + '_add');
        addgoNb.attr('keyname', bacv);
        addgoNb.attr('ii', 'banner');

        var delgoNb = $('#' + bacv + '_banner_form #' + bacv + '_clr');
        delgoNb.attr('delname', bacv);
        delgoNb.attr('ii', 'banner');
    }
    
    //behúzzuk stílusokat, és kiírjuk
    if (banner_style_set === 1){
        var banner_style_set_txt = '<p id="banner_style_set">#topic-information{<br><span class="tab"></span>}</p>';
        $('.css_container').append(banner_style_set_txt);

        for (var i = 0; i < banner_css_values.length; i++) {
            var bacv = banner_css_values[i];
            if (localStorage.getItem(bacv + '-banner')){
               $('#banner_style_set span').append(bacv + ': ' + localStorage.getItem(bacv + '-banner') + ';');
            }
        }

        $('.css_container #banner_style_set span').each(function() {
            $(this).html($(this).html().replace(/;/g, ';\n'));
        });
    }
    
////////////
    //TEXTAREA//////
////////////

    //ami egyszerű, azt loopoljuk (valid css névvel)
    var textarea_css_values = ['background-color', 'padding', 'margin', 'color', 'border', 'border-top', 'border-right', 'border-bottom', 'border-left', 'border-radius'];
    for (var i = 0; i < textarea_css_values.length; i++) {
        var tacv = textarea_css_values[i];
        $('section#textarea_set').append('<div id="' + tacv + '_textarea_form"></div>');
        $("#" + tacv + "_textarea_form").append('<input id="' + tacv + '_set" name="' + tacv + '_set" type="text" placeholder=' + tacv + ' /><button id="' + tacv + '_add" class="set-clear addset">+</button><button id="' + tacv + '_clr" class="set-clear hide delset">-</button>');

        //!important beállítása
        $('#background-color_textarea_form #background-color_set').attr('imp', 'imp');
        $('#border_textarea_form #border_set').attr('imp', 'imp');

        if (localStorage.getItem(tacv + "-textarea")) {
            var textarea_css = document.createTextNode("textarea{" + tacv + ": " + localStorage.getItem(tacv + "-textarea") + ";}");
            style.appendChild(textarea_css);
            $('#' + tacv + '_textarea_form #' + tacv + '_set').attr('disabled', true);
            $('#' + tacv + '_textarea_form #' + tacv + '_add').addClass("hide");
            $('#' + tacv + '_textarea_form #' + tacv + '_clr').removeClass("hide");
            $('#' + tacv + '_textarea_form #' + tacv + '_clr').addClass("show");
            textarea_style_set = 1;
        }

        var addgoNb = $('#' + tacv + '_textarea_form #' + tacv + '_add');
        addgoNb.attr('keyname', tacv);
        addgoNb.attr('ii', 'textarea');


        var delgoNb = $('#' + tacv + '_textarea_form #' + tacv + '_clr');
        delgoNb.attr('delname', tacv);
        delgoNb.attr('ii', 'textarea');
    }

    //ami nem megoldható loop-al, azt egyesével
    //background-image
    $('#background-color_textarea_form').after('<div id="bgurl_textarea_form"></div>');
    $('#bgurl_textarea_form').append('<input id="bgurl_set" name="bgurl_set" type="text" placeholder="background-image" /><button id="bgurl_add" class="set-clear addset">+</button><button id="bgurl_clr" class="set-clear hide delset">-</button>');
    $('#bgurl_textarea_form #bgurl_add').attr('keyname', 'bgurl');
    $('#bgurl_textarea_form #bgurl_add').attr('ii', 'textarea');
    $('#bgurl_textarea_form #bgurl_clr').attr('delname', 'bgurl');
    $('#bgurl_textarea_form #bgurl_clr').attr('ii', 'body');

    if (localStorage.getItem("bgurl-textarea")) {
        var textarea_css = document.createTextNode("textarea{background-image: url(" + localStorage.getItem("bgurl-textarea") + ");}");
        style.appendChild(textarea_css);
        $('#bgurl_textareay_form #bgurl_set').attr('disabled', true);
        $('#bgurl_textarea_form #bgurl_add').addClass('hide');
        $('#bgurl_textarea_form #bgurl_clr').removeClass('hide');
        $('#bgurl_textarea_form #bgurl_clr').addClass('show');
        textarea_style_set = 1;
    }

    //background-image repeat
    $('#bgurl_textarea_form').after('<div id="bgrepeat_textarea_form"></div>');
    $('#bgrepeat_textarea_form').append('<select id="bgrepeat_textarea_set" name="bgrepeat_set" size="1"></select><button id="bgrepeat_add" class="set-clear addset">+</button><button id="bgrepeat_clr" class="set-clear hide delset">-</button>');
    $('#bgrepeat_textarea_set').append('<option value="empty" selected="selected" disabled="disabled">background-repeat</option>');
    $('#bgrepeat_textarea_set').append('<option value="no-repeat">no-repeat</option>');
    $('#bgrepeat_textarea_set').append('<option value="repeat">repeat</option>');
    $('#bgrepeat_textarea_set').append('<option value="repeat-x">repeat-x</option>');
    $('#bgrepeat_textarea_set').append('<option value="repeat-y">repeat-y</option>');
    $('#bgrepeat_textarea_form #bgrepeat_add').attr('keyname', 'bgrepeat');
    $('#bgrepeat_textarea_form #bgrepeat_add').attr('ii', 'textarea');
    $('#bgrepeat_textarea_form #bgrepeat_clr').attr('delname', 'bgrepeat');
    $('#bgrepeat_textarea_form #bgrepeat_clr').attr('ii', 'textarea');

    if (localStorage.getItem("bgrepeat-textarea")) {
        var textarea_css = document.createTextNode("textarea{background-repeat: " + localStorage.getItem("bgrepeat-textarea") + ";}");
        style.appendChild(textareay_css);
        $('#bgrepeat_textarea_form #bgrepeat_set').attr('disabled', true);
        $('#bgrepeat_textarea_form #bgrepeat_add').addClass('hide');
        $('#bgrepeat_textarea_form #bgrepeat_clr').removeClass('hide');
        $('#bgrepeat_textarea_form #bgrepeat_clr').addClass('show');
        textarea_style_set = 1;
    }

    //background-position
    $('#bgrepeat_textarea_form').after('<div id="bgpos_textarea_form"></div>');
    $('#bgpos_textarea_form').append('<select id="bgpos_textarea_set" name="bgpos_set" size="1"></select><button id="bgpos_add" class="set-clear addset">+</button><button id="bgpos_clr" class="set-clear hide delset">-</button>');
    $('#bgpos_textarea_set').append('<option value="empty" selected="selected" disabled="disabled">background-position</option>');
    $('#bgpos_textarea_set').append('<option value="left top">left top</option>');
    $('#bgpos_textarea_set').append('<option value="left center">left center</option>');
    $('#bgpos_textarea_set').append('<option value="left bottom">left bottom</option>');
    $('#bgpos_textarea_set').append('<option value="right top">right top</option>');
    $('#bgpos_textarea_set').append('<option value="right center">right center</option>');
    $('#bgpos_textarea_set').append('<option value="right bottom">right bottom</option>');
    $('#bgpos_textarea_set').append('<option value="center top">center top</option>');
    $('#bgpos_textarea_set').append('<option value="center center">center center</option>');
    $('#bgpos_textarea_set').append('<option value="center bottom">center bottom</option>');

    $('#bgpos_textarea_form #bgpos_add').attr('keyname', 'bgpos');
    $('#bgpos_textarea_form #bgpos_add').attr('ii', 'textarea');
    $('#bgpos_textarea_form #bgpos_clr').attr('delname', 'bgpos');
    $('#bgpos_textarea_form #bgpos_clr').attr('ii', 'textarea');

    if (localStorage.getItem("bgpos-textarea")) {
        var textarea_css = document.createTextNode("textarea{background-position: " + localStorage.getItem("bgpos-textarea") + ";}");
        style.appendChild(textarea_css);
        $('#bgpos_textarea_form #bgpos_set').attr('disabled', true);
        $('#bgpos_textarea_form #bgpos_add').addClass('hide');
        $('#bgpos_textarea_form #bgpos_clr').removeClass('hide');
        $('#bgpos_textarea_form #bgpos_clr').addClass('show');
        textarea_style_set = 1;
    }

    //behúzzuk stílusokat, és kiírjuk
    if (textarea_style_set === 1){
        var textarea_style_set_txt = '<p id="textarea_style_set">textarea{<br><span class="tab"></span>}</p>';
        $('.css_container').append(textarea_style_set_txt);

        //loopolt stiusok
        for (var i = 0; i < textarea_css_values.length; i++) {
            var tacv = textarea_css_values[i];
            if (localStorage.getItem(tacv + '-textarea')){
               $('#textarea_style_set span').append(tacv + ': ' + localStorage.getItem(tacv + '-textarea') + ';');
            }
        }


        //egyéb stílusok aminek nem lehet loopolni
        //bg-url
        if (localStorage.getItem('bgurl-textarea')){
                $('#textarea_style_set span').append('background-image: ' + localStorage.getItem('bgurl-textarea') + ';');   
            }
        //bg-repeat
        if (localStorage.getItem('bgrepeat-textarea')){
                $('#textarea_style_set span').append('background-repeat: ' + localStorage.getItem('bgrepeat-textarea') + ';');   
            }
        //bg-pos
        if (localStorage.getItem('bgpos-textarea')){
                $('#textarea_style_set span').append('background-position: ' + localStorage.getItem('bgpos-textarea') + ';');   
            }

        $('.css_container #textarea_style_set span').each(function() {
            $(this).html($(this).html().replace(/;/g, ';\n'));
        });
    }

////////////
    //smallButton//////
////////////

    //ami egyszerű, azt loopoljuk (valid css névvel)
    var smallButton_css_values = ['background-color', 'font-size', 'line-height', 'width', 'height', 'padding', 'margin', 'color', 'border', 'border-top', 'border-right', 'border-bottom', 'border-left', 'border-radius'];
    for (var i = 0; i < smallButton_css_values.length; i++) {
        var sBcv = smallButton_css_values[i];
        $('section#smallButton_set').append('<div id="' + sBcv + '_smallButton_form"></div>');
        $("#" + sBcv + "_smallButton_form").append('<input id="' + sBcv + '_set" name="' + sBcv + '_set" type="text" placeholder=' + sBcv + ' /><button id="' + sBcv + '_add" class="set-clear addset">+</button><button id="' + sBcv + '_clr" class="set-clear hide delset">-</button>');

        if (localStorage.getItem(sBcv + "-smallButton")) {
            var smallButton_css = document.createTextNode("#forum-codes button.smallButton{" + sBcv + ": " + localStorage.getItem(sBcv + "-smallButton") + ";}");
            style.appendChild(smallButton_css);
            if (!localStorage.getItem('hover_smallButton')) {
                $('#' + sBcv + '_smallButton_form #' + sBcv + '_set').attr('disabled', true);
                $('#' + sBcv + '_smallButton_form #' + sBcv + '_add').addClass("hide");
                $('#' + sBcv + '_smallButton_form #' + sBcv + '_clr').removeClass("hide");
                $('#' + sBcv + '_smallButton_form #' + sBcv + '_clr').addClass("show");
            }
            smallButton_style_set = 1;
        }
        
        if (localStorage.getItem(sBcv + "-smallButton-hover")) {
            var smallButton_css = document.createTextNode("#forum-codes button.smallButton:hover{" + sBcv + ": " + localStorage.getItem(sBcv + "-smallButton-hover") + ";}");
            style.appendChild(smallButton_css);
            if (localStorage.getItem('hover_smallButton')) {
                $('#' + sBcv + '_smallButton_form #' + sBcv + '_set').attr('disabled', true);
                $('#' + sBcv + '_smallButton_form #' + sBcv + '_add').addClass("hide");
                $('#' + sBcv + '_smallButton_form #' + sBcv + '_clr').removeClass("hide");
                $('#' + sBcv + '_smallButton_form #' + sBcv + '_clr').addClass("show");
            }
            smallButton_style_set_hover = 1;
        }

        var addgoNb = $('#' + sBcv + '_smallButton_form #' + sBcv + '_add');
        addgoNb.attr('keyname', sBcv);
        addgoNb.attr('ii', 'smallButton');


        var delgoNb = $('#' + sBcv + '_smallButton_form #' + sBcv + '_clr');
        delgoNb.attr('delname', sBcv);
        delgoNb.attr('ii', 'smallButton');
    }

    //text-transform
    $('#font-size_smallButton_form').after('<div id="txt_trans_smallButton_form"></div>');
    $('#txt_trans_smallButton_form').append('<select id="txt_trans_set" name="txt_trans_set" size="1"></select><button id="txt_trans_add" class="set-clear addset">+</button><button id="txt_trans_clr" class="set-clear hide delset">-</button>');
    $('#txt_trans_smallButton_form #txt_trans_set').append('<option value="empty" selected="selected" disabled="disabled">text-transform</option>');
    $('#txt_trans_smallButton_form #txt_trans_set').append('<option value="none">none</option>');
    $('#txt_trans_smallButton_form #txt_trans_set').append('<option value="capitalize">Capitalize</option>');
    $('#txt_trans_smallButton_form #txt_trans_set').append('<option value="lowercase">lowercase</option>');
    $('#txt_trans_smallButton_form #txt_trans_set').append('<option value="uppercase">UPPERCASE</option>');

    $('#txt_trans_smallButton_form #txt_trans_add').attr('keyname', 'txt_trans');
    $('#txt_trans_smallButton_form #txt_trans_add').attr('ii', 'smallButton');
    $('#txt_trans_smallButton_form #txt_trans_clr').attr('delname', 'txt_trans');
    $('#txt_trans_smallButton_form #txt_trans_clr').attr('ii', 'smallButton');

    if (localStorage.getItem("txt_trans-smallButton")) {
        var smallButton_css = document.createTextNode("#forum-codes button.smallButton{text-transform: " + localStorage.getItem("txt_trans-smallButton") + ";}");
        style.appendChild(smallButton_css);
        if (!localStorage.getItem('hover_smallButton')) {
            $('#txt_trans_smallButton_form #txt_trans_set').attr('disabled', true);
            $('#txt_trans_smallButton_form #txt_trans_add').addClass('hide');
            $('#txt_trans_smallButton_form #txt_trans_clr').removeClass('hide');
            $('#txt_trans_smallButton_form #txt_trans_clr').addClass('show');
        }
        smallButton_style_set = 1;
    }
    if (localStorage.getItem("txt_trans-smallButton-hover")) {
        var smallButton_css = document.createTextNode("#forum-codes button.smallButton:hover{text-transform: " + localStorage.getItem("txt_trans-smallButton-hover") + ";}");
        style.appendChild(smallButton_css);
        if (localStorage.getItem('hover_smallButton')) {
            $('#txt_trans_smallButton_form #txt_trans_set').attr('disabled', true);
            $('#txt_trans_smallButton_form #txt_trans_add').addClass('hide');
            $('#txt_trans_smallButton_form #txt_trans_clr').removeClass('hide');
            $('#txt_trans_smallButton_form #txt_trans_clr').addClass('show');
        }
        smallButton_style_set_hover = 1;
    }


    //behúzzuk stílusokat, és kiírjuk
    if (smallButton_style_set === 1){
        var smallButton_style_set_txt = '<p id="smallButton_style_set">#forum-codes button.smallButton{<br><span class="tab"></span>}</p>';
        $('.css_container').append(smallButton_style_set_txt);

        //loopolt stiusok
        for (var i = 0; i < smallButton_css_values.length; i++) {
            var sBcv = smallButton_css_values[i];
            if (localStorage.getItem(sBcv + '-smallButton')){
                $('#smallButton_style_set span').append(sBcv + ': ' + localStorage.getItem(sBcv + '-smallButton') + ';');
            }
        }

        //egyéb stílusok aminek nem lehet loopolni
        //text-transform
        if (localStorage.getItem('txt_trans-smallButton')){
                $('#smallButton_style_set span').append('text-transform: ' + localStorage.getItem('txt_trans-smallButton') + ';');   
            }


        $('.css_container #smallButton_style_set span').each(function() {
            $(this).html($(this).html().replace(/;/g, ';\n'));
        });
    }
    if (smallButton_style_set_hover === 1){
        var smallButton_style_set_txt_hover = '<p id="smallButton_style_set_hover">#forum-codes button.smallButton:hover{<br><span class="tab"></span>}</p>';
        $('.css_container').append(smallButton_style_set_txt_hover);

        //loopolt stiusok
        for (var i = 0; i < smallButton_css_values.length; i++) {
            var sBcv = smallButton_css_values[i];
            if (localStorage.getItem(sBcv + '-smallButton-hover')){
                $('#smallButton_style_set_hover span').append(sBcv + ': ' + localStorage.getItem(sBcv + '-smallButton-hover') + ';');
            }
        }

        //egyéb stílusok aminek nem lehet loopolni
        //text-transform
        if (localStorage.getItem('txt_trans-smallButton-hover')){
                $('#smallButton_style_set_hover span').append('text-transform: ' + localStorage.getItem('txt_trans-smallButton-hover') + ';');   
            }


        $('.css_container #smallButton_style_set_hover span').each(function() {
            $(this).html($(this).html().replace(/;/g, ';\n'));
        });
    }

    //js
    $("section#smallButton_set").append(separator);
    //Küldés gomb feltolása
    $("section#smallButton_set").append('<div id="kuldes_fel_smallButton_form"></div>');
    $('#kuldes_fel_smallButton_form').append('<input id="kuldes_fel_set" name="kuldes_fel_set" type="text" placeholder="Küldés gomb feltolása" value="Küldés gomb feltolása" disabled="true" /><button id="kuldes_fel_add" class="set-clear addset">+</button><button id="kuldes_fel_clr" class="set-clear hide delset">-</button>');
    $('#kuldes_fel_smallButton_form #kuldes_fel_add').attr('keyname', 'kuldes_fel');
    $('#kuldes_fel_smallButton_form #kuldes_fel_add').attr('ii', 'smallButton');
    $('#kuldes_fel_smallButton_form #kuldes_fel_clr').attr('delname', 'kuldes_fel');
    $('#kuldes_fel_smallButton_form #kuldes_fel_clr').attr('ii', 'smallButton');

    if (localStorage.getItem("kuldes_fel-smallButton")) {
        var jsMode1 = document.createElement("script");
        jsMode1.type = "text/javascript";
        jsMode1.src = baseUrl + 'jsMods/mehet-fel.js?' + time;
        document.body.appendChild(jsMode1);

        $('#kuldes_fel_smallButton_form #kuldes_fel_set').attr('disabled', true);
        $('#kuldes_fel_smallButton_form #kuldes_fel_add').addClass('hide');
        $('#kuldes_fel_smallButton_form #kuldes_fel_clr').removeClass('hide');
        $('#kuldes_fel_smallButton_form #kuldes_fel_clr').addClass('show');
        smallButton_js_set = 1;
    }

    //Küldés gomb -> smallButton
    $("#kuldes_fel_smallButton_form").after('<div id="kuldes_class_smallButton_form"></div>');
    $('#kuldes_class_smallButton_form').append('<input id="kuldes_class_set" name="kuldes_class_set" type="text" placeholder="Küldés gomb class" value="Küldés gomb class" disabled="true" /><button id="kuldes_class_add" class="set-clear addset">+</button><button id="kuldes_class_clr" class="set-clear hide delset">-</button>');
    $('#kuldes_class_smallButton_form #kuldes_class_add').attr('keyname', 'kuldes_class');
    $('#kuldes_class_smallButton_form #kuldes_class_add').attr('ii', 'smallButton');
    $('#kuldes_class_smallButton_form #kuldes_class_clr').attr('delname', 'kuldes_class');
    $('#kuldes_class_smallButton_form #kuldes_class_clr').attr('ii', 'smallButton');

    if (localStorage.getItem("kuldes_class-smallButton")) {
        var jsMode2 = document.createElement("script");
        jsMode2.type = "text/javascript";
        jsMode2.src = baseUrl + 'jsMods/mehet-class.js?' + time;
        document.body.appendChild(jsMode2);

        $('#kuldes_class_smallButton_form #kuldes_class_set').attr('disabled', true);
        $('#kuldes_class_smallButton_form #kuldes_class_add').addClass('hide');
        $('#kuldes_class_smallButton_form #kuldes_class_clr').removeClass('hide');
        $('#kuldes_class_smallButton_form #kuldes_class_clr').addClass('show');
        smallButton_js_set = 1;
    }

    //behúzzuk scriptet, és kiírjuk
    if (smallButton_js_set === 1){

        //kuldes-fel
        if (localStorage.getItem('kuldes_fel-smallButton')){
                var kuldes_fel_js = '//A Küldés gomb feltolása a többi gomb mellé\nvar submit = $("#post-submit");submit.addClass(\'post-submit\');$("#forum-codes").append($("submit"));';
                $('#js_set span').append(kuldes_fel_js);   
            }
        //kuldes-class
        if (localStorage.getItem('kuldes_class-smallButton')){
                var kuldes_class_js = '<br>//A Küldés gomb megkapja a többi gomb beállításait\n$("#post-submit").addClass("btn btn-info btn-sg smallButton");$("#post-submit").removeAttr(\'id\');';
                $('#js_set span').append(kuldes_class_js);   
            }

        $('.js_container #js_set span').each(function() {
            $(this).html($(this).html().replace(/;/g, ';\n'));
        });
    }

    //hover opció
    $('#smallButton_set').prepend('<input id="hover_smallButton" class="extra-btn" type="button" value=":hover opció bekapcsolás"/>');
    $('#hover_smallButton').after(separator);

    $('#hover_smallButton').click(function() {
        if (localStorage.getItem('hover_smallButton')){
            $(this).removeClass('button-on');
            localStorage.removeItem('hover_smallButton');
            window.location = document.URL;
        }
        else{
            $(this).addClass('button-on');
            localStorage.setItem('hover_smallButton', 'on');
            window.location = document.URL;
        }
    });

    if (localStorage.getItem('hover_smallButton')) {
        $('#hover_smallButton').addClass('button-on');
        $('#hover_smallButton').val(':hover opció kikapcsolás');
        var addsets = $('#smallButton_set div button.addset');
        var delsets = $('#smallButton_set div button.delset');
        addsets.attr('hover', 'hover');
        delsets.attr('hover', 'hover');
        //nem kellenek, mert nincs közük a hoverhez
        $('#smallButton_set #kuldes_fel_smallButton_form').prev('.separator').remove();
        $('#smallButton_set #kuldes_fel_smallButton_form').remove();
        $('#smallButton_set #kuldes_class_smallButton_form').remove();
    };

////////////
    //hsz//////
////////////

    var hsz_css_values = ['background-color', 'padding', 'margin', 'border', 'border-top', 'border-right', 'border-bottom', 'border-left', 'border-radius'];
    for (var i = 0; i < hsz_css_values.length; i++) {
        var hszcv = hsz_css_values[i];
        $('section#hsz_set').append('<div id="' + hszcv + '_hsz_form"></div>');
        $("#" + hszcv + "_hsz_form").append('<input id="' + hszcv + '_set" name="' + hszcv + '_set" type="text" placeholder=' + hszcv + ' /><button id="' + hszcv + '_add" class="set-clear addset">+</button><button id="' + hszcv + '_clr" class="set-clear hide delset">-</button>');

        if (localStorage.getItem(hszcv + "-hsz")) {
            var hsz_css = document.createTextNode("#forum-posts-list li, #forum-posts-list li:nth-child(2n){" + hszcv + ": " + localStorage.getItem(hszcv + "-hsz") + ";}");
            style.appendChild(hsz_css);
            $('#' + hszcv + '_hsz_form #' + hszcv + '_set').attr('disabled', true);
            $('#' + hszcv + '_hsz_form #' + hszcv + '_add').addClass("hide");
            $('#' + hszcv + '_hsz_form #' + hszcv + '_clr').removeClass("hide");
            $('#' + hszcv + '_hsz_form #' + hszcv + '_clr').addClass("show");
            hsz_style_set = 1;
        }

        var addgoNb = $('#' + hszcv + '_hsz_form #' + hszcv + '_add');
        addgoNb.attr('keyname', hszcv);
        addgoNb.attr('ii', 'hsz');

        var delgoNb = $('#' + hszcv + '_hsz_form #' + hszcv + '_clr');
        delgoNb.attr('delname', hszcv);
        delgoNb.attr('ii', 'hsz');
    }
    

    //behúzzuk stílusokat, és kiírjuk
    if (hsz_style_set === 1){
        var hsz_style_set_txt = '<p id="hsz_style_set">#forum-posts-list li,<br>#forum-posts-list li:nth-child(2n){<br><span class="tab"></span>}</p>';
        $('.css_container').append(hsz_style_set_txt);

        for (var i = 0; i < hsz_css_values.length; i++) {
            var hszcv = hsz_css_values[i];
            if (localStorage.getItem(hszcv + '-hsz')){
               $('#hsz_style_set span').append(hszcv + ': ' + localStorage.getItem(hszcv + '-hsz') + ';');
            }
        }

        $('.css_container #hsz_style_set span').each(function() {
            $(this).html($(this).html().replace(/;/g, ';\n'));
        });
    }

////////////
    //hszheader//////
////////////

    var hszheader_css_values = ['background-color', 'font-size', 'line-height', 'padding', 'margin', 'height', 'color', 'border', 'border-top', 'border-right', 'border-bottom', 'border-left', 'border-radius'];
    for (var i = 0; i < hszheader_css_values.length; i++) {
        var hhcv = hszheader_css_values[i];
        $('section#hszheader_set').append('<div id="' + hhcv + '_hszheader_form"></div>');
        $("#" + hhcv + "_hszheader_form").append('<input id="' + hhcv + '_set" name="' + hhcv + '_set" type="text" placeholder=' + hhcv + ' /><button id="' + hhcv + '_add" class="set-clear addset">+</button><button id="' + hhcv + '_clr" class="set-clear hide delset">-</button>');

        if (localStorage.getItem(hhcv + "-hszheader")) {
            var hszheader_css = document.createTextNode("#forum-posts-list .header, #forum-posts-list .post-answer .header{" + hhcv + ": " + localStorage.getItem(hhcv + "-hszheader") + ";}");
            style.appendChild(hszheader_css);
            $('#' + hhcv + '_hszheader_form #' + hhcv + '_set').attr('disabled', true);
            $('#' + hhcv + '_hszheader_form #' + hhcv + '_add').addClass("hide");
            $('#' + hhcv + '_hszheader_form #' + hhcv + '_clr').removeClass("hide");
            $('#' + hhcv + '_hszheader_form #' + hhcv + '_clr').addClass("show");
            hszheader_style_set = 1;
        }

        var addgoNb = $('#' + hhcv + '_hszheader_form #' + hhcv + '_add');
        addgoNb.attr('keyname', hhcv);
        addgoNb.attr('ii', 'hszheader');

        var delgoNb = $('#' + hhcv + '_hszheader_form #' + hhcv + '_clr');
        delgoNb.attr('delname', hhcv);
        delgoNb.attr('ii', 'hszheader');
    }
    
    //text-transform
    $('#font-size_hszheader_form').after('<div id="txt_trans_hszheader_form"></div>');
    $('#txt_trans_hszheader_form').append('<select id="txt_trans_set" name="txt_trans_set" size="1"></select><button id="txt_trans_add" class="set-clear addset">+</button><button id="txt_trans_clr" class="set-clear hide delset">-</button>');
    $('#txt_trans_hszheader_form #txt_trans_set').append('<option value="empty" selected="selected" disabled="disabled">text-transform</option>');
    $('#txt_trans_hszheader_form #txt_trans_set').append('<option value="none">none</option>');
    $('#txt_trans_hszheader_form #txt_trans_set').append('<option value="capitalize">Capitalize</option>');
    $('#txt_trans_hszheader_form #txt_trans_set').append('<option value="lowercase">lowercase</option>');
    $('#txt_trans_hszheader_form #txt_trans_set').append('<option value="uppercase">UPPERCASE</option>');

    $('#txt_trans_hszheader_form #txt_trans_add').attr('keyname', 'txt_trans');
    $('#txt_trans_hszheader_form #txt_trans_add').attr('ii', 'hszheader');
    $('#txt_trans_hszheader_form #txt_trans_clr').attr('delname', 'txt_trans');
    $('#txt_trans_hszheader_form #txt_trans_clr').attr('ii', 'hszheader');

    if (localStorage.getItem("txt_trans-hszheader")) {
        var hszheader_css = document.createTextNode("#forum-posts-list .header, #forum-posts-list .post-answer .header{text-transform: " + localStorage.getItem("txt_trans-hszheader") + ";}");
        style.appendChild(hszheader_css);
        if (!localStorage.getItem('hover_hszheader')) {
            $('#txt_trans_hszheader_form #txt_trans_set').attr('disabled', true);
            $('#txt_trans_hszheader_form #txt_trans_add').addClass('hide');
            $('#txt_trans_hszheader_form #txt_trans_clr').removeClass('hide');
            $('#txt_trans_hszheader_form #txt_trans_clr').addClass('show');
        }
        hszheader_style_set = 1;
    }   

    //behúzzuk stílusokat, és kiírjuk
    if (hszheader_style_set === 1){
        var hszheader_style_set_txt = '<p id="hszheader_style_set">#forum-posts-list .header,\n#forum-posts-list .post-answer .header{<br><span class="tab"></span>}</p>';
        $('.css_container').append(hszheader_style_set_txt);

        for (var i = 0; i < hszheader_css_values.length; i++) {
            var hhcv = hszheader_css_values[i];
            if (localStorage.getItem(hhcv + '-hszheader')){
               $('#hszheader_style_set span').append(hhcv + ': ' + localStorage.getItem(hhcv + '-hszheader') + ';');
            }
        }

        //txt-transf
        if (localStorage.getItem('txt_trans-hszheader')){
                $('#hszheader_style_set span').append('text-transform: ' + localStorage.getItem('txt_trans-hszheader') + ';');   
            }

        $('.css_container #hszheader_style_set span').each(function() {
            $(this).html($(this).html().replace(/;/g, ';\n'));
        });
    }

    //userképek törlése
    $('#hszheader_set').append(separator);
    $('#hszheader_set').append('<input id="userlogodel" class="extra-btn" type="button" value="user logok törlése"/>');

    $('#userlogodel').click(function() {
        if (localStorage.getItem('userlogodel')){
            $(this).removeClass('button-on');
            localStorage.removeItem('userlogodel');
            window.location = document.URL;
        }
        else{
            $(this).addClass('button-on');
            localStorage.setItem('userlogodel', 'on');
            window.location = document.URL;
        }
    });

    if (localStorage.getItem('userlogodel')) {
        $('#userlogodel').addClass('button-on');
        $('#userlogodel').val('user logók visszaállítása');
        var jsMode3 = document.createElement("script");
        jsMode3.type = "text/javascript";
        jsMode3.src = baseUrl + 'jsMods/userkep-del.js?' + time;
        document.body.appendChild(jsMode3);
        userlogodel_js_set = 1;
        
        var userlogodel_js = document.createTextNode("\n//User logók törlése\n$(\"#forum-posts-list ul li.forum-post header.header, #forum-posts-list ul li.forum-post.post-answer header.header\").each(function () {\nvar username = ($(this).find(\"a.name img\").length == 1) ? $(this).find(\"a.name img\").attr(\"alt\") : $(this).find(\"a.name\")[0].innerHTML;\nusername = username.replace(/ - VIP/, \"\");\nvar userid = $(this).find(\"a.name\").attr(\"href\").split(\"/\");\n$(this).find(\"a.name\").remove();\n$(this).find(\"span.icons\").remove();\n$(this).prepend('<a id=\"name\" href=\"/felhasznalo/' + userid[2] + '\" class=\"name avatar-link pull-left\">' + username + '</a>');\n});");
        $('#js_set span').append(userlogodel_js);

        //usernév előtti jel
        $('#userlogodel').after('<div id="usernevbefore_hszheader_form"></div>');
        $('#usernevbefore_hszheader_form').append('<input id="usernevbefore_set" name="usernevbefore_set" type="text" placeholder="usernév előtti jel" /><button id="usernevbefore_add" class="set-clear addset">+</button><button id="usernevbefore_clr" class="set-clear hide delset">-</button>');
        $('#usernevbefore_hszheader_form #usernevbefore_add').attr('keyname', 'usernevbefore');
        $('#usernevbefore_hszheader_form #usernevbefore_add').attr('ii', 'hszheader');
        $('#usernevbefore_hszheader_form #usernevbefore_clr').attr('delname', 'usernevbefore');
        $('#usernevbefore_hszheader_form #usernevbefore_clr').attr('ii', 'hszheader');

        if (localStorage.getItem("usernevbefore-hszheader")) {
            var hszheader_css = document.createTextNode("#forum-posts-list .avatar-link:before{content: '" + localStorage.getItem("usernevbefore-hszheader") + "';}");
            style.appendChild(hszheader_css);
            $('#usernevbefore_hszheader_form #usernevbefore_set').attr('disabled', true);
            $('#usernevbefore_hszheader_form #usernevbefore_add').addClass('hide');
            $('#usernevbefore_hszheader_form #usernevbefore_clr').removeClass('hide');
            $('#usernevbefore_hszheader_form #usernevbefore_clr').addClass('show');
            usernevbefore_style_set = 1;
        }

        if (usernevbefore_style_set === 1){
        var usernevbefore_style_set_txt = '<p id="usernevbefore_style_set">#forum-posts-list .avatar-link:before{<br><span class="tab"></span>}</p>';
        $('.css_container').append(usernevbefore_style_set_txt);

        if (localStorage.getItem('usernevbefore-hszheader')){
                $('#usernevbefore_style_set span').append("content: '" + localStorage.getItem('usernevbefore-hszheader') + "';");   
            }

        $('.css_container #usernevbefore_style_set span').each(function() {
            $(this).html($(this).html().replace(/;/g, ';\n'));
        });
    }

    };



////////////
    //hsztxt//////
////////////

    //ami egyszerű, azt loopoljuk (valid css névvel)
    var hsztxt_css_values = ['background-color', 'padding', 'margin', 'color', 'border', 'border-top', 'border-right', 'border-bottom', 'border-left', 'border-radius'];
    for (var i = 0; i < hsztxt_css_values.length; i++) {
        var htcv = hsztxt_css_values[i];
        $('section#hsztxt_set').append('<div id="' + htcv + '_hsztxt_form"></div>');
        $("#" + htcv + "_hsztxt_form").append('<input id="' + htcv + '_set" name="' + htcv + '_set" type="text" placeholder=' + htcv + ' /><button id="' + htcv + '_add" class="set-clear addset">+</button><button id="' + htcv + '_clr" class="set-clear hide delset">-</button>');

        //!important beállítása
        $('#background-color_hsztxt_form #background-color_set').attr('imp', 'imp');
        $('#border_hsztxt_form #border_set').attr('imp', 'imp');

        if (localStorage.getItem(htcv + "-hsztxt")) {
            var hsztxt_css = document.createTextNode("#forum-posts-list .body{" + htcv + ": " + localStorage.getItem(htcv + "-hsztxt") + ";}");
            style.appendChild(hsztxt_css);
            $('#' + htcv + '_hsztxt_form #' + htcv + '_set').attr('disabled', true);
            $('#' + htcv + '_hsztxt_form #' + htcv + '_add').addClass("hide");
            $('#' + htcv + '_hsztxt_form #' + htcv + '_clr').removeClass("hide");
            $('#' + htcv + '_hsztxt_form #' + htcv + '_clr').addClass("show");
            hsztxt_style_set = 1;
        }

        var addgoNb = $('#' + htcv + '_hsztxt_form #' + htcv + '_add');
        addgoNb.attr('keyname', htcv);
        addgoNb.attr('ii', 'hsztxt');


        var delgoNb = $('#' + htcv + '_hsztxt_form #' + htcv + '_clr');
        delgoNb.attr('delname', htcv);
        delgoNb.attr('ii', 'hsztxt');
    }

    //ami nem megoldható loop-al, azt egyesével
    //background-image
    $('#background-color_hsztxt_form').after('<div id="bgurl_hsztxt_form"></div>');
    $('#bgurl_hsztxt_form').append('<input id="bgurl_set" name="bgurl_set" type="text" placeholder="background-image" /><button id="bgurl_add" class="set-clear addset">+</button><button id="bgurl_clr" class="set-clear hide delset">-</button>');
    $('#bgurl_hsztxt_form #bgurl_add').attr('keyname', 'bgurl');
    $('#bgurl_hsztxt_form #bgurl_add').attr('ii', 'hsztxt');
    $('#bgurl_hsztxt_form #bgurl_clr').attr('delname', 'bgurl');
    $('#bgurl_hsztxt_form #bgurl_clr').attr('ii', 'hsztxt');

    if (localStorage.getItem("bgurl-hsztxt")) {
        var hsztxt_css = document.createTextNode("#forum-posts-list .body{background-image: url(" + localStorage.getItem("bgurl-hsztxt") + ");}");
        style.appendChild(hsztxt_css);
        $('#bgurl_hsztxty_form #bgurl_set').attr('disabled', true);
        $('#bgurl_hsztxt_form #bgurl_add').addClass('hide');
        $('#bgurl_hsztxt_form #bgurl_clr').removeClass('hide');
        $('#bgurl_hsztxt_form #bgurl_clr').addClass('show');
        hsztxt_style_set = 1;
    }

    //background-image repeat
    $('#bgurl_hsztxt_form').after('<div id="bgrepeat_hsztxt_form"></div>');
    $('#bgrepeat_hsztxt_form').append('<select id="bgrepeat_hsztxt_set" name="bgrepeat_set" size="1"></select><button id="bgrepeat_add" class="set-clear addset">+</button><button id="bgrepeat_clr" class="set-clear hide delset">-</button>');
    $('#bgrepeat_hsztxt_set').append('<option value="empty" selected="selected" disabled="disabled">background-repeat</option>');
    $('#bgrepeat_hsztxt_set').append('<option value="no-repeat">no-repeat</option>');
    $('#bgrepeat_hsztxt_set').append('<option value="repeat">repeat</option>');
    $('#bgrepeat_hsztxt_set').append('<option value="repeat-x">repeat-x</option>');
    $('#bgrepeat_hsztxt_set').append('<option value="repeat-y">repeat-y</option>');
    $('#bgrepeat_hsztxt_form #bgrepeat_add').attr('keyname', 'bgrepeat');
    $('#bgrepeat_hsztxt_form #bgrepeat_add').attr('ii', 'hsztxt');
    $('#bgrepeat_hsztxt_form #bgrepeat_clr').attr('delname', 'bgrepeat');
    $('#bgrepeat_hsztxt_form #bgrepeat_clr').attr('ii', 'hsztxt');

    if (localStorage.getItem("bgrepeat-hsztxt")) {
        var hsztxt_css = document.createTextNode("#forum-posts-list .body{background-repeat: " + localStorage.getItem("bgrepeat-hsztxt") + ";}");
        style.appendChild(hsztxty_css);
        $('#bgrepeat_hsztxt_form #bgrepeat_set').attr('disabled', true);
        $('#bgrepeat_hsztxt_form #bgrepeat_add').addClass('hide');
        $('#bgrepeat_hsztxt_form #bgrepeat_clr').removeClass('hide');
        $('#bgrepeat_hsztxt_form #bgrepeat_clr').addClass('show');
        hsztxt_style_set = 1;
    }

    //background-position
    $('#bgrepeat_hsztxt_form').after('<div id="bgpos_hsztxt_form"></div>');
    $('#bgpos_hsztxt_form').append('<select id="bgpos_hsztxt_set" name="bgpos_set" size="1"></select><button id="bgpos_add" class="set-clear addset">+</button><button id="bgpos_clr" class="set-clear hide delset">-</button>');
    $('#bgpos_hsztxt_set').append('<option value="empty" selected="selected" disabled="disabled">background-position</option>');
    $('#bgpos_hsztxt_set').append('<option value="left top">left top</option>');
    $('#bgpos_hsztxt_set').append('<option value="left center">left center</option>');
    $('#bgpos_hsztxt_set').append('<option value="left bottom">left bottom</option>');
    $('#bgpos_hsztxt_set').append('<option value="right top">right top</option>');
    $('#bgpos_hsztxt_set').append('<option value="right center">right center</option>');
    $('#bgpos_hsztxt_set').append('<option value="right bottom">right bottom</option>');
    $('#bgpos_hsztxt_set').append('<option value="center top">center top</option>');
    $('#bgpos_hsztxt_set').append('<option value="center center">center center</option>');
    $('#bgpos_hsztxt_set').append('<option value="center bottom">center bottom</option>');

    $('#bgpos_hsztxt_form #bgpos_add').attr('keyname', 'bgpos');
    $('#bgpos_hsztxt_form #bgpos_add').attr('ii', 'hsztxt');
    $('#bgpos_hsztxt_form #bgpos_clr').attr('delname', 'bgpos');
    $('#bgpos_hsztxt_form #bgpos_clr').attr('ii', 'hsztxt');

    if (localStorage.getItem("bgpos-hsztxt")) {
        var hsztxt_css = document.createTextNode("#forum-posts-list .body{background-position: " + localStorage.getItem("bgpos-hsztxt") + ";}");
        style.appendChild(hsztxt_css);
        $('#bgpos_hsztxt_form #bgpos_set').attr('disabled', true);
        $('#bgpos_hsztxt_form #bgpos_add').addClass('hide');
        $('#bgpos_hsztxt_form #bgpos_clr').removeClass('hide');
        $('#bgpos_hsztxt_form #bgpos_clr').addClass('show');
        hsztxt_style_set = 1;
    }

    //behúzzuk stílusokat, és kiírjuk
    if (hsztxt_style_set === 1){
        var hsztxt_style_set_txt = '<p id="hsztxt_style_set">#forum-posts-list .body{<br><span class="tab"></span>}</p>';
        $('.css_container').append(hsztxt_style_set_txt);

        //loopolt stiusok
        for (var i = 0; i < hsztxt_css_values.length; i++) {
            var htcv = hsztxt_css_values[i];
            if (localStorage.getItem(htcv + '-hsztxt')){
               $('#hsztxt_style_set span').append(htcv + ': ' + localStorage.getItem(htcv + '-hsztxt') + ';');
            }
        }


        //egyéb stílusok aminek nem lehet loopolni
        //bg-url
        if (localStorage.getItem('bgurl-hsztxt')){
                $('#hsztxt_style_set span').append('background-image: ' + localStorage.getItem('bgurl-hsztxt') + ';');   
            }
        //bg-repeat
        if (localStorage.getItem('bgrepeat-hsztxt')){
                $('#hsztxt_style_set span').append('background-repeat: ' + localStorage.getItem('bgrepeat-hsztxt') + ';');   
            }
        //bg-pos
        if (localStorage.getItem('bgpos-hsztxt')){
                $('#hsztxt_style_set span').append('background-position: ' + localStorage.getItem('bgpos-hsztxt') + ';');   
            }

        $('.css_container #hsztxt_style_set span').each(function() {
            $(this).html($(this).html().replace(/;/g, ';\n'));
        });
    }

////////////
    //hszfoot//////
////////////

    //ami egyszerű, azt loopoljuk (valid css névvel)
    var hszfoot_css_values = ['background-color', 'padding', 'margin', 'color', 'border', 'border-top', 'border-right', 'border-bottom', 'border-left', 'border-radius'];
    for (var i = 0; i < hszfoot_css_values.length; i++) {
        var hfcv = hszfoot_css_values[i];
        $('section#hszfoot_set').append('<div id="' + hfcv + '_hszfoot_form"></div>');
        $("#" + hfcv + "_hszfoot_form").append('<input id="' + hfcv + '_set" name="' + hfcv + '_set" type="text" placeholder=' + hfcv + ' /><button id="' + hfcv + '_add" class="set-clear addset">+</button><button id="' + hfcv + '_clr" class="set-clear hide delset">-</button>');

        //!important beállítása
        $('#background-color_hszfoot_form #background-color_set').attr('imp', 'imp');
        $('#border_hszfoot_form #border_set').attr('imp', 'imp');

        if (localStorage.getItem(hfcv + "-hszfoot")) {
            var hszfoot_css = document.createTextNode("#forum-posts-list .footer{" + hfcv + ": " + localStorage.getItem(hfcv + "-hszfoot") + ";}");
            style.appendChild(hszfoot_css);
            $('#' + hfcv + '_hszfoot_form #' + hfcv + '_set').attr('disabled', true);
            $('#' + hfcv + '_hszfoot_form #' + hfcv + '_add').addClass("hide");
            $('#' + hfcv + '_hszfoot_form #' + hfcv + '_clr').removeClass("hide");
            $('#' + hfcv + '_hszfoot_form #' + hfcv + '_clr').addClass("show");
            hszfoot_style_set = 1;
        }

        var addgoNb = $('#' + hfcv + '_hszfoot_form #' + hfcv + '_add');
        addgoNb.attr('keyname', hfcv);
        addgoNb.attr('ii', 'hszfoot');


        var delgoNb = $('#' + hfcv + '_hszfoot_form #' + hfcv + '_clr');
        delgoNb.attr('delname', hfcv);
        delgoNb.attr('ii', 'hszfoot');
    }

    //ami nem megoldható loop-al, azt egyesével
    //background-image
    $('#background-color_hszfoot_form').after('<div id="bgurl_hszfoot_form"></div>');
    $('#bgurl_hszfoot_form').append('<input id="bgurl_set" name="bgurl_set" type="text" placeholder="background-image" /><button id="bgurl_add" class="set-clear addset">+</button><button id="bgurl_clr" class="set-clear hide delset">-</button>');
    $('#bgurl_hszfoot_form #bgurl_add').attr('keyname', 'bgurl');
    $('#bgurl_hszfoot_form #bgurl_add').attr('ii', 'hszfoot');
    $('#bgurl_hszfoot_form #bgurl_clr').attr('delname', 'bgurl');
    $('#bgurl_hszfoot_form #bgurl_clr').attr('ii', 'body');

    if (localStorage.getItem("bgurl-hszfoot")) {
        var hszfoot_css = document.createTextNode("#forum-posts-list .footer{background-image: url(" + localStorage.getItem("bgurl-hszfoot") + ");}");
        style.appendChild(hszfoot_css);
        $('#bgurl_hszfooty_form #bgurl_set').attr('disabled', true);
        $('#bgurl_hszfoot_form #bgurl_add').addClass('hide');
        $('#bgurl_hszfoot_form #bgurl_clr').removeClass('hide');
        $('#bgurl_hszfoot_form #bgurl_clr').addClass('show');
        hszfoot_style_set = 1;
    }

    //background-image repeat
    $('#bgurl_hszfoot_form').after('<div id="bgrepeat_hszfoot_form"></div>');
    $('#bgrepeat_hszfoot_form').append('<select id="bgrepeat_hszfoot_set" name="bgrepeat_set" size="1"></select><button id="bgrepeat_add" class="set-clear addset">+</button><button id="bgrepeat_clr" class="set-clear hide delset">-</button>');
    $('#bgrepeat_hszfoot_set').append('<option value="empty" selected="selected" disabled="disabled">background-repeat</option>');
    $('#bgrepeat_hszfoot_set').append('<option value="no-repeat">no-repeat</option>');
    $('#bgrepeat_hszfoot_set').append('<option value="repeat">repeat</option>');
    $('#bgrepeat_hszfoot_set').append('<option value="repeat-x">repeat-x</option>');
    $('#bgrepeat_hszfoot_set').append('<option value="repeat-y">repeat-y</option>');
    $('#bgrepeat_hszfoot_form #bgrepeat_add').attr('keyname', 'bgrepeat');
    $('#bgrepeat_hszfoot_form #bgrepeat_add').attr('ii', 'hszfoot');
    $('#bgrepeat_hszfoot_form #bgrepeat_clr').attr('delname', 'bgrepeat');
    $('#bgrepeat_hszfoot_form #bgrepeat_clr').attr('ii', 'hszfoot');

    if (localStorage.getItem("bgrepeat-hszfoot")) {
        var hszfoot_css = document.createTextNode("#forum-posts-list .footer{background-repeat: " + localStorage.getItem("bgrepeat-hszfoot") + ";}");
        style.appendChild(hszfooty_css);
        $('#bgrepeat_hszfoot_form #bgrepeat_set').attr('disabled', true);
        $('#bgrepeat_hszfoot_form #bgrepeat_add').addClass('hide');
        $('#bgrepeat_hszfoot_form #bgrepeat_clr').removeClass('hide');
        $('#bgrepeat_hszfoot_form #bgrepeat_clr').addClass('show');
        hszfoot_style_set = 1;
    }

    //background-position
    $('#bgrepeat_hszfoot_form').after('<div id="bgpos_hszfoot_form"></div>');
    $('#bgpos_hszfoot_form').append('<select id="bgpos_hszfoot_set" name="bgpos_set" size="1"></select><button id="bgpos_add" class="set-clear addset">+</button><button id="bgpos_clr" class="set-clear hide delset">-</button>');
    $('#bgpos_hszfoot_set').append('<option value="empty" selected="selected" disabled="disabled">background-position</option>');
    $('#bgpos_hszfoot_set').append('<option value="left top">left top</option>');
    $('#bgpos_hszfoot_set').append('<option value="left center">left center</option>');
    $('#bgpos_hszfoot_set').append('<option value="left bottom">left bottom</option>');
    $('#bgpos_hszfoot_set').append('<option value="right top">right top</option>');
    $('#bgpos_hszfoot_set').append('<option value="right center">right center</option>');
    $('#bgpos_hszfoot_set').append('<option value="right bottom">right bottom</option>');
    $('#bgpos_hszfoot_set').append('<option value="center top">center top</option>');
    $('#bgpos_hszfoot_set').append('<option value="center center">center center</option>');
    $('#bgpos_hszfoot_set').append('<option value="center bottom">center bottom</option>');

    $('#bgpos_hszfoot_form #bgpos_add').attr('keyname', 'bgpos');
    $('#bgpos_hszfoot_form #bgpos_add').attr('ii', 'hszfoot');
    $('#bgpos_hszfoot_form #bgpos_clr').attr('delname', 'bgpos');
    $('#bgpos_hszfoot_form #bgpos_clr').attr('ii', 'hszfoot');

    if (localStorage.getItem("bgpos-hszfoot")) {
        var hszfoot_css = document.createTextNode("#forum-posts-list .footer{background-position: " + localStorage.getItem("bgpos-hszfoot") + ";}");
        style.appendChild(hszfoot_css);
        $('#bgpos_hszfoot_form #bgpos_set').attr('disabled', true);
        $('#bgpos_hszfoot_form #bgpos_add').addClass('hide');
        $('#bgpos_hszfoot_form #bgpos_clr').removeClass('hide');
        $('#bgpos_hszfoot_form #bgpos_clr').addClass('show');
        hszfoot_style_set = 1;
    }

    //behúzzuk stílusokat, és kiírjuk
    if (hszfoot_style_set === 1){
        var hszfoot_style_set_txt = '<p id="hszfoot_style_set">#forum-posts-list .footer{<br><span class="tab"></span>}</p>';
        $('.css_container').append(hszfoot_style_set_txt);

        //loopolt stiusok
        for (var i = 0; i < hszfoot_css_values.length; i++) {
            var hfcv = hszfoot_css_values[i];
            if (localStorage.getItem(hfcv + '-hszfoot')){
               $('#hszfoot_style_set span').append(hfcv + ': ' + localStorage.getItem(hfcv + '-hszfoot') + ';');
            }
        }


        //egyéb stílusok aminek nem lehet loopolni
        //bg-url
        if (localStorage.getItem('bgurl-hszfoot')){
                $('#hszfoot_style_set span').append('background-image: ' + localStorage.getItem('bgurl-hszfoot') + ';');   
            }
        //bg-repeat
        if (localStorage.getItem('bgrepeat-hszfoot')){
                $('#hszfoot_style_set span').append('background-repeat: ' + localStorage.getItem('bgrepeat-hszfoot') + ';');   
            }
        //bg-pos
        if (localStorage.getItem('bgpos-hszfoot')){
                $('#hszfoot_style_set span').append('background-position: ' + localStorage.getItem('bgpos-hszfoot') + ';');   
            }

        $('.css_container #hszfoot_style_set span').each(function() {
            $(this).html($(this).html().replace(/;/g, ';\n'));
        });
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////   
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////   
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////   
    
    if (body_style_set || forumbg_style_set || banner_style_set || textarea_style_set || smallButton_style_set || smallButton_style_set_hover || hszheader_style_set || hsz_style_set || hsztxt_style_set || hszfoot_style_set === 1){
        $('.css_container').prepend('<header class="cont_header">CSS kód</header>');
    }
    if (smallButton_js_set || userlogodel_js_set === 1){
        $('.js_container').prepend('<header class="cont_header">JS kód</header>');
    }

    $("#info").prepend('<header class="cont_header">INFO <span> - v0.00001 preprealfa</span></header>');

    //hozzáadás gomb magic
    $('.addset').click(function() {
        var keyname = $(this).attr('keyname');
        var ii = $(this).attr('ii');
        var important = $(this).prev('input').attr('imp');
        var hover_attr = $(this).attr('hover');
        var hover = "";
        if (hover_attr){
            hover = "-hover";
        }
        itemadd(keyname, ii, important, hover);
    });

    function itemadd(keyname, ii, important, hover) {
        console.log(keyname);
        var set = $('#' + keyname + '_' + ii + '_form' + ' #' + keyname + '_set');
        var set_val = set.val();
        //console.log(set_val); //debug
        //return false;

        if (set_val === '') {
            alert('Nem adhatsz hozzá üres sort!');
            return false;
        }

        if(hover === ""){
            console.log("nincs hover");
            if (important) {
                localStorage.setItem(keyname + '-' + ii, set_val + ' !important');
                window.location = document.URL;
                return false;
            }
            else {
                localStorage.setItem(keyname + '-' + ii, set_val);
                window.location = document.URL;
                return false;
            }
        }
        else{
            console.log("van hover");
            if (important) {
                localStorage.setItem(keyname + '-' + ii + hover, set_val + ' !important');
                window.location = document.URL;
                return false;
            }
            else {
                localStorage.setItem(keyname + '-' + ii + hover, set_val);
                window.location = document.URL;
                return false;
            }
        }
    }

    //törlés gomb magic
    $('.delset').click(function() {
        var delname = $(this).attr('delname');
        var ii = $(this).attr('ii');
        var hover_attr = $(this).attr('hover');
        var hover = "";
        if (hover_attr){
            hover = "-hover";
        }
        itemdel(delname, ii, hover);
    });

    function itemdel(delname, ii, hover) {
        if(hover === ""){
            localStorage.removeItem(delname + '-' + ii);
            window.location = document.URL;
            return false;    
        }
        else{
            localStorage.removeItem(delname + '-' + ii + hover);
            window.location = document.URL;
            return false;
        }
    }

    //beállítások toggle
    $('.tab-head').click(function(){
        var slideId = $(this).parent().attr('class');
        var classes = $(this).next('section').attr('class');
        if (classes === 'sets hide'){
            $(this).next('section').removeClass('hide');
            $(this).next('section').addClass('show');
            localStorage.setItem(slideId, '1');
        }
        if (classes === 'sets show'){
            $(this).next('section').removeClass('show');
            $(this).next('section').addClass('hide');
            localStorage.removeItem(slideId, '1');
        }
    });

    var slides = ['body', 'banner', 'textarea', 'smallButton', 'hsz', 'hszheader', 'hsztxt', 'hszfoot'];
    for (var i = 0; i < slides.length; i++) {
        var slide = slides[i];
        var rest_class = '_css_container';
        if (localStorage.getItem(slide + rest_class)) {
            $(' #' + slide + '_set').removeClass('hide');
            $(' #' + slide + '_set').addClass('show');
        }       
    };

    //menü toggle
    $('#custom_menu').click(function(){
        var custom_menu = $("aside#menu_container");
        var custom_menu_class = custom_menu.attr('class');
        if (custom_menu_class === 'show_custom_menu'){
            custom_menu.removeClass('show_custom_menu');
            custom_menu.addClass('hide_custom_menu');
            localStorage.setItem('custom_menu', '1');
        }
        else{
            custom_menu.removeClass('hide_custom_menu');
            custom_menu.addClass('show_custom_menu');
            localStorage.removeItem('custom_menu', '1');
        }

    });

    if (localStorage.getItem('custom_menu')){
        var custom_menu = $("aside#menu_container");
        custom_menu.removeClass('show_custom_menu');
        custom_menu.addClass('hide_custom_menu');
    }

    //kódok toggle
    $('#custom_kodok').click(function(){
        var custom_kodok = $("#custom_code_container");
        var custom_kodok_class = custom_kodok.attr('class');
        if (custom_kodok_class === 'show_custom_code'){
            custom_kodok.removeClass('show_custom_code');
            custom_kodok.addClass('hide_custom_code');
            localStorage.setItem('custom_kodok', '1');
        }
        else{
            custom_kodok.removeClass('hide_custom_code');
            custom_kodok.addClass('show_custom_code');
            localStorage.removeItem('custom_kodok', '1');
        }

    });

    if (localStorage.getItem('custom_kodok')){
        var custom_kodok = $("#custom_code_container");
        custom_kodok.removeClass('show_custom_code');
        custom_kodok.addClass('hide_custom_code');
    }

    //info toggle
    $('#custom_info').click(function(){
        var custom_info = $("#info");
        var custom_info_class = custom_info.attr('class');
        if (custom_info_class === 'show_custom_info'){
            custom_info.removeClass('show_custom_info');
            custom_info.addClass('hide_custom_info');
            localStorage.setItem('custom_info', '1');
        }
        else{
            custom_info.removeClass('hide_custom_info');
            custom_info.addClass('show_custom_info');
            localStorage.removeItem('custom_info', '1');
        }

    });

    if (localStorage.getItem('custom_info')){
        var custom_info = $("#info");
        custom_info.removeClass('show_custom_info');
        custom_info.addClass('hide_custom_info');
    }

    $('#custom_clear').click(function(){
            localStorage.clear()
            window.location = document.URL;
    });


    //colorpicker by SG.hu :)
    //colorPicker csodabox hozzáadása
    var bgcolor = $("section input#background-color_set, section input#forumbg_set");
    bgcolor.addClass("colorPicker");

    var txtcolor = $("section input#color_set");
    txtcolor.addClass("colorPicker");

    //stb
    var $colorPickers = $body.find('#menu_container section input.colorPicker');
    if ($colorPickers.length > 0) {
        require(['vendor/colorpicker/colorpicker'], function () {
            var link = document.createElement("link");
            link.type = "text/css";
            link.rel = "stylesheet";
            link.href = require.toUrl("../css/colorpicker/colorpicker.css");
            document.getElementsByTagName("head")[0].appendChild(link);

            $colorPickers.each(function () {
                var $self = $(this);
                $self.ColorPicker({
                    onBeforeShow: function () {
                        $self.ColorPickerSetColor(this.value);
                    },
                    onSubmit    : function (hsb, hex, rgb, el) {
                        $self.val('#' + hex);
                        $self.ColorPickerHide();
                    },
                    onChange    : function (hsb, hex, rgb) {
                        $self.val('#' + hex);
                    }
                });
            });
        });
    }
    
}


$(document).ready(function() {
    css_maker();
});