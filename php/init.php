<?php
header("Access-Control-Allow-Origin: *");

require_once('db.php');

// set
if (isset($_POST['user']) and isset($_POST['color']) and isset($_POST['bgcolor']) and isset($_POST['side'])) {
    $color = mysqli_real_escape_string($mysql, $_POST['color']);
    $bgcolor = mysqli_real_escape_string($mysql, $_POST['bgcolor']);
    $name = mysqli_real_escape_string($mysql, GetNameFromSide($_POST['side']));

    mysqli_query($mysql, "INSERT INTO settings (name, color, bgcolor) VALUES('$name', '$color', '$bgcolor') ON DUPLICATE KEY UPDATE color = '$color', bgcolor = '$bgcolor'");
}

// loadsettings
$settings = Array("settings" => array(), "ages" => array());
$r = mysqli_query($mysql, "SELECT name, color, bgcolor FROM settings");
while ($row = mysqli_fetch_assoc($r)) {
    $settings["settings"][] = array(utf8_encode($row["name"]), $row["color"], $row["bgcolor"]);
}
$r = mysqli_query($mysql, "SELECT userid, age, gender FROM age");
while ($row = mysqli_fetch_assoc($r)) {
    $settings["age"][] = array($row['userid'], $row['age'], $row['gender']);
}

echo json_encode($settings);

function GetNameFromSide($side)
{
    $name = trim(GetBetween($side, '<header class="user-hello">', '</header>'));
    $name = GetBetween($name, 'Üdv, ', '!');
    if (strlen($name)) {
        return ($name);
    }
    else {
        die();
    }
}

function GetBetween($content, $start, $end)
{
    $r = explode($start, $content);
    if (isset($r[1])) {
        $r = explode($end, $r[1]);

        return $r[0];
    }

    return '';
}

?> 