<?php

header("Access-Control-Allow-Origin: *");

$ch = curl_init();
if (!$ch) {
    die("Couldn't initialize a cURL handle");
}
//$ret = curl_setopt($ch, CURLOPT_HEADER,         1);
//$ret = curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
$ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$ret = curl_setopt($ch, CURLOPT_TIMEOUT,        30);

$tags = $_POST['tags'] ? $_POST['tags'] : '';
$url = 'http://memeful.com/web/ajax/posts?count=30&page=1&tags=' . urlencode($tags);
$ret = curl_setopt($ch, CURLOPT_URL, $url);

$ret = curl_exec($ch);

if (empty($ret)) {
    curl_close($ch);
    //echo(curl_error($ch));
}
else {
    $info = curl_getinfo($ch);
    //echo $name . ' - ' . $info['http_code'] . '<br />';

    if (empty($info['http_code'])) {
        die("No HTTP code was returned");
    }
    else {
        if ($info['http_code'] == 200) {
            $data = json_decode($ret, true);

            echo $ret;
        }
    }

    curl_close($ch);
}

?> 