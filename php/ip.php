<?php
header("Access-Control-Allow-Origin: *");

require_once('db.php');

if (isset($_GET['ip'])) {
    $r = mysqli_query($mysql, "SELECT ip FROM ip WHERE ip = '" . mysqli_real_escape_string($mysql, $_GET['ip']) . "';");
    if (mysqli_num_rows($r) == 0) {
        mysqli_query($mysql, "INSERT INTO ip SET ip = '" . mysqli_real_escape_string($mysql, $_GET['ip']) . "';");
    }
}

$ips = array();

$r = mysqli_query($mysql, "SELECT ip FROM ip ORDER BY INET_ATON(ip);");
if (mysqli_num_rows($r) > 0) {
    while ($row = mysqli_fetch_assoc($r)) {
        $ips[] = $row['ip'];
    }
}

die(json_encode($ips));