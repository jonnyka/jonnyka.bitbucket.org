<?php
header("Access-Control-Allow-Origin: *");

require_once('db.php');

// loaduser
if (isset($_GET['userid'])) {
    $user = Array(0, "");
    $r = mysqli_query($mysql, "SELECT age, gender FROM age WHERE userid = " . mysqli_real_escape_string($mysql, $_GET['userid']) . ";");
    if (mysqli_num_rows($r) > 0) {
        while ($row = mysqli_fetch_assoc($r)) {
            $user = array($row['age'], $row['gender']);
        }
    }
    else {
        $http_response = file_get_contents('http://sg.hu/felhasznalo/' . $_GET['userid']);

        $row['age'] = trim(strip_tags(GetBetween($http_response, "<td>Életkor:</td>", "</td>"))) + 0;
        $row['gender'] = trim(strip_tags(GetBetween($http_response, "<td>Neme:</td>", "</td>")));

        mysqli_query($mysql, "INSERT INTO age SET userid = " . mysqli_real_escape_string($mysql, $_GET['userid']) . ", age = $row[age], gender = '$row[gender]', modified = NOW()");

        $user = array($row['age'], $row['gender'][0]);
    }

    die(json_encode($user));
}

function GetNameFromSide($side)
{
    $name = trim(GetBetween($side, '<header class="user-hello">', '</header>'));
    $name = GetBetween($name, 'Üdv, ', '!');
    if (strlen($name)) {
        return ($name);
    }
    else {
        die();
    }
}

function getTextBetweenTags($string, $tagname)
{
    $pattern = "/<header class=\"user-hello\">(.*?)<\/header>/";
    preg_match($pattern, $string, $matches);

    return $matches[1];
}

function GetBetween($content, $start, $end)
{
    $r = explode($start, $content);
    if (isset($r[1])) {
        $r = explode($end, $r[1]);

        return $r[0];
    }

    return '';
}
?> 