/**
 * console.log style
 */
var log_style_start = [
    'background-color: #222',
    'color: orange',
    'display: inline-block',
    'border-left: 5px solid orange',
    'border-radius: 0 17px 17px 0',
    'padding: 10px 20px 10px 10px',
    'line-height: 35px',
    'font-weight: bold'
].join(';');

var log_style_done = [
    'background-color: #222',
    'color: lime',
    'display: inline-block',
    'border-left: 5px solid lime',
    'border-radius: 0 17px 17px 0',
    'padding: 10px 20px 10px 10px',
    'line-height: 35px',
    'font-weight: bold'
].join(';');

console.log('%c Config loading started... ', log_style_start);

/**
 * Custom css definitions
 * The keys must be the file names like this: /custom_css/{key}.css
 * The values are the values of the select option.
 *
 * @type {Array}
 */
var customCssFiles = [
    { 'lines': 'Lines' },
    { 'material_empty' : 'Material' },
    { 'material_empty_dark' : 'Material Dark' }
];

// All the css files, including the default.
var allCssFiles;
allCssFiles = customCssFiles;
allCssFiles.unshift({'default': 'Világos'});

/**
 * Localstorage settings for the options div with default values.
 * Type is the form element type.
 * Id is the id used in the localstorage and the form element.
 * Default is the default initial value of the setting.
 * Name is the label of the form element.
 * If its a select element, options stores the options.
 * Help is well.. help.
 * Separator is needed if you want a divider on top of the form element
 *
 * @type {Array}
 */
var localStoreSettings = [
    { type: 'disabled', id: 'favs', "default": 1, name: 'Kedvencek', help: '' },
    { type: 'select', id: 'custom_css', "default": 'material_empty', name: 'Kinézet', help: 'A főoldal kinézete.', options: allCssFiles },
    { type: 'checkbox', id: 'responsive', "default": 1, name: 'Reszponzív', help: 'Ha be van kapcsolva, a megjelenés fluid lesz, azaz kitölti az egész képernyőt.' },
    { type: 'checkbox', id: 'header', "default": 0, name: 'SG fejléc', help: 'Ha be van kapcsolva, megjelenik felül az SG-s kék fejléc.' },
    { type: 'checkbox', id: 'fbbox', "default": 0, name: 'Facebook box megjelenítése', help: 'Ha be van kapcsolva, látszik a facebook-os like doboz.' },
    { type: 'checkbox', id: 'jedi', "default": 1, name: 'Jedi Mester megemlékezés', help: 'Fejlécben lévő apró megemlékező rész ki-be kapcsolása. R.I.P. Mester!' },
    { type: 'checkbox', id: 'snow', "default": 0, name: 'Hóesés', help: 'Ha be van kapcsolva, lesz hóesés az oldalon.' },
    { type: 'select', id: 'sgiframe', "default": 2, name: 'SG linkek', help: 'A bal oldali sávon és a topic listában lévő linkek működését állítja.<br /><br />Normál: az alap működés, azaz rányílik az aktuális ablakra.<br /><br />Új oldalra: új oldalra nyílik.<br /><br />Iframe-be: a cset alatt egy iframe-be nyílik.', options: [
        { 0: 'Normál' },
        { 2: 'Új oldalra' },
        { 1: 'Iframe-be' }
    ]},

    { type: 'checkbox', id: 'sidebar_newtopics', "default": 1, name: 'Legújabb témák', help: 'Ha be van kapcsolva, látható a sidebaron a legújabb témák doboz a bal oldali sávban', separator: true },
    { type: 'checkbox', id: 'sidebar_cooltopics', "default": 1, name: 'Népszerű témák', help: 'Ha be van kapcsolva, látható a népszerű témák doboz a bal oldali sávban' },
    { type: 'checkbox', id: 'sidebar_birthdays', "default": 1, name: 'Mai szülinaposok', help: 'Ha be van kapcsolva, látható a szülinaposok doboz a bal oldali sávban' },
    { type: 'checkbox', id: 'articles', "default": 0, name: 'Főoldali cikkek', help: 'Ha be van kapcsolva, látható a cikkeink doboz a cset alatt' },
    { type: 'checkbox', id: 'topics', "default": 0, name: 'Témák listája', help: 'Ha be van kapcsolva, láthatóak a témák a cset alatt' },
    { type: 'checkbox', id: 'gallery', "default": 0, name: 'Legújabb galériák', help: 'Ha be van kapcsolva, látható a Legújabb galériák menüpont bal oldalt.' },

    { type: 'select', id: 'utolsoreg', "default": 5, mod: true, name: 'Utolsó regisztrálók', help: 'Hányat mutasson az utoljára regisztráltak közül a cset alatt. Kikapcsolnál nem lesz ilyen táblázat, egyébként minél nagyobb a szám, annál lassabb.', options: [
        { 0: 'Kikapcsol' },
        { 5: '5' },
        { 10: '10' },
        { 25: '25' },
        { 50: '50' }
    ]}
];

/**
 * Global variables.
 */
var pUrl = '//sg.hu/chat/php/',
    phpUrl = pUrl + 'init.php',
    readUserUrl = pUrl + 'read_user.php',
    readThreadUrl = pUrl + 'thread.php',
    gifSearchUrl = pUrl + 'gifsearch.php',
    ipUrl = pUrl + 'ip.php',
    sgNodeUrl = 'https://chat.sg.hu:18080',
    focused = true,
    msg_counter = 0,
    new_topics = 0,
    settings = [],
    age = [],
    threadData = [],
    mute_list = [],
    sidebar = "",
    clearName = "",
    initialized = false,
    interval = '',
    isResponsive = false,
    socketOnline = true,
    socket = null,
    socketl = null,
    sgTitle = "SG fórum",
    forumChatInput = $('#forum-chat-input'),
    identid = '',
    floodTime = '2000', // 3 sec
    clearNameTime = 5000,
    threadLength = 255,
    os = 'windows',
    userid = 0,
    videow = "476",
    videoh = "267",
    headerTop = $('#header-top'),
    $textInput = document.getElementById('forum-chat-input'),
    $chatListFrame = document.getElementById('list-frame'),
    $chatList = document.getElementById('forum-chat-list'),
    $userList = document.getElementById('forum-chat-user-list'),
    $users = {};

var is_chrome = false;
var isChromium = window.chrome,
    vendorName = window.navigator.vendor;
if (isChromium !== null && isChromium !== undefined && vendorName === "Google Inc.") {
    is_chrome = true;
}
var neoScrollSpeed = 8;
var otherScrollSpeed = 15;
if (is_chrome) {
    neoScrollSpeed = 1;
    otherScrollSpeed = 6;
}

/**
 * Current user's name.
 */
var userTextDiv = $('header.user-hello')[0];
var userText = $(userTextDiv).text();
var userTextParts = userText.split(',');
var replacedUserStr = userTextParts[1].replace('!', '');
var currentUser = replacedUserStr.replace('\n', '').trim();
var lastUser = 'xxx';

console.log('%c Config loaded. ', log_style_done);