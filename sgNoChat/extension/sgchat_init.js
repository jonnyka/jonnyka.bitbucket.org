if (window.location.href === 'https://sg.hu/forum/' && window.location.pathname === '/forum/') {
    Object.defineProperty(window, 'chatLoad', {
        value: function(){}
    });

    window.addEventListener('load', onLoad, false);

    function onLoad(evt) {
        var jsInitChecktimer = setInterval(checkForJSFinish, 111);

        function checkForJSFinish () {
            if ($ && $('#userdata').length) {
                clearInterval (jsInitChecktimer);

                if ($('#userdata').data('uid')) {
                    var baseUrl = 'https://print.hu/chat/';
                    require([baseUrl + 'sgchat_include.js?rand=' + Math.random()]);
                }
            }
        }
    }
}
