var requireQueue = function(modules, callback) {
    function load(queue, results) {
        if (queue.length) {
            require([queue.shift()], function(result) {
                results.push(result);
                load(queue, results);
            });
        } else {
            callback.apply(null, results);
        }
    }

    load(modules, []);
};

var baseUrl = 'https://print.hu/chat/';
requireQueue([
    baseUrl + 'sgchat_config.js?rand=' + Math.random(),
    baseUrl + 'sgchat_common_functions.js?rand=' + Math.random(),
    baseUrl + 'sgchat_chat_functions.js?rand=' + Math.random(),
    baseUrl + 'sgchat_options.js?rand=' + Math.random(),
    '//cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.12/jquery.mousewheel.min.js',
    '//cdnjs.cloudflare.com/ajax/libs/jScrollPane/2.0.22/script/jquery.jscrollpane.min.js',
    baseUrl + 'sgchat_base.js?rand=' + Math.random()
], function() {
    jChatLoad();
});