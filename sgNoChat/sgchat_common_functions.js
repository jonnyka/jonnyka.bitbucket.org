console.log('%c Functions loading started... ', log_style_start);

/**
 * Includes a js file.
 * @param baseUrl
 * @param jsFile
 */
function includeJs(jsFile, baseUrl) {
    var jsNode = document.createElement("script");
    jsNode.type = "text/javascript";
    jsNode.src = baseUrl + jsFile;
    if (baseUrl === undefined) {
        jsNode.src = jsFile;
    }
    else {
        jsNode.src = baseUrl + jsFile;
    }
    jsNode.async = false;
    document.body.appendChild(jsNode);
}

/**
 * Includes a css file.
 * @param cssFile
 * @param baseUrl
 */
function includeCss(cssFile, baseUrl) {
    var head = document.getElementsByTagName('head')[0];
    var link = document.createElement('link');
    var id = 'base_' + cssFile;
    if (baseUrl === undefined) {
        id = 'remote_' + cssFile;
    }
    link.id = id;
    link.rel = 'stylesheet';
    link.type = 'text/css';
    if (baseUrl === undefined) {
        link.href = cssFile;
    }
    else {
        link.href = baseUrl + cssFile + '?rand=' + Math.random();
    }
    link.media = 'all';
    head.appendChild(link);
}

/**
 * Disables a local css file.
 * @param cssFile
 */
function disableCss(cssFile) {
    $('link[href*="' + cssFile + '"]').remove();
}

/**
 * Enables a local css file.
 * @param cssFile
 * @param baseUrl
 */
function enableCss(cssFile, baseUrl) {
    var href = '#base_' + cssFile;
    var isLoaded = $(href);

    if (isLoaded.length > 0) {
        isLoaded.attr('disabled', false);
    }
    else {
        includeCss(cssFile, baseUrl);
    }
}

/**
 * Loads a custom css file, or disables all of them.
 */
function loadCustomLocalCss() {
    var customCss = getSetting('custom_css');

    for (var css in customCssFiles) {
        for (var cs in customCssFiles[css]) {
            disableCss(cs);
        }
    }

    if (customCss && customCss != 'default') {
        enableCss(customCss + '.css', baseUrl + 'custom_css/');
    }

    if (customCss == 'TimeToColor') {
        TimeToColor();
    }

    if (customCss != 'TimeToColor') {
        removeTimeToColor();
    }

    if (customCss != 'xevoDes') {
        removexevoDes();
    }

    if (customCss == 'xevoDes') {
        xevoDes();
    }

    if (customCss == 'lines') {
        includeCss('linesScrollbar.css', baseUrl + 'custom_css/');
    }

    if (customCss == 'material_empty') {  
        MaterialDesign();
        
        // @Override MDL if necessary
        includeCss('material.css', baseUrl + 'custom_css/');
    }

    if (customCss == 'material_empty_dark') {  
        MaterialDesign();
        
        // @Override MDL if necessary
        includeCss('material_dark.css', baseUrl + 'custom_css/');
    }
}

/**
 * Sets a localStorage array setting.
 * @param name
 * @param value
 */
function addSetting(name, value) {
    var object = localStorage.getItem(name);
    var array = [];
    if (object) {
        array = JSON.parse(object).value;
    }

    array.push(value);

    var testObject = { 'value': array };
    localStorage.setItem(name, JSON.stringify(testObject));
}

/**
 * Removes a localStorage array setting.
 * @param name
 * @param value
 */
function removeSetting(name, value) {
    var object = localStorage.getItem(name);
    var array = [];
    if (object) {
        array = JSON.parse(object).value;
    }

    for (var i = array.length - 1; i >= 0; i--) {
        if(array[i] === value) {
            array.splice(i, 1);
        }
    }

    var testObject = { 'value': array };
    localStorage.setItem(name, JSON.stringify(testObject));
}

/**
 * Sets a localStorage setting.
 * @param name
 * @param value
 */
function setSetting(name, value) {
    var testObject = { 'value': value };
    localStorage.setItem(name, JSON.stringify(testObject));
}

/**
 * Gets a localStorage setting.
 * @param name
 * @returns {*}
 */
function getSetting(name) {
    var retrievedObject = localStorage.getItem(name);
    if (!retrievedObject) {
        return false;
    }

    var jsonObject = JSON.parse(retrievedObject);
    return jsonObject.value;
}

/**
 * Sets a checkbox's checked status based on a localStorage setting.
 * @param name
 * @returns {string}
 */
function checkedVal(name) {
    var ret = '';
    if (getSetting(name) == 1) {
        ret = 'checked="checked"';
    }
    return ret;
}


/**
 * PHP's inArray function.
 * @param needle
 * @param haystack
 * @returns {boolean}
 */
function inArray(needle, haystack) {
    var length = haystack.length;

    for (var i = 0; i < length; i++) {
        if (haystack[i] == needle) {
            return true;
        }
    }

    return false;
}

var newSettings = 0;
var modUser = false;

/**
 * Initializes all localStorage settings.
 */
function initSettings() {
    for (var setting in localStoreSettings) {
        var name = localStoreSettings[setting]['id'];
        if (getSetting(name) === false && (!localStoreSettings[setting]['mod'] || modUser)) {
            var value = localStoreSettings[setting]['default'];
            initSetting(name, value);
            newSettings++;
        }
    }

    if (getSetting('lines_color') === false) {
        setSetting('lines_color', '#1DA2C8');
    }
}

/**
 * Initializes a localStorage setting.
 * @param name
 * @param val
 */
function initSetting(name, val) {
    val = val || 0;
    if (!getSetting(name)) {
        setSetting(name, val);
    }
}

/**
 * Create modal window
 */
function createModal(data, noScrollDiv){
    var shade, modal, close;
    var body = $('body');

    shade = $('<div />', {
        class: "modal-shade"
    }).on('click', function() {
        removeModal();
        forumChatInput.focus();
    });

    modal = $('<div />', {
        class: "modal-cont",
        html: data()
    });

    close = $('<span />', {
        class: "modal-close",
        html: '<i></i>'
    }).on('click', function() {
        removeModal();
        forumChatInput.focus();
    });

    body.prepend(shade, modal);
    modal.prepend(close);

    shade.fadeIn(500);
    modal.fadeIn(500);

    lines_color();

    if (noScrollDiv) {
        bindNoScroll(modal.find('.options')[0], otherScrollSpeed);
    }
}

/**
 * Remove modal window
 */
function removeModal() {
    $('.modal-shade').fadeOut(500, function(){
        $('.modal-shade').remove();
        $('.modal-shade').dequeue();
    });

    $('.modal-cont').fadeOut(500, function(){
        $('.modal-cont').remove();
        $('.modal-cont').dequeue();
    });
}

/**
 * Shortcut for getting elements.
 * @param id
 */
function elem(id) {
    if (id.indexOf('.') === 0) {
        return document.getElementsByClassName(id.substring(1));
    }
    return document.getElementById(id);
}

/**
 * Get current date formatted
 */
function getCurrentDate() {
    var date = new Date();
    var year = date.getFullYear();
    var month = formatDatePart(date.getMonth() + 1);
    var day = formatDatePart(date.getDate());
    var hour = formatDatePart(date.getHours());
    var minute = formatDatePart(date.getMinutes());
    var second = formatDatePart(date.getSeconds());

    return year + '.' + month + '.' + day + ' ' + hour + ':' + minute + ':' + second;
}

/**
 * Formats date parts with leading zeros.
 * @param part
 * @returns {string}
 */
function formatDatePart(part) {
    return part < 10 ? '0' + part : part;
}

/**
 * Removes spaces from a username.
 * @param username
 * @returns string
 */
function fixUserName(username) {
    return username.replace(/ /g, '_');
}

/**
 * Insert BBCodes before and after the selection
 */
function iBBCodes(bb) {
    var input = $('#forum-chat-input'),
        txt = input.val(),
        sel = input.selection(),
        sel_n = sel.replace(sel, '[' + bb + ']' + sel + '[/' + bb + ']');

    if (bb == 'color') {
        sel_n = sel.replace(sel, '[' + bb + '=red]' + sel + '[/' + bb + ']');
    }
    if (bb.indexOf('font') >= 0) {
        sel_n = sel.replace(sel, '[' + bb + ']' + sel + '[/font]');
    }

    if (!sel) {
        txt = txt + sel_n;
    }
    else{
        txt = txt.replace(sel, sel_n);
    }

    input.val(txt);
    
    if (bb === 'spoiler') {
        setCaretPosition('forum-chat-input', 9);
    }

    forumChatInput.focus();
}

/*
 * Time to HTML Background color
 */

var setTimeToColor;
function TimeToColor() {
    var html = $('html'),
        content = $('#content'),
        now = new Date(),
        h = now.getHours(),
        m = now.getMinutes(),
        s = now.getSeconds();

    if (h < 10){
        h = '0' + h;
    }
    if (m < 10){
        m = '0' + m;
    }
    if (s < 10){
        s = '0' + s;
    }
    var color = '#' +  h + '' + m + '' + s;
    html.attr('style','background:' + color + ' !important');
    setTimeToColor = setTimeout('TimeToColor()', 1000);
}

function removeTimeToColor() {
    clearTimeout(setTimeToColor);
    var html = $('html');
    html.removeAttr('style');
}

/*
 * Git RSS Check & notify if something new
 */
function rssChk() {
    var url = '//bitbucket.org/jonnyka/jonnyka.bitbucket.org/rss?token=7c3624ba10e2daa5bb8eaa83413b472a#' + Math.random();
    $.ajax({
        type: "get",
        url: document.location.protocol + '//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=1000&callback=?&q=' + encodeURIComponent(url),
        dataType: 'json',
        cache: false,
        async: false,
        error: function () {
            console.log('Unable to load feed...');
        },
        success: function (xml) {
            var obj = xml.responseData.feed.entries[0],
                title = obj.title,
                pubDate = obj.publishedDate,
                oldDate = getSetting('rss-date');

            if (pubDate != oldDate) {
                var notify;

                notify = '<div id="notify">';
                notify += '<span>Elérhető frissítés a cset addonhoz! Letöltéshez frissítsd az oldalt.';
                notify += '<button id="notifyRefresh" class="notifyButton refresh">Frissítés</button><button id="notifyCancel" class="notifyButton cancel">Mégsem</button>';

                if (title.indexOf('p:') >= 0) {
                    title = title.replace('p:','');

                    notify += '<div>Leírás: ' + title + '</div>';
                }

                notify += '</span></div>';

                if(document.getElementById("notify") == null) {
                    $('#forum-chat section.main-window').prepend(notify);
                    $('#notify').slideToggle(500);
                }

                $("#notifyRefresh").click(function() {
                    setSetting('rss-date', pubDate);
                    location.reload();
                });

                $("#notifyCancel").click(function() {
                    $('#notify').fadeOut(500);
                });
                
                $(window).bind("beforeunload", function(){
                        return setSetting('rss-date', pubDate); 
                });
            }
        }
    });
    setTimeout('rssChk()', 300000);
}
//rssChk();

/*
 * Reordering input & submit fields
 */
function inputToDiv() {
    var cont = '<div id="inputs" class="pull-left cf"></div>',
        input = $('#forum-chat-input'),
        submit = $('#forum-chat-send-message'),
        input_cont = '<div id="input_cont"></div>';

    submit.removeClass('btn btn-info');
    $('#forum-chat').append(cont);
    $('#inputs').append(input_cont);
    $('#input_cont').append(input);
    $('#input_cont').append(submit);
}

/*
 * Typeahead - OFF
 *

function typeahead() {
    var easters = ['/me Lorem','!loál','!rave','!nyan','!grayscale','!invert','!barrelroll','!flipcoin','!pwt'];

    for (var i in songs) {
        var song = songs[i]['str'];
        easters.push('!' + song);
    }

    //console.log(easters);

    var easter = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,

        local: $.map(easters, function(data) { return { value: data }; })
    });

    easter.initialize();

    $('#input_cont #forum-chat-input').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'easters',
            displayKey: 'value',
            source: easter.ttAdapter()
        });
}
*/

function toggleSections() {
    var section = $('#neosidebar .forum-topics-block').children();
    section.each(function() {
        var header = $(this).children('header'),
            n_id = header.text();
        n_id = n_id.replace(/\n/g, '');
        n_id = n_id.replace(/ /g, '');

        header.attr('id', n_id);
        header.css('cursor', 'pointer');
        header.prepend('<span id="plus_minus_icon">–</span>');

        header.each(function(){
            $(this).click(function(){
                var text = $(this).children('#plus_minus_icon').text(),
                    id = $(this).attr('id');

                $(this).children('#plus_minus_icon').text(text == "–" ? "+" : "–");

                $(this).next('ul').slideToggle();

                if (text == '+') {
                    setSetting(id, 1);
                } else if (text == '–') {
                    setSetting(id, 0);
                }
            });
        });

        if(getSetting(n_id) == 0) {
            $('#' + n_id).next('ul').css('display', 'none');
            $('#' + n_id).children('#plus_minus_icon').text('+');
        }

    });

    var hbd = $('#neosidebar #sidebar-birthday h4');
    hbd.css('cursor', 'pointer');
    hbd.attr('id', 'birthday');

    hbd.prepend('<span id="plus_minus_icon">–</span>');

    hbd.click(function(){
        var text = $(this).children('#plus_minus_icon').text(),
            id = $(this).attr('id');

        $(this).children('#plus_minus_icon').text(
            text == "–" ? "+" : "–");

        $(this).next('div').slideToggle();

        if(text == '+'){
            setSetting(id, 1);
        } else if (text == '–') {
            setSetting(id, 0);
        }
    });

    if(getSetting('birthday') == 0) {
        $('#neosidebar #birthday-wrap').css('display', 'none');
        $('#neosidebar #birthday').children('#plus_minus_icon').text('+');
    }
}

/*
 * xevoDes téma
 */
var setxevoDes;
function xevoDes() {
    setSetting('responsive', 1);

    var fifty_o = innerHeight/2 - 36,
        fifty_n = fifty_o + 46;
    $('.online-list').attr('style', 'height:' + fifty_o + 'px !important');
    $('#neosidebar').attr('style', 'top:' + fifty_n + 'px');

    if(!$('#forum-wrap #sidebar-user').length) {
        $('#neosidebar #sidebar-user').prependTo('#forum-wrap');
    }

    $('#forum-chat-list li').each(function(){
        var time = $(this).children('time').width(),
            sender = $(this).children('.sender').width(),
            msg = $(this).children('.msg'),
            margin = 200 - time - sender;

        if (time != null) {
            if(msg.height() > 25){
                msg.attr('style', 'margin-left: 200px; margin-top: -20px; display: inline-block');
            } else {
                msg.attr('style', 'margin-left: ' + margin + 'px');
            }
        }

        if ($('#forum-chat-list li .msg iframe').length) {
            if (!$(this).hasClass('ytiframe') || !$(this).hasClass('vmiframe')){
                $('#forum-chat-list li .msg iframe').parent().removeAttr('style');
            }
        }
        else if (!$(this).hasClass('player_cont')){
            $('#forum-chat-list li .msg .player_cont').parent().removeAttr('style');
        }
    });

    $('#options_custom_css').next('.help').addClass('warning');
    $('#options_custom_css').next().next('.help-msg').text('Ezzel a témával pár beállítás nem elérhető!');

    setxevoDes = setTimeout('xevoDes()', 1000);
}

function removexevoDes() {
    clearTimeout(setxevoDes);

    $('.online-list').removeAttr('style');
    $('#neosidebar').removeAttr('style');

    $('#forum-wrap #sidebar-user').remove();
    $('#header').prependTo('body');

    $('#forum-chat-list li .sender').removeAttr('style');
    $('#forum-chat-list li .msg').removeAttr('style');

    $('#options_custom_css').next('.help').removeClass('warning');
    $('#options_custom_css').next().next('.help-msg').text('A főoldal kinézete.');
}

/**
 * Material Design source fuck up 
 */
// waitUntilExists() neosidebar miatt...
(function(e,f){var b={},g=function(a){b[a]&&(f.clearInterval(b[a]),b[a]=null)};e.fn.waitUntilExists=function(a,h,j){var c=this.selector,d=e(c),k=d.not(function(){return e(this).data("waitUntilExists.found")});"remove"===a?g(c):(k.each(a).data("waitUntilExists.found",!0),h&&d.length?g(c):j||(b[c]=f.setInterval(function(){d.waitUntilExists(a,h,!0)},500)));return d}})(jQuery,window);

function MaterialDesign() {
    // resetresponsive...
    var reset = [
        $('.jsg-csetname-old'),
        $('#forum-chat-input'),
        $('.tt-dropdown-menu'),
        $('#forum-chat > .main-window'),
        $('#forum-chat > #inputs'),
        $('#forum-chat'),
        $('#sgTopicFrameBtn'),
        $('#sgTopicFrame'),
        $('#content'),
        $('#header'),
        $('#forum-wrap .forums-block'),
        $('#forum-wrap .forums-block header'),
        $('#forum-wrap .forums-block .col1'),
        $('#forum-wrap .forums-block > header .col1'),
        $('.online-wrap header'),
        $('#forum-block-wrapper-button'),
        $('.easteregg'),
        $('.gif_tag'),
        $('.bbcs'),
        $('#gif_search'),
        $('.optionsButton'),
        $('#input_cont span'),
        $('#input_cont button'),
        $('#sidebar-user nav header'),
        $('#sidebar-user-favorites h4 span #icon'),
        $('.forum-topics-block section header'),
        $('#sidebar-birthday h4')
    ];

    for (var i in reset) {
        reset[i].removeAttr('style');
    }

    $('#options_responsive').waitUntilExists(function() {
        var optionsRemove = [
            $('#options_responsive'),
            $('#options_header'),
            $('#options_fbbox'),
            $('#options_jedi'),
            $('#options_articles'),
            $('#options_gallery')
        ];

        for (var i in optionsRemove) {
            optionsRemove[i].parents('.row').css('display', 'none');
        }
    });


    var csetUsers = $('.online-wrap');
    csetUsers.removeAttr('id');
    csetUsers.insertAfter('.main-window');


    $(window).unbind('resize');
    $(window).resize(function () {
        return true;
    });

    setSetting('responsive', 0);
    setSetting('header', 0);
    setSetting('jedi', 0);
    setSetting('fbbox', 0);
    setSetting('gallery', 0);

    includeCss('https://fonts.googleapis.com/icon?family=Material+Icons');
    includeCss('https://fonts.googleapis.com/css?family=Roboto:400,500,700&subset=latin,latin-ext');
    includeCss('lib/material.blue-red.min.css', baseUrl);
    includeJs('lib/material-min.js', baseUrl);

    $('.modal-cont .options').waitUntilExists(function() {
        var checked = (getSetting('asidebar') == 1) ? 'checked' : '',
            material_option = '<span class="row">';
            material_option += '<div class="options_chat_label">Sidebar pozíció:</div>';
            material_option += '<label><input id="options_asidebar" type="checkbox" ' + checked + '><i></i></label>';
            material_option += '<i class="help"></i>';
            material_option += '<span class="help-msg">A sidebar pozíciója jobb vagy bal oldalra.</span>';
            material_option += '</span>';

        $(this).children(':nth-child(6)').after(material_option);
    });

    var separator = '<div class="material_separator mdl-cell--1-col"></div>',
        layout_container = '<div id="layout_container" class="mdl-layout mdl-js-layout has-drawer is-upgraded"></div>',
        main = '<main id="main" class="mdl-layout__content"></main>',
        grid = '<div id="grid" class="mdl-grid"></div>',
        content = $('#content'),
        csetUsers = $('.online-wrap'),
        sidebar = '<div id="asidebar" class="mdl-cell mdl-cell--8-col mdl-cell--2-col-desktop"></div>',
        sidebarUser = $('#content #sidebar-user'),
        sidebarFav = $('#content #sidebar-user-favorites');

    $('#header').removeClass('page-center');

    if (!$('#layout_container').length) {
        $('#content').before(layout_container);
        $('#layout_container').append(main);
        $('main#main').append(grid);

        $('#grid').append(content);
    }


    $('#grid #content').after(csetUsers);

    if (!$("#asidebar").length) {
        if (getSetting('asidebar') == 1) {
            $('#grid').append(sidebar);
        } else {
            $('#grid').prepend(sidebar);
        }
    }

    content.addClass('mdl-cell mdl-cell--10-col');
    $('#grid #content #forum-wrap #forum-chat').addClass('mdl-shadow--2dp');
    $('#grid .online-wrap header').addClass('mdl-shadow--2dp');
    $('#grid .online-wrap .online-list').addClass('mdl-shadow--2dp');

    $('#neosidebar').waitUntilExists(function() {
        $('#asidebar').append($(this));
    });

    $("#asidebar #sidebar-user nav a").mouseover(function(){
        $(this).addClass('mdl-shadow--2dp');
    });
    $("#asidebar #sidebar-user nav a").mouseout(function(){
        $(this).removeClass('mdl-shadow--2dp');
    });

    $("#asidebar #sidebar-user-favorites #favorites-list span a").mouseover(function(){
        $(this).addClass('mdl-shadow--2dp');
    });
    $("#asidebar #sidebar-user-favorites #favorites-list span a").mouseout(function(){
        $(this).removeClass('mdl-shadow--2dp');
    });
}

function chatnameColorReplace() {
    function rgbCalc(color) {
        var rgb = color.replace(/^(rgb|rgba)\(/,'').replace(/\)$/,'').replace(/\s/g,'').split(','),
            sum_rgb = 0;

        for(var i in rgb) {
            sum_rgb += Number(rgb[i]);
        }

        return sum_rgb;
    }

    $('#grid .online-wrap .online-list .frame ul#forum-chat-user-list li a.chatname').each(function() {
        var chatname_color = $(this).css('color');

        if (rgbCalc(chatname_color) >= 600 && $(this).attr('nick') != 'pxxl') {
            $(this).css('text-shadow', '1px 1px #333, -1px -1px 0 #333, 1px -1px 0 #333, -1px 1px 0 #333');
        }
    });

    $('#grid #content #forum-wrap #forum-chat #list-frame #forum-chat-list li span.sender').each(function() {
        var sender_color = $(this).css('color');

        if (rgbCalc(sender_color) >= 600 && $(this).html() != 'pxxl') {
            $(this).css('text-shadow', '1px 1px #333, -1px -1px 0 #333, 1px -1px 0 #333, -1px 1px 0 #333');
        }
    });
}

/**
 * strip html tags from a script
 * @param html
 * @returns {string|string}
 */
function strip(html) {
    var tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
}

/**
 * Calculates a user's age.
 *
 * @param birthYear
 * @param birthMonth
 * @param birthDay
 * @returns {number|*}
 */
function calculateAge(birthYear, birthMonth, birthDay) {
    var todayDate = new Date();
    var todayYear = todayDate.getFullYear();
    var todayMonth = todayDate.getMonth();
    var todayDay = todayDate.getDate();
    var age = todayYear - birthYear;
    birthDay = parseInt(birthDay.substring(0, birthDay.indexOf('T')));

    if (todayMonth < birthMonth - 1) {
        age--;
    }

    if (birthMonth - 1 == todayMonth && todayDay < birthDay) {
        age--;
    }

    return age;
}

/**
 * Gets operating system.
 * @returns {string}
 */
function getOs() {
    var os = 'windows';

    if (navigator) {
        if (navigator.platform.match(/iPhone|iPad|iPod|Android|BlackBerry|webOS/i)) {
            os = 'phone';
        }
        else if (navigator.platform.match(/Macintosh|MacIntel|MacPPC|Mac68K/i)) {
            os = 'mac';
        }
        else if (navigator.platform.match(/Linux|FreeBSD|OpenBSD|SunOS/i)) {
            os = 'linux';
        }
    }

    return os;
}

/**
 * Converts hex color to rgb
 *
 * @param hex
 * @returns {{r: Number, g: Number, b: Number}}
 */
function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

initSettings();
includeCss('sgchat.css', baseUrl);
includeCss('lib/jquery.jscrollpane.min.css', baseUrl);
includeCss('smile_fontelico.css', baseUrl + 'custom_css/');

loadCustomLocalCss();

console.log('%c Functions loaded. ', log_style_done);