console.log('%c Chat command functions loading started... ', log_style_start);

/**
 * Responsive blocks resize.
 */
function blocksResponsive() {
    if ($(window).width() < 1880) {
        $('#forum-wrap .forums-block .col1').attr('style', 'width: 42% !important');
        $('#forum-wrap .forums-block > header .col1').attr('style', 'font-size: 13px !important; width: 42% !important');
    }
    if ($(window).width() < 1590) {
        $('#forum-wrap .forums-block').removeAttr('style');
        $('#forum-wrap .forums-block header').removeAttr('style');
        $('#forum-wrap .forums-block .col1').removeAttr('style');
        $('#forum-wrap .forums-block .col2').removeAttr('style');
        $('#forum-wrap .forums-block .col3').removeAttr('style');
        $('#forum-wrap .forums-block > header .col1').removeAttr('style');
    }
}

/**
 * Sets responsive design.
 */
function setResponsive() {
    if (!isResponsive) {
        var csetUsers = $('.online-wrap');
        csetUsers.appendTo('body');
        csetUsers.attr('id', 'sgcset-lista');
        var winWidth = $(window).width();

        function resizeChatArea() {
            winWidth = $(window).width();
            var outerWidth = winWidth - 433;
            var innerWidth = outerWidth - 20;
            var chatInputWidth = innerWidth - 272;
            $('#header').css('width', outerWidth);
            $('#content').css('width', outerWidth);
            //$('#jsg-utolso-regisztraltak').css('width', innerWidth);
            $('#forum-chat').css('width', innerWidth);
            $('#forum-chat > .main-window').css('width', innerWidth);
            $('#forum-chat-list').css('width', innerWidth);
            $('#forum-chat > #inputs').css('width', innerWidth + 2);
            $('#sgTopicFrameBtn').css('width', innerWidth);
            $('#sgTopicFrame').css('width', innerWidth);
            $('#forum-chat-input').css('width', chatInputWidth + 49);
            $('.tt-dropdown-menu').css('width', chatInputWidth + 94);
            $('.jsg-csetname-old').css('max-width', '180px');

            $('#forum-wrap .forums-block').attr('style', 'width: 32% !important; margin: 20px .6% !important; float: left !important');
            $('#forum-wrap .forums-block header').attr('style', 'padding: 10px !important; background-color: rgba(29, 162, 200, .2)');
            $('#forum-wrap .forums-block .col1').attr('style', 'width: 45% !important; box-sizing: border-box');
            $('#forum-wrap .forums-block .col2').attr('style', 'width: 29% !important; box-sizing: border-box');
            $('#forum-wrap .forums-block .col3').attr('style', 'width: 22% !important; box-sizing: border-box');
            $('#forum-wrap .forums-block > header .col1').attr('style', 'width: 45% !important; box-sizing: border-box');
            $('#forum-wrap .forums-block > header .col2').attr('style', 'width: 34% !important; box-sizing: border-box');
            $('#forum-wrap .forums-block > header .col3').attr('style', 'width: 18% !important; box-sizing: border-box');

            blocksResponsive();
        }

        $(window).resize(resizeChatArea);

        resizeChatArea();
    }
    else {
        $(window).unbind('resize');
        $(window).resize(function () {
            return true;
        });
        isResponsive = false;
    }
}

/**
 * Resets responsive design.
 */
function resetResponsive() {
    $('.jsg-csetname-old').css('max-width', '100px');
    $('#forum-chat-input').css('width', 735);
    $('.tt-dropdown-menu').css('width', 750);
    $('#forum-chat > .main-window').css('width', 675);
    $('#forum-chat > #inputs').css('width', '100%');
    $('#forum-chat').css('width', '900px');
    $('#sgTopicFrameBtn').css('width', '900px');
    $('#sgTopicFrame').css('width', '900px');
    //$('#jsg-utolso-regisztraltak').css('width', '900px');
    $('#content').css('width', '');
    $('#header').css('width', '');

    $('#forum-wrap .forums-block').removeAttr('style');
    $('#forum-wrap .forums-block header').removeAttr('style');
    $('#forum-wrap .forums-block .col1').removeAttr('style');
    $('#forum-wrap .forums-block > header .col1').removeAttr('style');

    var csetUsers = $('.online-wrap');
    csetUsers.removeAttr('id');
    csetUsers.insertAfter('.main-window');

    $(window).unbind('resize');
    $(window).resize(function () {
        return true;
    });

    isResponsive = false;
}

/**
 * Show sg iFrame.
 *
 * @param href
 */
function showFrame(href) {
    var frameBtn = $('#sgTopicFrameBtn');
    var topicFrame = $("#sgTopicFrame");

    topicFrame.attr("src", href + "#forum-wrap");
    topicFrame.show();
    frameBtn.css('display', 'block');
    if (!frameBtn.hasClass('open')) {
        frameBtn.addClass('open');
    }
}

/**
 * Open sg links in iFrame.
 */
function sgLinksToIframe(clickables) {
    clickables.click(function (e) {
        e.preventDefault();
        showFrame($(this).attr('href'));
    });
    $('#favorites-list a').click(function () {
        updateSidebar();
    });
}

/**
 * Open sg links in self.
 *
 * @param clickables
 */
function sgLinksToSelf(clickables) {
    sgLinksReset(clickables);

    clickables.each(function () {
        $(this).attr('target', '_self');
    });
}

/**
 * Open sg links in blank background.
 *
 * @param clickables
 */
function sgLinksToBlank(clickables) {
    sgLinksReset(clickables);

    clickables.each(function () {
        $(this).attr('target', '_blank');
    });
}

/**
 * Resets sg links.
 */
function sgLinksReset(clickables) {
    $("#sgTopicFrame").hide();
    $('#sgTopicFrameBtn').hide();
    clickables.unbind('click');
    $('#favorites-list a').click(function () {
        updateSidebar();
    });
}

/**
 * Updates sg links realtime.
 */
function updateSgLinks() {
    var clickables = $('.sglinked > a, #favorites-list > span > a, .forum-topics-block li > a, #sidebar-user a:not(".notClickable"), .forums-block a, #birthday-wrap > a, #sidebar-sg-new-galleries > nav > a');

    if (getSetting('sgiframe') == 1) {
        sgLinksToIframe(clickables);
    }
    else if (getSetting('sgiframe') == 2) {
        sgLinksToBlank(clickables);
    }
    else {
        sgLinksToSelf(clickables, '_self');
    }
}

/**
 * Shows / hides fb box.
 */
function fbToggle() {
    $('#forum-fb-likebox').slideToggle();
}

/**
 * Shows / hides Jedi Mester remembering.
 */
function jediToggle() {
    $('#jedimester').slideToggle();
}

/**
 * Updates sidebar boxes realtime.
 */
function updateBoxes() {
    var topics = $('#neosidebar > .forum-topics-block');
    var newTopics = $(topics.find('section')[0]);
    var coolTopics = $(topics.find('section')[1]);
    var birthdays = $('#neosidebar #sidebar-birthday');
    var articles = $('.articles-block');

    if (getSetting('sidebar_newtopics') == 0) {
        newTopics.hide();
    }
    else {
        newTopics.show();
    }

    if (getSetting('sidebar_cooltopics') == 0) {
        coolTopics.hide();
    }
    else {
        coolTopics.show();
    }

    if (getSetting('sidebar_birthdays') == 0) {
        birthdays.hide();
    }
    else {
        birthdays.show();
    }

    if (getSetting('articles') == 0) {
        articles.hide();
    }
    else {
        articles.show();
    }
}

/**
 * Update topic boxes.
 */
function updateTopics(update) {
    var curVal = getSetting('topics');

    if (update) {
        if (curVal == 0) {
            curVal = 1;
        }
        else {
            curVal = 0;
        }
        setSetting('topics', curVal);
    }

    var topicBoxes = $('#forums-block-wrapper');
    var topicButton = $('#forum-block-wrapper-button');
    var txt = 'Témák listája ';

    if (curVal) {
        topicBoxes.show();
        topicButton.text(txt + '\u2193');
    } else {
        topicBoxes.hide();
        topicButton.text(txt + '\u2191');
    }
}

/**
 * Toggle snow effect
 */
function toggleSnow(checkSetting) {
    var name = 'snow',
        selector = $('#' + name),
        go = checkSetting ? getSetting('snow') == 1 : true;

    if (!selector.length && go) {
        $('<div id="snow"></div>').appendTo('#header-bottom');
    }
    else {
        selector.remove();
    }
}

/**
 * Update gallery box.
 */
function updateGallery() {
    var gallery = $('#neosidebar > #sidebar-sg-new-galleries');

    if (getSetting('gallery') == 0) {
        gallery.hide();
    }
    else {
        gallery.show();
    }
}

function addEvent(el, type, handler) {
    if (el.attachEvent) el.attachEvent('on'+type, handler); else el.addEventListener(type, handler);
}
function removeEvent(el, type, handler) {
    if (el.detachEvent) el.detachEvent('on' + type, handler); else el.removeEventListener(type, handler);
}

function bindNoScroll(element, speed) {
    removeEvent(element, 'mousewheel DOMMouseScroll');
    addEvent(element, 'mousewheel DOMMouseScroll', function (e) {
        var scrollTo = null;

        if (e.type == 'mousewheel') {
            scrollTo = (e.originalEvent.wheelDelta * -1);
        }
        else if (e.type == 'DOMMouseScroll') {
            scrollTo = speed * e.originalEvent.detail;
        }

        if (scrollTo) {
            e.preventDefault();
            this.scrollTop = this.scrollTop + scrollTo;
        }
    });
}

/**
 * Show clock for the bitches
 */
function showTime() {
    var date = new Date();
    var h = date.getHours(); // 0 - 23
    var m = date.getMinutes(); // 0 - 59
    var s = date.getSeconds(); // 0 - 59

    if (h === 0){
        h = 12;
    }

    h = (h < 10) ? '0' + h : h;
    m = (m < 10) ? '0' + m : m;
    s = (s < 10) ? '0' + s : s;

    var time = h + ":" + m + ":" + s;
    document.getElementById('clock').innerText = time;
    document.getElementById('clock').textContent = time;

    setTimeout(showTime, 1000);
}

/**
 * Updates jScrollPane
 */
function updateJscroll() {
    var neoside = $('#neosidebar');

    neoside.jScrollPane({
        mouseWheelSpeed: 50,
        arrowButtonSpeed: 50
    });
    var neoApi = neoside.data('jsp');
    if (neoApi) {
        neoApi.destroy();
    }
    $('#neosidebar').jScrollPane({
        mouseWheelSpeed: 50,
        arrowButtonSpeed: 50
    });

    scrollToBottom();
}

/**
 * Realtime lines recolor.
 */
function lines_color(fromSettings) {
    var c = getSetting('lines_color');
    if (fromSettings) {
        c = $("#options_linescolor").attr('col');
    }

    if (getSetting('custom_css') == 'lines') {
        var fcs = $('#forum-chat');
        var sgl = $('#sgcset-lista').find('header');
        var suf = $('#sidebar-user-favorites');
        var sch = $('#sgcset-header');
        var fbwb = $('#forum-block-wrapper-button');
        var bbt = $('.blue-border-top');
        var con = $('#content');
        var fw = $('#forum-wrap');
        var inp = $('#inputs #input_cont #forum-chat-send-message');
        var eag = $('.easteregg');
        var gif = $('.gif_tag');
        var bbcs = $('.bbcs');
        var gifs = $('#gif_search');
        var obn = $('.optionsButton');
        var fav = $('#sidebar-user-favorites a.category');

        // color
        var sColor = [
            $('h2.sgneojonny'),
            $('span.row'),
            $('.options_chat_label'),
            $('h3.title'),
            $('.modal-close'),
            $('.player_btn'),
            $('h4.alt_title'),
            $('div.player_cont'),
            $('span.title'),
            fbwb,
            inp,
            eag,
            gif,
            bbcs,
            gifs,
            obn,
            fav,
            $('#forum-wrap .forums-block > header .col1'),
            fcs.find('.main-window a'),
            sch.find('a.lefthead'),
            sch.find('a.righthead'),
            $('.extras_label'),
            $('.extras_desc')
        ];
        for (var i in sColor) {
            sColor[i].each(function () {
                this.style.setProperty('color', c, '');
            });
        }

        // border 1px solid
        var sBorder1 = [
            fbwb,
            eag,
            gif,
            bbcs,
            gifs,
            obn
        ];
        for (var i in sBorder1) {
            sBorder1[i].each(function () {
                this.style.setProperty('border', '1px solid ' + c, '');
            });
        }

        // border 1px solid important
        var sBorder1Important = [
            //$('#inputs'),
            $('#input_cont input'),
            $('#smileyButton'),
            $('#songButton'),
            $('#gifButton'),
            $('#bbcodesButton'),
            $('#spoilerButton'),
            $('.spoilerText'),
            $('.spoilerContentBlock'),
            $('.player_cont'),
            $('#birthday-wrap a'),
            inp
        ];
        for (var i in sBorder1Important) {
            sBorder1Important[i].each(function () {
                this.style.setProperty('border', '1px solid ' + c, 'important');
            });
        }

        // border-bottom 1px solid important
        var sBorderBottom1Important = [
            sgl,
            fcs.find('.main-window'),
            fcs.find('#forum-chat-input'),
            fcs.find('#forum-chat-send-message'),
            $('#currentThread'),
            $('header'),
            suf.find('#favorites-open-close-button #icon'),
            $('.headers-medium, h3.title'),
            $('.headers-medium'),
            $('#sidebar-user-favorites #favorites-open-close-button #icon')
        ];
        for (var i in sBorderBottom1Important) {
            sBorderBottom1Important[i].each(function () {
                this.style.setProperty('border-bottom', '1px solid ' + c, 'important');
            });
        }

        // border-bottom 0 important
        var sBorderBottom0Important = [
            bbt,
            fw.find('.blue-border-top')
        ];
        for (var i in sBorderBottom0Important) {
            sBorderBottom0Important[i].each(function () {
                this.style.setProperty('border-bottom', '0', 'important');
            });
        }

        // border-bottom 1px dashed important
        var sBorderBottom1Dashed = [
            suf.find('a.category'),
            fav
        ];
        for (var i in sBorderBottom1Dashed) {
            sBorderBottom1Dashed[i].each(function () {
                this.style.setProperty('border-bottom', '1px dashed ' + c, 'important');
            });
        }

        // border-top 1px solid
        var sBorderTop1 = [
            con
        ];
        for (var i in sBorderTop1) {
            sBorderTop1[i].each(function () {
                this.style.setProperty('border-top', '1px solid ' + c, '');
            });
        }

        // border-top 2px solid
        var sBorderTop2 = [
            sgl,
            bbt
        ];
        for (var i in sBorderTop2) {
            sBorderTop2[i].each(function () {
                this.style.setProperty('border-top', '2px solid ' + c, '');
            });
        }

        // border-top 1px solid important
        var sBorderTop1Important = [
            fcs.find('.main-window'),
            fw.find('.blue-border-top')
        ];
        for (var i in sBorderTop1Important) {
            sBorderTop1Important[i].each(function () {
                this.style.setProperty('border-top', '1px solid ' + c, 'important');
            });
        }

        // background-color
        var sBgcolor = [
            $('.separator')
        ];
        for (var i in sBgcolor) {
            sBgcolor[i].each(function () {
                this.style.setProperty('background-color', c, '');
            });
        }

        if (c) {
            var rgb = hexToRgb(c);
            var al = 'rgba(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ', ';
            var track = al + '.2)';
            var thumb = al + '.5)';
            var thumbHover = al + '1)';
        }
    }
}

/**
 * Update sidebar with favourites and private messages.
 */
function updateSidebar() {
    $.get("//sg.hu/forum/", function (response) {
        $('#sidebar-forum').hide();
        sidebar = $(response).find('#sidebar-forum').html();
        var neosidebar = $('#neosidebar');
        neosidebar.html(sidebar);
        var icon = neosidebar.find('#favorites-open-close-button #icon');
        neosidebar.find('.user-hello').after('<a href="/"><span>Főoldal</span></a>');

        $('.categoryfav-not-new-msg').addClass('category fav-not-new-msg').removeClass('categoryfav-not-new-msg');
        for (var i = 1; i <= 50; i++) {
            $(`.category-${i}fav-not-new-msg`).addClass(`category-${i} fav-not-new-msg`).removeClass(`category-${i}fav-not-new-msg`);
        }

        var sidebarupdate = getSetting('sidebarupdate', 0);
        var newsidebarupdate = sidebarupdate + 1;
        if (newsidebarupdate == 3) {
            newsidebarupdate = 0;
        }
        setSetting('sidebarupdate', newsidebarupdate);

        if (newsidebarupdate == 0) {
            $.get("//sg.hu/uzenetek", function (resp) {
                var privik = $(resp).find('span.new').length;
                // <span>Üzeneteim <span class="hilight opacity-pulse">(1)</span></span>
                var uzeneteim = '<span>Üzeneteim</span>';
                if (privik > 0) {
                    uzeneteim = '<span>Üzeneteim <span class="hilight opacity-pulse">(' + privik + ')</span></span>';
                }
                neosidebar.find('#sidebar-user').find('a[href$="uzenetek"]').html(uzeneteim);
            });
        }

        if (getSetting('favs') == 1) {
            icon.html('+');

            neosidebar.find('#favorites-list .fav-not-new-msg').css({'display': 'none'});
        }

        neosidebar.find('#sidebar-user a').first().addClass('notClickable');
        neosidebar.find('#sidebar-user a').last().addClass('notClickable');

        neosidebar.find('#icon').click(function () {
            var sign = $(this).text();
            if (sign == '+') {
                setSetting('favs', 0);
            }
            if (sign == '-') {
                setSetting('favs', 1);
            }
        });

        $('.forum-topics-block').remove();
        $(response).find('.forum-topics-block').insertAfter('#neosidebar > #sidebar-user-favorites');

        var topics = $('#neosidebar > .forum-topics-block');
        topics.find('header').addClass('headers-medium blue-border-bottom');
        topics.find('section').removeClass('pull-left pull-right');
        //topics.find('ul').removeClass('blue-plus-icon');

        // new fav topics
        var new_topics2 = 0;
        neosidebar.find(".new").each(function () {
            new_topics2++;
        });

        if (new_topics2 != new_topics) {
            new_topics = new_topics2;
            updateTitle();
        }

        updateSgLinks();
        updateBoxes();
        updateTopics();
        updateGallery();
        toggleSections();
        lines_color();
    });
}

/**
 * Update window title with unread chat messages and new favourite topics
 */
function updateTitle() {
    var new_topics_text;
    if (new_topics > 0) {
        new_topics_text = "[" + new_topics + "] ";
    }
    else {
        new_topics_text = "";
    }

    var lik = $('#forum-chat-list > li');
    if (focused) {
        document.title = new_topics_text + sgTitle;
        setTimeout(function () {
            lik.removeClass('unread', 1000);
        }, 2000);
    }
    else {
        if (msg_counter > 0) {
            document.title = "(" + msg_counter + ") " + new_topics_text + sgTitle;
            lik.removeClass('unread');
            var e = lik.slice(-(msg_counter));
            e.addClass('unread');
        }
        else {
            document.title = new_topics_text + sgTitle;
            lik.removeClass('unread');
        }
    }
}

/**
 * Update sidebar and responsive design every 10 seconds.
 */
function updateStuff() {
    updateSidebar();
    if (getSetting('responsive') == 1) {
        if (!isResponsive) {
            setResponsive();
            isResponsive = true;
        }
    }
}

/**
 * Initializes some basic browser shit.
 */
function initData() {
    document.title = sgTitle;

    if (document.cookieHack) {
        document.cookie = document.cookieHack;
    }

    var match = document.cookie.match(new RegExp('identid' + '=([^;]+)'));
    if (match) {
        identid = match[1];
    }

    if (identid === undefined || identid.length == 0) {
        document.getElementById('forum-chat').innerHTML = 'HIBA: cookie hiba';
        return;
    }

    $('.col1').each(function () {
        $(this).text($(this).text().replace('Számítástechnikai fórumok', 'Számtech fórumok'));
        $(this).text($(this).text().replace(' és gazdaság', ''));
    });

    lines_color();

    return identid;
}

function newMessageListener() {
    console.log($("#cset").contents().find("#channels"));
    $("#cset").contents().find("#channels").on('DOMNodeInserted', 'div', function () {
        console.log('sajt');
    });

}

/**
 * Creates the hacked sidebar.
 */
function createNeoSidebar() {
    sidebar = document.createElement("div");
    sidebar.setAttribute('id', 'neosidebar');
    $(sidebar).html("<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>");
    document.body.appendChild(sidebar);

    updateSidebar();
}

console.log('%c Chat command functions loaded. ', log_style_done);