console.log('%c Base loading started...', log_style_start);

// Some operations when the user comes back to the chat window
window.onfocus = function () {
    msg_counter = 0;
    focused = true;
    updateTitle();
};
window.onblur = function () {
    focused = false;
};

//bindNoScroll(document.getElementById('list-frame'), otherScrollSpeed);

function preventDefault(event) {
    event = event || window.event;
    if (event.preventDefault) {
        event.preventDefault();
    }
    event.returnValue = false;
}

/**
 * Main function.
 */
function jChatLoad() {
    console.log('%c Chat loading started... ', log_style_start);

    identid = initData();
    createNeoSidebar();
    setTimeout(updateStuff, 2000);
    setInterval(updateStuff, 10000);
    showTime();
    toggleSnow(true);
    if (getSetting('fbbox') == 0) {
        elem('forum-fb-likebox').style.display = 'none';
    }

    updateSgLinks();

    $('.forums-block').wrapAll('<div id="forums-block-wrapper" />');
    $('#forums-block-wrapper').before('<div id="forum-block-wrapper-button">Témák listája</div>');
    $("#forum-block-wrapper-button").click(function (event) {
        updateTopics(true);
    });

    var frameUrl = 'https://discord.com/channels/466361375565021196/485159821159694359';
    //var frameUrl = 'https://titanembeds.com/embed/466361375565021196';
    $('#forum-wrap').prepend('<iframe id="cset" class="cset" src="' + frameUrl + '" width="100%" height="700"  allowtransparency="true" frameborder="0" style="border:none;"></iframe>');
    setTimeout(newMessageListener, 2000);

    if (getSetting('responsive') == 1) {
        $(window).trigger('resize');
        setResponsive();
    }

    if (getSetting('header') == 0) {
        headerTop.hide();
    }

    if (newSettings > 0) {
        $('<span class="badge">' + newSettings + '</span>').prependTo('#sgcset-header');
    }

    $('h4:contains("Hirdetés")').parent().remove();
   
    console.log('%c Chat loaded.', log_style_done);
}

$('body').scrollTop(0);

console.log('%c Base loaded. ', log_style_done);