var cssNode2 = document.createElement('link');
cssNode2.href = baseUrl + 'jsg_usersearch.css';
cssNode2.type = 'text/css';
cssNode2.rel = 'stylesheet';
cssNode2.media = 'screen';
document.getElementsByTagName('head')[0].appendChild(cssNode2);

if (!$("#jsg-usersearch").length) {
    $('nav#menu-main').prepend('<input type="text" placeholder="Felhasználónév" name="jsg-usersearch" id="jsg-usersearch"><input type="submit" id="jsg-usersearch-submit" value="Keres">');
}

$('#jsg-usersearch').keypress(function (e) {
    var key = e.which;
    if (key == 13) {
        $('#jsg-usersearch-submit').click();
        return false;
    }
});

$('#jsg-usersearch-submit').click(function () {
    var val = $('#jsg-usersearch').val();
    if (val != "") {
        val = val.replace(" ", "+");
        window.open('http://sg.hu/kereses?s=' + val + '&u=1&sh=', '_blank');
    }
});