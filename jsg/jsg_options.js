// TODO maxinak:
// link-blocks link-block-smedium one-line link-right-icon blue-plus-icon
// $('.forum-topics-block').insertAfter('#sidebar-user');
// $('.forum-topics-block').insertAfter('#forum-chat');
// header: headers-medium blue-border-bottom

// TODO átszervezni az options elemek kreálását

// TODO options elemekre hover help

// TODO cset cuccok szétszedése, má túl nagy

var cssNode = document.createElement('link');
cssNode.href = baseUrl + 'jsg_options.css';
cssNode.type = 'text/css';
cssNode.rel = 'stylesheet';
cssNode.media = 'screen';
document.getElementsByTagName('head')[0].appendChild(cssNode);

var jsgBase = 'jsg-base';
var jsgTomsolo = 'jsg-tomsolo';
var jsgCsetRendez = 'jsg-csetrendez';
var jsgUserKereso = 'jsg-usersearch';
initSetting(jsgBase);
initSetting(jsgTomsolo);
initSetting(jsgCsetRendez);
initSetting(jsgUserKereso);
var jsgBaseVal = checkedVal(jsgBase);
var jsgTomsoloVal = checkedVal(jsgTomsolo);
var jsgCsetRendezVal = checkedVal(jsgCsetRendez);
var jsgUserKeresoVal = checkedVal(jsgUserKereso);

var mod = false;
var modCheckBox = modCsetIpCheckBox = modUtolsoSzamSelect = '';
$('section#sidebar-user > nav.link-blocks > a').each(function () {
    if ($(this).attr('href') == '/moderator/index') {
        mod = true;
    }
});
if (mod) {
    var jsgMod = 'jsg-mod';
    initSetting(jsgMod);
    var jsgModVal = checkedVal(jsgMod);

    var jsgModCsetIp = 'jsg-mod-csetip';
    initSetting(jsgModCsetIp, 0);
    var jsgModCsetIpVal = checkedVal(jsgModCsetIp);

    var jsgModTime = 'jsg-mod-time';
    var now = new Date().getTime();
    initSetting(jsgModTime, now);

    var jsgModUtolsoSzam = 'jsg-mod-utolsoszam';
    initSetting(jsgModUtolsoSzam, "5");
    var jsgModUtolsoSzamVal = GetSetting(jsgModUtolsoSzam);

    modCheckBox = '<label class="jsg-options-checkbox-container"><input type="checkbox" id="jsg-options-checkbox-mod" class="jsg-options-checkbox" name="' + jsgMod + '" ' + jsgModVal + ' /><span class="jsg-options-checkbox-text">Moderátori funkciók</span></label>';
    modCsetIpCheckBox = '<label class="jsg-options-checkbox-container"><input type="checkbox" id="jsg-options-checkbox-mod-csetip" class="jsg-options-checkbox" name="' + jsgModCsetIp + '" ' + jsgModCsetIpVal + ' /><span class="jsg-options-checkbox-text">Cset IP címek</span></label>';
    modUtolsoSzamSelect = '<label class="jsg-options-checkbox-container"><select id="jsg-options-checkbox-mod-utolsoszam" class="jsg-options-select" name="' + jsgModUtolsoSzam + '"><option value="0">0</option><option value="5">5</option><option value="10">10</option><option value="20">20</option><option value="50">50</option></select><span class="jsg-options-checkbox-text">Utolsó regisztrálók</span></label>';
}

var baseCheckBox = '<label class="jsg-options-checkbox-container"><input type="checkbox" id="jsg-options-checkbox-base" class="jsg-options-checkbox" name="' + jsgBase + '" ' + jsgBaseVal + ' /><span class="jsg-options-checkbox-text">Általános téma beállítások</span></label>';

var tomsoloCheckBox = '<label class="jsg-options-checkbox-container"><input type="checkbox" id="jsg-options-checkbox-tomsolo" class="jsg-options-checkbox" name="' + jsgTomsolo + '" ' + jsgTomsoloVal + ' /><span class="jsg-options-checkbox-text">Tomsolo-féle redesign</span></label>';

var csetrendezCheckBox = '<label class="jsg-options-checkbox-container"><input type="checkbox" id="jsg-options-checkbox-tomsolo" class="jsg-options-checkbox" name="' + jsgCsetRendez + '" ' + jsgCsetRendezVal + ' /><span class="jsg-options-checkbox-text">Cset rendezés ABC</span></label>';

var userKeresoCheckBox = '<label class="jsg-options-checkbox-container"><input type="checkbox" id="jsg-options-checkbox-tomsolo" class="jsg-options-checkbox" name="' + jsgUserKereso + '" ' + jsgUserKeresoVal + ' /><span class="jsg-options-checkbox-text">Felhasználó keresés</span></label>';

var optionsButtonHTML = '<div id="jsg-options-menu"><a href="' + document.URL + '" id="jsg-options-button"><img src="http://kocsog.eu/jsg/jsg_options.png" id="jsg-options-icon" /></a></div>';
$('body').append(optionsButtonHTML);

var optionsModalHTML = '<div id="jsg-options-modal"><p id="jsg-options-modal-title">jSG beállítások</p><p id="jsg-options-modal-content">' + modCheckBox + modCsetIpCheckBox + modUtolsoSzamSelect + tomsoloCheckBox + csetrendezCheckBox + userKeresoCheckBox + '</p><button id="jsg-options-modal-button">Mehet</button></div>';
$('body').append(optionsModalHTML);

if (mod) {
    $('#jsg-options-checkbox-mod-utolsoszam').val(jsgModUtolsoSzamVal);
}

var optionsCheckbox = $('.jsg-options-checkbox');
var optionsSelect = $('.jsg-options-select');
var optionsModal = $('#jsg-options-modal');
var optionsButton = $('#jsg-options-menu');
var optionsButtonLink = $('#jsg-options-button');
var optionsMehet = $('#jsg-options-modal-button');
optionsButton.click(function (event) {
    event.stopPropagation();
    optionsModal.toggle();
});

$('html').click(function () {
    optionsModal.hide();
});

optionsButtonLink.click(function (event) {
    event.preventDefault();
});


optionsModal.click(function (event) {
    event.stopPropagation();
});

optionsMehet.click(function (event) {
    location.reload();
});

optionsCheckbox.click(function (event) {
    var val = 0;
    var chkd = $(this).prop('checked');
    if (chkd) {
        val = 1;
    }
    var name = $(this).attr('name');

    SetSetting(name, val);
});

optionsSelect.change(function (event) {
    var val = $(this).val();
    var name = $(this).attr('name');

    SetSetting(name, val);
});