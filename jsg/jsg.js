// ==UserScript==
// @name        sg
// @namespace   sg
// @description sg topic cuccok
// @include     http://sg.hu/*
// @version     1
// @grant       none
// ==/UserScript==
var baseUrl = "http://kocsog.eu/jsg/";

var loadBaseCss = GetSetting('jsg-base') == 1 ? true : false;
var loadModStuff = GetSetting('jsg-mod') == 1 ? true : false;
var loadTomsolo = GetSetting('jsg-tomsolo') == 1 ? true : false;
var loadUserSearch = GetSetting('jsg-usersearch') == 1 ? true : false;
var loadOptions = false;

var urls = [
    "http://sg.hu/forum/",
    "http://sg.hu/forum/#",
    "https://sg.hu/forum/",
    "https://sg.hu/forum/#"
];
var joUrl = inArray(document.URL, urls);

if (joUrl) {
    loadOptions = true;
}

if (loadOptions) {
    var jsNode2 = document.createElement("script");
    jsNode2.type = "text/javascript";
    jsNode2.src = baseUrl + 'jsg_options.js';
    jsNode2.async = false;
    document.body.appendChild(jsNode2);
}

if (loadBaseCss) {
    var cssNode = document.createElement('link');
    cssNode.href = baseUrl + 'jsg_base.css';
    cssNode.type = 'text/css';
    cssNode.rel = 'stylesheet';
    cssNode.media = 'screen';
    document.getElementsByTagName('head')[0].appendChild(cssNode);
}

if (loadTomsolo) {
    var cssNode2 = document.createElement('link');
    cssNode2.href = baseUrl + 'jsg_tomsolo.css';
    cssNode2.type = 'text/css';
    cssNode2.rel = 'stylesheet';
    cssNode2.media = 'screen';
    document.getElementsByTagName('head')[0].appendChild(cssNode2);
}

if (loadModStuff) {
    var jsNode3 = document.createElement("script");
    jsNode3.type = "text/javascript";
    jsNode3.src = baseUrl + 'jsg_mod.js';
    document.body.appendChild(jsNode3);
}

if (loadUserSearch) {
    var jsNode4 = document.createElement("script");
    jsNode4.type = "text/javascript";
    jsNode4.src = baseUrl + 'jsg_usersearch.js';
    document.body.appendChild(jsNode4);
}

function SetSetting(name, value) {
    var testObject = { 'value': value };
    localStorage.setItem(name, JSON.stringify(testObject));
}

/**
 * @return {boolean}
 */
function GetSetting(name) {
    var retrievedObject = localStorage.getItem(name);
    if (!retrievedObject) {
        return false;
    }

    var jsonObject = JSON.parse(retrievedObject);
    return jsonObject.value;
}

function initSetting(name, val) {
    val = val || 0;
    if (!GetSetting(name)) {
        SetSetting(name, val);
    }
}

function checkedVal(name) {
    var ret = '';
    if (GetSetting(name) == 1) {
        ret = 'checked="checked"';
    }
    return ret;
}

function inArray(needle, haystack) {
    var length = haystack.length;

    for (var i = 0; i < length; i++) {
        if (haystack[i] == needle) {
            return true;
        }
    }

    return false;
}