var cssNode = document.createElement('link');
cssNode.href = baseUrl + 'jsg_mod.css';
cssNode.type = 'text/css';
cssNode.rel = 'stylesheet';
cssNode.media = 'screen';
document.getElementsByTagName('head')[0].appendChild(cssNode);

var jsNode666 = document.createElement("script");
jsNode666.type = "text/javascript";
jsNode666.src = baseUrl + 'jsg_ips.js';
document.body.appendChild(jsNode666);

var mods = [
    "Efreet",
    "Mortimer",
    "Lteebee",
    "Pheel",
    "Kandurex",
    "Hmuda",
    "Sir Quno Jedi",
    "[NST]Cifu",
    "Cat",
    "Maximus4",
    "MerlinW",
    "pedrohsi",
    "Monoton",
    "j0nNyKa",
    "Jim Morrison",
    "domby",
    "SHADOWEYES7",
    "pixxel",
    "SZABl",
    "Dzsini",
    "SuhiP"
];

var isMod = false;
$.ajax({
    url: '/moderator/index/',
    type: 'GET',
    async: false
}).done(function (res) {
    res = res.replace(/\n/g, "");
    res = res.replace(/  /g, "");
    var result = res.match(/<input type=\"password\"/g);
    if (result == null) {
        isMod = true;
    }
});

var mod = false;
$.ajax({
    url: '/forum/',
    type: 'GET',
    async: false
}).done(function (res) {
    res = res.replace(/\n/g, "");
    res = res.replace(/  /g, "");
    $(res).find('section#sidebar-user > nav.link-blocks > a').each(function () {
        if ($(this).attr('href') == '/moderator/index') {
            mod = true;
        }
    });
});

var jsgCsetRendez = 'jsg-csetrendez';
var jsgCsetRendezVal = GetSetting(jsgCsetRendez);
var jsgCsetIp = 'jsg-mod-csetip';
var jsgCsetIpVal = GetSetting(jsgCsetIp);

var utolsok = window.location.pathname.match(/moderator\/utolsok/g);
if (utolsok && mod) {
    $('#lastusers').find("tr:gt(0)").each(function () {
        if ($(this).hasClass('red')) {
            $(this).removeClass('red');
            $(this).addClass('jsg-bold');
        }

        var ip = $(this).find("td:eq(2)").text();
        if (inArray(ip, trollIps)) {
            $(this).addClass('jsg-red');
        }
        else if (inArray(getIpPart(ip), trollIpRanges)) {
            $(this).addClass('jsg-orange');
        }
    });
}

var adatlap = window.location.pathname.match(/felhasznalo\/[0-9]+/g);
if (adatlap && mod) {
    var ipDiv = $('table.data-table').find('tr:contains("Utoljára használt IP:") > td:last-child');
    var ip = ipDiv.text();

    if (inArray(ip, trollIps)) {
        ipDiv.addClass('jsg-red');
    }
    else if (inArray(getIpPart(ip), trollIpRanges)) {
        ipDiv.addClass('jsg-orange');
    }
}

var topicId = window.location.pathname.match(/tema\/[0-9]+/g);
if (topicId && mod) {
    topicId = topicId[0].replace('tema/', '');

    // Topic vakera gombok
    /*
     if (!$('.moderation-breadcrumbs')[0]) {
     var topicVakera = '<div class="moderation-breadcrumbs">';
     topicVakera += '<a href="/moderator/topic/szerkesztes/' + topicId + '"><button class="btn btn-info">Téma karbantartása</button></a>';
     topicVakera += '<a href="/moderator/topic/torles/' + topicId + '"><button class="btn btn-danger">Téma törlése</button></a>';
     topicVakera += '</div>';

     $('#topic-panel').before(topicVakera);
     }
     */

    // Szerkesztés - törlés gombok
    $('header.header').each(function () {
        var info = 'data-info=' + $(this).parent().attr('data-post-info') + '';
        infoObj = info.replace('data-info=', '');

        if (infoObj != 'undefined') {
            obj = JSON.parse(infoObj);
            msgId = obj.msg_id;
            msgUnique = obj.msg_unique;

            var nick = '';
            var aName = $(this).find('a.name');
            var userId = aName.attr('href');
            userId = userId.replace('/felhasznalo/', '');
            var aHtml = aName.html();
            var vanE = aHtml.indexOf('<img');
            if (vanE == -1) {
                nick = aName.text();
            } else {
                nick = $(this).find('a.name > img').attr('title');
            }
            nick = nick.replace(' - VIP', '');
            nick = nick.replace(' ', '_');
            var newInfo = info.replace('}', ',"nick":"' + nick + '","user_id":' + userId + '}');

            var del = ban = szerk = osszPont = '';
            if ($(this).find('a.delete-msg').length == 0) {
                del = '<a style="margin-right: 5px;" class="jsg-hekked-delete jsg-delete-msg pull-right" ' + newInfo + ' href="' + document.URL + '">[Törlés]</a> ';
            }
            if ($(this).find('a:contains("Szerk")').length == 0) {
                szerk = '<a style="margin-right: 5px;" class="pull-right" href="/moderator/uzenet/szerkesztes/' + topicId + '/' + msgId + '">[Szerk]</a> ';
            }

            if (!isMod) {
                ban = '<a style="margin-right: 5px;" class="jsg-hekked-mod pull-right" href="' + document.URL + '">[Mod login]</a> ';
            } else {
                if (userId != '1101142569') {
                    ban = '<a style="margin-right: 5px;" class="jsg-hekked-ban pull-right" ' + newInfo + ' href="' + document.URL + '">[5pont]</a> ';
                    ban += '<a style="margin-right: 5px;" class="jsg-vegleges-ban jsg-hekked-ban pull-right" ' + newInfo + ' href="' + document.URL + '">[Kitiltás]</a> ';
                }
            }

            osszPont = '<span class="jsg-osszPontSpan jsg-osszPontSpan-' + userId + ' pull-right" id="jsg-osszPontSpan-' + msgId + '"></span> <a style="margin-right: 5px;" class="jsg-hekked-osszPont jsg-hekked-osszPont-' + userId + ' pull-right" ' + newInfo + ' href="' + document.URL + '">[Összes büntipont]</a>';

            $(this).append(del + szerk + ban + osszPont);
        }
    });

    addBanEvent(true);
    addModEvent();
    addOsszPontEvent();
}
else if (joUrl) {
    var bannoltUserek = [];
    $.ajax({
        url: '/moderator/buntetopontok/',
        type: 'GET',
        async: false
    }).done(function (res) {
            res = res.replace(/\n/g, "");
            res = res.replace(/  /g, "");
            var result = res.match(/<a href=\"\/felhasznalo\/[0-9]+\">[a-zA-Z0-9 ]+<\/a><\/b><\/td><td>(<div class=\"icon alert\"><\/div>)*/g);
            result.forEach(function (entry) {
                var aResult = entry.match(/<a href=\"\/felhasznalo\/[0-9]+\">[a-zA-Z0-9 ]+<\/a>/g);
                var pontokArr = entry.match(/<td>(<div class=\"icon alert\"><\/div>)*/g);
                var pontok = (pontokArr[0].split("icon").length - 1);
                var name = $(entry).text();
                if (!inArray(name, bannoltUserek) && pontok >= 5) {
                    bannoltUserek.push(name);
                }
            });
        });

    // Cset ban gomb, utolso regisztraltak
    var csetBan = function () {
        if (jsgCsetIpVal == 1) {
            $('#forum-chat > .pull-left').css('width', '100%');
            $('#forum-chat > .pull-right').attr('id', 'jsg-lista');
            $('body').prepend($('#jsg-lista'));
        }

        var csetlist = $('#forum-chat-user-list');
        /*
         if (!csetlist.hasClass('jsg-tabled')) {
         csetlist.addClass('jsg-tabled');
         csetlist.wrap('<table></table>');
         }
         */

        $('#forum-chat-user-list > li').each(function () {
            var dat = $(this);
            var firstA = $(this).find('a');
            var uName = firstA.text();
            var uidStr = $(this).attr('id');
            var uid = uidStr.replace('userlist-user-', '');

            if (inArray(uName, mods)) {
                if (!firstA.hasClass('jsg-bold')) {
                    firstA.addClass('jsg-bold');
                    firstA.text(' @ ' + firstA.text());
                }
            }

            if (!$(this).hasClass('jsg-tded')) {
                $(this).addClass('jsg-tded');
                firstA.addClass('jsg-csetname-old');
                //$(this).wrap('<tr></tr>');
                //firstA.wrap('<td class="jsg-csetname jsg-csetname-' + uid + '"></td>');
            }

            if (inArray(uName, bannoltUserek)) {
                firstA.addClass('jsg-athuzott');
                //$(this).append('<td class="jsg-csetfunc"></td>');
            }
            else {
                uName = uName.replace(' ', '');
                var banStr = '<td class="jsg-csetfunc"><a href="' + document.URL + '" class="jsg-hekked-ban jsg-cset-ban" data-info={"nick":"' + uName + '","user_id":' + uid + '}>[5p]</a>';
                banStr += '<a href="' + document.URL + '" class="jsg-vegleges-ban jsg-hekked-ban jsg-cset-ban" data-info={"nick":"' + uName + '","user_id":' + uid + '}>[k]</a></td>';
                if (!isMod) {
                    banStr = '<td class="jsg-csetfunc"><a class="jsg-hekked-mod jsg-cset-mod" href="' + document.URL + '">[m]</a></td>';
                }
                if ($(this).find('a.jsg-hekked-ban').length == 0 && $(this).find('a.jsg-hekked-mod').length == 0 && mod) {
                    $(this).append(banStr);
                }
            }

            if (jsgCsetIpVal == 1 && !dat.hasClass('jsg-iped') && isMod) {
                $.ajax({
                    url: '/felhasznalo/' + uid,
                    type: 'GET'
                }).done(function (res) {
                        res = res.replace(/\n/g, "");
                        res = res.replace(/  /g, "");
                        var result = $(res).find("table.data-table");

                        var csetIp = result.find('tr:contains("Utoljára használt IP:") > td:last-child').text();

                        var trollE = '';
                        if (inArray(csetIp, trollIps)) {
                            trollE = ' jsg-red';
                        }
                        else if (inArray(getIpPart(csetIp), trollIpRanges)) {
                            trollE = ' jsg-orange';
                        }

                        var ipStr = '<span class="jsg-ip jsg-ip-' + uid + trollE + '">' + csetIp + '</span>';
                        if (csetIp != null && csetIp != "") {
                            dat.append(ipStr);
                            dat.addClass('jsg-iped');
                        }
                    });
            }

            if (jsgCsetIpVal == 1 && isMod) {
                var jsgips = [];
                $('.jsg-ip').each(function () {
                    var jsgIp = $(this).text();
                    if ($(this).hasClass('jsg-bold')) {
                        $(this).removeClass('jsg-bold');
                    }
                    if (inArray(jsgIp, jsgips)) {
                        $('.jsg-ip:contains("' + jsgIp + '")').addClass('jsg-bold');
                    }
                    if (inArray(jsgIp, trollIps)) {
                        $('.jsg-ip:contains("' + jsgIp + '")').addClass('jsg-red');
                    }
                    else if (inArray(getIpPart(jsIp), trollIpRanges)) {
                        $('.jsg-ip:contains("' + jsgIp + '")').addClass('jsg-orange');
                    }
                    jsgips.push(jsgIp);
                });
            }
        });

        if (jsgCsetRendezVal == 1) {
            var mylist = $('#forum-chat-user-list');
            var listItems = mylist.children('li').get();
            listItems.sort(function (a, b) {
                return $(a).text().toUpperCase().localeCompare($(b).text().toUpperCase());
            });
            $.each(listItems, function (index, item) {
                mylist.append(item);
            });
        }

        var utolsoReg = GetSetting('jsg-mod-utolsoszam');
        if ($("#jsg-utolso-regisztraltak").length == 0 && isMod && utolsoReg != "0" && mod) {
            var jsgModTime = 'jsg-mod-time';
            var savedTime = GetSetting(jsgModTime);
            $.ajax({
                url: '/moderator/utolsok/',
                type: 'GET',
                async: false
            }).done(function (res) {
                    res = res.replace(/\n/g, "");
                    res = res.replace(/  /g, "");
                    var result = $(res).find("table#lastusers");

                    var ips = [];
                    result.find("tr").each(function () {
                        var ip = $(this).find("td:eq(2)").text();
                        ips.push(ip);
                    });

                    var ipCounts = {};
                    for (var i = 0; i < ips.length; i++) {
                        var num = ips[i];
                        ipCounts[num] = ipCounts[num] ? ipCounts[num] + 1 : 1;
                    }

                    result.find("tr:gt(" + utolsoReg + ")").remove();
                    result.find("tr:eq(0)").addClass('jsg-lastusers-thead');
                    result.find("tr:eq(0)").append('<th>Kitiltás</th>');
                    result.find("tr:gt(0)").each(function () {
                        var d = $(this).find("td:eq(0)").text();
                        var dateString = d.replace(/-/g, '/');
                        var parsedDate = Date.parse(dateString);
                        if (!$(this).hasClass('light-border-bottom')) {
                            $(this).addClass('light-border-bottom');
                        }

                        if (parsedDate > savedTime) {
                            if (savedTime != false) {
                                result.find("tr:gt(0)").each(function () {
                                    $(this).removeAttr('id', 'jsg-last-seen');
                                });
                                $(this).attr('id', 'jsg-last-seen');
                            }
                            var now = new Date().getTime();
                            SetSetting(jsgModTime, now);
                        }

                        var ipTd = $(this).find("td:eq(2)");
                        var ip = ipTd.text();
                        if (ipCounts[ip] > 1 && inArray(ip, trollIps)) {
                            ipTd.addClass('jsg-red jsg-bold');
                        }
                        else if (ipCounts[ip] > 1 && !inArray(ip, trollIps)) {
                            ipTd.addClass('jsg-bold');
                        }
                        else if (ipCounts[ip] <= 1 && inArray(ip, trollIps)) {
                            ipTd.addClass('jsg-red');
                        }
                        else if (ipCounts[ip] > 1 && inArray(getIpPart(ip), trollIpRanges)) {
                            ipTd.addClass('jsg-orange');
                        }

                        var link = $(this).find("a");
                        var href = link.attr('href');
                        var uName = link.text();
                        if (inArray(uName, bannoltUserek)) {
                            link.addClass('jsg-athuzott');
                            $(this).addClass('jsg-bannolt');
                        } else {
                            var mehet = true;
                            $.ajax({
                                url: href,
                                type: 'GET',
                                async: false
                            }).done(function (res) {
                                    res = res.replace(/\n/g, "");
                                    res = res.replace(/  /g, "");
                                    if (res.search("A felhasználó ki van tiltva") >= 0) {
                                        mehet = false;
                                    }
                                });

                            if (mehet) {
                                uName = uName.replace(' ', '');
                                var uid = link.attr('href').replace('/felhasznalo/', '');
                                var banStr = '<a href="' + document.URL + '" class="jsg-hekked-ban jsg-cset-ban" data-info={"nick":"' + uName + '","user_id":' + uid + '}>[5p]</a>';
                                banStr += '<a href="' + document.URL + '" class="jsg-vegleges-ban jsg-hekked-ban jsg-cset-ban" data-info={"nick":"' + uName + '","user_id":' + uid + '}>[k]</a>';
                                if (!isMod) {
                                    banStr = '<a class="jsg-hekked-mod jsg-cset-mod" href="' + document.URL + '">[m]</a>';
                                }
                                if ($(this).find('a.jsg-hekked-ban').length == 0 && $(this).find('a.jsg-hekked-mod').length == 0) {
                                    $(this).append('<td>' + banStr + '</td>');
                                }
                            } else {
                                link.addClass('jsg-athuzott');
                                $(this).addClass('jsg-bannolt');
                                if ($(this).find('a.jsg-hekked-ban').length == 0 && $(this).find('a.jsg-hekked-mod').length == 0) {
                                    $(this).append('<td></td>');
                                }
                            }
                        }
                    });

                    var resultHtml = '<table id="jsg-lastusers">' + result.html() + '</table>';
                    var newDiv = '<section id="jsg-utolso-regisztraltak" class="forums-block"><header class="blue-border-bottom"><span class="col1">Utolsó ' + utolsoReg + ' regisztráló</span></header><ul class="forums-list list-unstyled">' + resultHtml + '</ul></section>';
                    $(newDiv).insertAfter('#forum-chat');
                });
        }

        addBanEvent(true);
        addModEvent();
    };

    setTimeout(csetBan, 2000);
    setInterval(csetBan, 10000);
}

$('.jsg-hekked-delete').on('click', function (e) {
    e.preventDefault();
    var info = $(this).data('info');

    if (confirm('Biztos törlöd ' + info.nick + ' #' + info.msg_unique + ' hozzászólását?')) {
        $.ajax({
            url: '/api/forum/delete/msg',
            type: 'POST',
            data: {
                'topic_id': topicId,
                'msg_unique': info.msg_unique
            }
        }).done(function (res) {
                if (res.status == 'error') {
                    alert(res.msg);
                    return;
                }

                $('#post-' + topicId + '-' + info.msg_id).animate({
                    opacity: 0
                }, 175, function () {
                    $(this).hide(175, function () {
                        $(this).remove();
                    });
                });
            });

        return false;
    }
    else {
        return false;
    }
});

function addModEvent() {
    $('.jsg-hekked-mod').on('click', function (e) {
        e.preventDefault();
        var newWin = window.open('/moderator/index', 'PopUp', 'scrollbars=1, menubar=0, resizable=0, width=850, height=500');
        var loggedIn = false;
        var pwPresent = false;

        var login = function () {
            var pws = newWin.document.getElementsByName('pass');
            var pwField = pws[pws.length - 1];
            if (pwField) {
                if (pwField.value != "") {
                    pwPresent = true;
                }
            } else {
                loggedIn = true;
            }
            if (!loggedIn && pwPresent) {
                var buts = newWin.document.getElementsByClassName('btn');
                var but = buts[buts.length - 1];
                but.click();
            }
        };
        var goBack = function () {
            if (pwPresent) {
                newWin.close();
                //location.reload();
            }
        };

        setTimeout(login, 1000);
        setTimeout(goBack, 2000);
    });
}

function addBanEvent(reload) {
    $('.jsg-hekked-ban').on('click', function (e) {
        e.preventDefault();
        var info = $(this).data('info');
        var cause = '<a style="color: #660000; font-weight: bold; padding: 5px;" href="http://kocsog.eu/ban/" target="_blank">http://kocsog.eu/ban/</a>';

        if ($(this).hasClass('jsg-vegleges-ban')) {
            if (confirm('Biztos bannolod véglegesen: ' + info.nick + '-t?')) {
                $.ajax({
                    url: '/moderator/kitiltas/' + info.user_id,
                    type: 'GET'
                }).done(function (res) {
                        if (res.status == 'error') {
                            alert(res.msg);
                            if (reload) {
                                location.reload();
                            }
                            return;
                        } else {
                            if (reload) {
                                location.reload();
                            }
                        }
                    });

                return false;
            }
            else {
                return false;
            }
        }
        else {
            if (confirm('Biztos adsz 5 pontot neki: ' + info.nick + ' ?')) {
                $.ajax({
                    url: '/moderator/pontosztas/' + info.user_id,
                    type: 'POST',
                    data: {
                        'points': 5,
                        'cause': cause
                    }
                }).done(function (res) {
                        if (res.status == 'error') {
                            alert(res.msg);
                            if (reload) {
                                location.reload();
                            }
                            return;
                        } else {
                            if (reload) {
                                location.reload();
                            }
                        }
                    });

                return false;
            }
            else {
                return false;
            }
        }
    });
}

function addOsszPontEvent() {
    $('.jsg-hekked-osszPont').on('click', function (e) {
        e.preventDefault();
        var info = $(this).data('info');
        var uId = info.user_id;

        $.ajax({
            url: '/felhasznalo/' + uId,
            type: 'GET',
            async: false
        }).done(function (res) {
                res = res.replace(/\n/g, "");
                res = res.replace(/  /g, "");
                var result = res.match(/<div class=\"icon alert\"><\/div>/g);
                var osszBuntiPont = 0;
                if (result != null) {
                    osszBuntiPont = result.length;
                }

                var osszPontBut = $('a.jsg-hekked-osszPont-' + uId);
                var osszPontSpan = $('span.jsg-osszPontSpan-' + uId);
                osszPontSpan.html('Összes büntipont: ' + osszBuntiPont + '&nbsp;');
                osszPontBut.hide();
            });
    });
}

$.expr[':']['hasText'] = function (node, index, props) {
    return node.innerText.contains(props[3]);
}
