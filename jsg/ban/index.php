<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta name="keywords" lang="hu" content=""/>
    <meta name="keywords" lang="en" content=""/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="author" content="j0nNyKa"/>
    <meta name="robots" content="nofollow"/>
    <meta name="version" content="1.0"/>
    <meta name="copyright" content="Copyright &#169; 2014 j0nNyKa"/>
    <meta name="description" content=""/>
    <title>SG ip ban list</title>
    <script type="text/javascript" src="tablesort.js"></script>
    <link rel="stylesheet" href="style.css" type="text/css"/>
</head>

<body>
<div id="container">

    <?php
        $file = '../jsg_banlist.js';

        if ($_POST) {
            $val = $_POST['banlist_textarea'];
            $val = str_replace('\\', '', $val);
            file_put_contents($file, $val);
        }

        $current = file_get_contents($file);

        echo '<form action="index.php" method="POST">';
        echo '<table id="test1" cellpadding="0" cellspacing="0" border="0" class="sortable-onload-10r rowstyle-alt colstyle-alt no-arrow">';

        echo '<thead>';
        echo '<tr>';
        echo '<th><input type="submit" value="Save" id="banlist_save" /></th>';
        echo '</tr>';
        echo '</thead>';

        echo '<tbody>';
        echo '<tr>';
        echo '<td>';
        echo '<textarea name="banlist_textarea" id="banlist_textarea" rows="4" cols="50">' . $current . '</textarea>';
        echo '</td>';
        echo '</tr>';
        echo '</tbody>';
        echo '</table>';
        echo '</form>';
    ?>

</div>
</body>
</html>