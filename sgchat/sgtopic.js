if (document.getElementById('forum-chat')) {
    if(window.location.protocol != 'http:') {
        location.href = location.href.replace("https://", "http://");
    }

    var base = 'http://jonnyka.bitbucket.org/sgchat/';
    // j0nNyKa
    if (document.URL.indexOf('1333095063') > -1) {
        base = 'http://kocsog.eu/sgchat/';
        //base = 'http://jonny.lilab.net/sgchat/';
    }
    // Neocortex
    else if (document.URL.indexOf('1411563165') > -1) {
        base = 'http://syspirit.com/syspirit/sgchat/';
    }
    // pixxel
    else if (document.URL.indexOf('1404811522') > -1) {
        base = 'http://beta.linedesign.hu/nyusg/sgchat/';
    }

    var socketOnline = true;

    require(['//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/basic/jquery.qtip.js']);
    require(['//cdnjs.cloudflare.com/ajax/libs/spectrum/1.3.0/js/spectrum.min.js']);
    require([base + 'lib/jquery.urlive.js']);

    require(['http://www.sg.hu:18080/socket.io/socket.io.js'], function (io) {
        window.io = io;
    });
    if (socketOnline) {
        require(['http://lilab.net:8000/socket.io/socket.io.js'], function (iol) {
            window.iol = iol;
        });
    }

    require([base + 'sgchat_config.js?rand=' + Math.random()]);
    require([base + 'sgchat_functions.js?rand=' + Math.random()]);
    require([base + 'sgchat_base.js?rand=' + Math.random()], function (io) {
        require(['nodeChat'], function () {
            chatLoad();
        })
    }, function () {
        var chat = document.getElementById('forum-chat-hacked');
        var div = document.createElement('div');
        div.id = 'forum-chat-error';
        div.innerHTML = 'HIBA<br/>Nem sikerült csatlakozni a szerverhez';

        console.log('chat initiated');
        chat.appendChild(div);
    });
}
