console.log('%c Mod stuff loading started... ', log_style_start);

function modLoggedCheck() {
    var ret = false;
    $.ajax({
        url: '/moderator/index/',
        type: 'GET',
        async: false
    }).done(function (res) {
        res = res.replace(/\n/g, "");
        res = res.replace(/  /g, "");
        var result = res.match(/<input type=\"password\"/g);
        ret = result == null;
    });

    return ret;
}

function checkModDiv() {
    if (!$('#modStatus').length) {
        $('#breadcrumb').append('<div id="modStatus"><span id="modStat">mod</span></div>');
    }
    $.ajax({
        url: '/moderator/index/',
        type: 'GET',
        async: false
    }).done(function (res) {
        res = res.replace(/\n/g, "");
        res = res.replace(/  /g, "");
        var result = res.match(/<input type=\"password\"/g);
        var modStat = $('#modStat');
        if (result == null) {
            modStat.addClass('isMod');
            modStat.removeClass('isNotMod');
            updateLastRegTable();
        }
        else {
            modStat.addClass('isNotMod');
            modStat.removeClass('isMod');
            addModEvent($('.isNotMod'));
        }
    });
}

function addModEvent(element) {
    element.on('click', function (e) {
        e.preventDefault();
        var newWin = window.open('/moderator/index', 'PopUp', 'scrollbars=1, menubar=0, resizable=0, width=850, height=500');
        var loggedIn = false;
        var pwPresent = false;

        var login = function() {
            var pws = newWin.document.getElementsByName('pass');
            var pwField = pws[pws.length - 1];
            if (pwField) {
                if (pwField.value != "") {
                    pwPresent = true;
                }
            }
            else {
                loggedIn = true;
            }
            if (!loggedIn && pwPresent) {
                var buts = newWin.document.getElementsByClassName('btn');
                var but = buts[buts.length - 1];
                //console.log(buts);console.log(but);
                but.click();
            }
        };

        var goBack = function() {
            if (pwPresent) {
                newWin.close();
                //location.reload();
            }
        };

        var update = function() {
            var modStat = $('#modStat');
            modStat.addClass('isMod');
            modStat.removeClass('isNotMod');
            updateLastRegTable();
        };

        setTimeout(login, 1000);
        setTimeout(goBack, 2000);
        setTimeout(update, 5000);
    });
}

function updateLastRegTable() {
    var utolsoReg = getSetting('utolsoreg');
    var realUtolsoReg = $('#jsg-utolso-regisztraltak tr').length - 1;

    if (utolsoReg == 0 || true) {
        $("#jsg-utolso-regisztraltak").remove();
    }
    else {
        $.ajax({
            url: '/moderator/utolsok/',
            type: 'GET',
            async: false
        }).done(function (res) {
            res = res.replace(/\n/g, "");
            res = res.replace(/  /g, "");
            var result = $(res).find("table#lastusers");

            var latestUser = result.find("tr:eq(1)").find("td:eq(2)").text();
            if (getSetting('utolsoreg-latestuser') != latestUser || realUtolsoReg != utolsoReg || trollIpRanges.length == 0) {
                setSetting('utolsoreg-latestuser', latestUser);

                var utolsoRegTime = getSetting('utolsoreg-time');

                var ips = [];
                result.find("tr").each(function () {
                    var ip = $(this).find("td:eq(2)").text();
                    ips.push(ip);
                });

                var ipCounts = {};
                for (var i = 0; i < ips.length; i++) {
                    var num = ips[i];
                    ipCounts[num] = ipCounts[num] ? ipCounts[num] + 1 : 1;
                }

                result.find("tr:gt(" + utolsoReg + ")").remove();
                result.find("tr:eq(0)").addClass('jsg-lastusers-thead');
                result.find("tr:eq(0)").append('<th>Kitiltás</th>');
                result.find("tr:eq(1)").addClass('jsg-lastusers-latestuser');
                result.find("tr:gt(0)").each(function () {
                    var d = $(this).find("td:eq(0)").text();
                    var dateString = d.replace(/-/g, '/');
                    var parsedDate = Date.parse(dateString);
                    if (!$(this).hasClass('light-border-bottom')) {
                        $(this).addClass('light-border-bottom');
                    }

                    if (parsedDate > utolsoRegTime) {
                        if (utolsoRegTime != false) {
                            result.find("tr:gt(0)").each(function () {
                                $(this).removeAttr('id', 'jsg-last-seen');
                            });
                            $(this).attr('id', 'jsg-last-seen');
                        }
                    }

                    var ipTd = $(this).find("td:eq(2)");
                    var ip = ipTd.text();
                    if (ipCounts[ip] > 1 && inArray(ip, trollIps)) {
                        ipTd.addClass('jsg-red jsg-bold');
                    }
                    else if (ipCounts[ip] > 1 && !inArray(ip, trollIps)) {
                        ipTd.addClass('jsg-bold');
                    }
                    else if (ipCounts[ip] <= 1 && inArray(ip, trollIps)) {
                        ipTd.addClass('jsg-red');
                    }
                    else if (ipCounts[ip] > 1 && inArray(getIpPart(ip), trollIpRanges)) {
                        ipTd.addClass('jsg-orange');
                    }

                    var link = $(this).find("a");
                    var href = link.attr('href');
                    var uName = link.text();
                    if (inArray(uName, bannoltUserek)) {
                        link.addClass('jsg-athuzott');
                        $(this).addClass('jsg-bannolt');
                    } else {
                        var mehet = true;
                        $.ajax({
                            url: href,
                            type: 'GET',
                            async: false
                        }).done(function (res) {
                            res = res.replace(/\n/g, "");
                            res = res.replace(/  /g, "");
                            if (res.search("A felhasználó ki van tiltva") >= 0) {
                                mehet = false;
                            }
                        });

                        if (mehet) {
                            uName = uName.replace(' ', '');
                            var uid = link.attr('href').replace('/felhasznalo/', '');
                            //var banStr = '<a href="' + document.URL + '" class="jsg-hekked-ban jsg-cset-ban" data-info={"nick":"' + uName + '","user_id":' + uid + '}>[5p]</a>';
                            var banStr = '<a href="' + document.URL + '" class="jsg-vegleges-ban jsg-hekked-ban jsg-cset-ban" data-info={"nick":"' + uName + '","user_id":' + uid + '}>[k]</a>';
                            if ($(this).find('a.jsg-hekked-ban').length == 0 && $(this).find('a.jsg-hekked-mod').length == 0) {
                                $(this).append('<td>' + banStr + '</td>');
                            }
                        } else {
                            link.addClass('jsg-athuzott');
                            $(this).addClass('jsg-bannolt');
                            if ($(this).find('a.jsg-hekked-ban').length == 0 && $(this).find('a.jsg-hekked-mod').length == 0) {
                                $(this).append('<td></td>');
                            }
                        }
                    }
                });

                var resultHtml = '<table id="jsg-lastusers">' + result.html() + '</table>';
                var newDiv = '<section id="jsg-utolso-regisztraltak" class=""><ul class="list-unstyled">' + resultHtml + '</ul></section>';
                var utolsoRegDiv = $("#jsg-utolso-regisztraltak");
                if (utolsoRegDiv.length != 0) {
                    utolsoRegDiv.remove();
                }
                $(newDiv).insertAfter('#forum-chat');
                addBanEvent();

                $("#jsg-utolso-regisztraltak tr:not(.jsg-lastusers-thead)").click(function() {
                    var now = new Date().getTime();
                    setSetting('utolsoreg-time', now);
                });

                $('.jsg-lastusers-thead').click(function() {
                    $("#jsg-utolso-regisztraltak tr:not(.jsg-lastusers-thead)").toggle(600);
                });
            }
        });
    }
}

var addBanEvent = function() {
    $('.jsg-hekked-ban').on('click', function (e) {
        e.preventDefault();
        var info = $(this).data('info');

        if (confirm('Biztos bannolod véglegesen: ' + info.nick + '-t?')) {
            $.ajax({
                url: '/moderator/kitiltas/' + info.user_id,
                type: 'GET'
            }).done(function (res) {
                if (res.status == 'error') {
                    console.log(res.msg);
                    return;
                }
            });

            return false;
        }
        else {
            return false;
        }

    });
};

function getIpPart(ip) {
    var parts = ip.split(".");
    return parts[0] + '.' + parts[1] + '.' + parts[2];
}

if (modUser && getSetting('modstuff') == 1) {
    var bannoltUserek = [];
    var trollIps = [];
    var trollIpRanges = [];
    var ipPhp = ipUrl;

    var ipsToDiv = '';
    var bannedIpData = '<div id="ipdiv" class="options cf center">';
    bannedIpData += '<h3 class="title">Troll IP címek</h3>';

    bannedIpData += '<span class="row">';
    //bannedIpData += <div class="options_chat_label">IP:</div>';
    bannedIpData += '<label><input id="newip" type="text"><i></i></label>';
    bannedIpData += '</span>';
    bannedIpData += '<span class="row">';
    bannedIpData += '<input type="button" value="IP mentése" id="ip_save_button" onclick="ip_save();">';
    bannedIpData += '</span>';

    bannedIpData += '<div class="separator"></div>';
    bannedIpData += '<p id="iplist"></p>';
    bannedIpData += '<div id="ipwait" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>';
    bannedIpData += '</div>';

    $.ajax({
        url: '/moderator/buntetopontok/',
        type: 'GET',
        async: false
    }).done(function (res) {
        res = res.replace(/\n/g, "");
        res = res.replace(/  /g, "");
        var result = res.match(/<a href=\"\/felhasznalo\/[0-9]+\">[a-zA-Z0-9 ]+<\/a><\/b><\/td><td>(<div class=\"icon alert\"><\/div>)*/g);
        result.forEach(function (entry) {
            var aResult = entry.match(/<a href=\"\/felhasznalo\/[0-9]+\">[a-zA-Z0-9 ]+<\/a>/g);
            var pontokArr = entry.match(/<td>(<div class=\"icon alert\"><\/div>)*/g);
            var pontok = (pontokArr[0].split("icon").length - 1);
            var name = $(entry).text();
            if (!inArray(name, bannoltUserek) && pontok >= 5) {
                bannoltUserek.push(name);
            }
        });
    });

    var bannedIpFunc = function() {
        var bannedIpDiv = $(bannedIpData);
        bannedIpDiv.find('#iplist').html(ipsToDiv);
        return bannedIpDiv[0].outerHTML;
    };

    var createIpModal = function() {
        createModal(bannedIpFunc, true);
    };

    var getIps = function() {
        $.ajax({
            url: ipPhp,
            dataType: 'json',
            type: 'GET'
        }).done(function (res) {
            trollIps = res;
        });

        if (trollIps.length > 0) {
            for (var i in trollIps) {
                var partIp = getIpPart(trollIps[i]);
                if (!inArray(partIp, trollIpRanges)) {
                    trollIpRanges.push(partIp);
                }
            }

            var ipList = $('#iplist');
            for (var i in trollIps) {
                ipList.html(ipList.html() + trollIps[i] + '<br />');
                ipsToDiv += trollIps[i] + '<br />';
            }

            checkModDiv();
            clearInterval(getIpsInterval);

            $('#ipwait').remove();
            console.log('%c Banned IPs loaded.', log_style_done);
        }
    };

    var ip_save = function() {
        var ipSaveButton = $('#ip_save_button');
        var ipToSave = $('#newip').val();
        if (ipToSave != '' && !inArray(ipToSave, trollIps)) {
            ipSaveButton.val("IP mentése...");
            $.ajax({
                url: ipPhp + '?ip=' +ipToSave ,
                type: 'GET'
            }).done(function (res) {
                ipSaveButton.val("IP mentve!");
            });
        }
    };

    setTimeout(checkModDiv, 3000);
    setInterval(checkModDiv, 30000);
    setTimeout(getIps, 3000);
    var getIpsInterval = setInterval(getIps, 30000);

    var bannedIpsInterval = function() {
        $('#sgcset-header').append('<a class="righthead bannedips" onclick="createIpModal()"><i></i></a>');
        lines_color();
    };
    setTimeout(bannedIpsInterval, 5000);
}

console.log('%c Mod stuff loaded. ', log_style_done);