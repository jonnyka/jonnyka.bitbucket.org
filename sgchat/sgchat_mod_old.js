console.log('%c Mod init loading started... ', log_style_start);0

if (modUser) {
    var modLoggedIn = modLoggedCheck();
    console.log('modLoggedIn: ' + modLoggedIn);

    // Cset ban gomb, utolso regisztraltak
    var csetBan = function () {
        $('#forum-chat-user-list').find('li').each(function () {
            var dat = $(this);
            var firstA = $(this).find('a:last-child');
            var allA = $(this).find('a');
            var uName = firstA.text();
            var uidStr = $(this).attr('id');
            var uid = uidStr.replace('userlist-user-', '');

            if (!$(this).hasClass('jsg-tded')) {
                $(this).addClass('jsg-tded');
                firstA.addClass('jsg-csetname-new');
                allA.addClass('jsg-csetname-old');
            }

            if (inArray(uName, bannoltUserek)) {
                firstA.addClass('jsg-athuzott');
            }
            else {
                uName = uName.replace(' ', '');
                var banStr = '<td class="jsg-csetfunc"><a href="' + document.URL + '" class="jsg-hekked-ban jsg-cset-ban" data-info={"nick":"' + uName + '","user_id":' + uid + '}>[5p]</a>';
                banStr += '<a href="' + document.URL + '" class="jsg-vegleges-ban jsg-hekked-ban jsg-cset-ban" data-info={"nick":"' + uName + '","user_id":' + uid + '}>[k]</a></td>';
                if (!modLoggedIn) {
                    banStr = '<td class="jsg-csetfunc"><a class="jsg-hekked-mod jsg-cset-mod" href="' + document.URL + '">[m]</a></td>';
                }
                if ($(this).find('a.jsg-hekked-ban').length == 0 && $(this).find('a.jsg-hekked-mod').length == 0) {
                    //$(banStr).insertBefore($(this).find('span.age'));
                }
            }
        });

        var utolsoReg = getSetting('jsg-mod-utolsoszam');
        if ($("#jsg-utolso-regisztraltak").length == 0 && modLoggedIn && utolsoReg != "0") {
            var jsgModTime = 'sgchat-mod-time';
            var savedTime = getSetting(jsgModTime);
            $.ajax({
                url: '/moderator/utolsok/',
                type: 'GET',
                async: false
            }).done(function (res) {
                    res = res.replace(/\n/g, "");
                    res = res.replace(/  /g, "");
                    var result = $(res).find("table#lastusers");

                    var ips = [];
                    result.find("tr").each(function () {
                        var ip = $(this).find("td:eq(2)").text();
                        ips.push(ip);
                    });

                    var ipCounts = {};
                    for (var i = 0; i < ips.length; i++) {
                        var num = ips[i];
                        ipCounts[num] = ipCounts[num] ? ipCounts[num] + 1 : 1;
                    }

                    result.find("tr:gt(" + utolsoReg + ")").remove();
                    result.find("tr:eq(0)").addClass('jsg-lastusers-thead');
                    result.find("tr:eq(0)").append('<th>Kitiltás</th>');
                    result.find("tr:gt(0)").each(function () {
                        var d = $(this).find("td:eq(0)").text();
                        var dateString = d.replace(/-/g, '/');
                        var parsedDate = Date.parse(dateString);
                        if (!$(this).hasClass('light-border-bottom')) {
                            $(this).addClass('light-border-bottom');
                        }

                        if (parsedDate > savedTime) {
                            if (savedTime != false) {
                                result.find("tr:gt(0)").each(function () {
                                    $(this).removeAttr('id', 'jsg-last-seen');
                                });
                                $(this).attr('id', 'jsg-last-seen');
                            }
                            var now = new Date().getTime();
                            setSetting(jsgModTime, now);
                        }

                        var ipTd = $(this).find("td:eq(2)");
                        var ip = ipTd.text();
                        if (ipCounts[ip] > 1 && inArray(ip, trollIps)) {
                            ipTd.addClass('jsg-red jsg-bold');
                        }
                        else if (ipCounts[ip] > 1 && !inArray(ip, trollIps)) {
                            ipTd.addClass('jsg-bold');
                        }
                        else if (ipCounts[ip] <= 1 && inArray(ip, trollIps)) {
                            ipTd.addClass('jsg-red');
                        }
                        else if (ipCounts[ip] > 1 && inArray(getIpPart(ip), trollIpRanges)) {
                            ipTd.addClass('jsg-orange');
                        }

                        var link = $(this).find("a");
                        var href = link.attr('href');
                        var uName = link.text();
                        if (inArray(uName, bannoltUserek)) {
                            link.addClass('jsg-athuzott');
                            $(this).addClass('jsg-bannolt');
                        } else {
                            var mehet = true;
                            $.ajax({
                                url: href,
                                type: 'GET',
                                async: false
                            }).done(function (res) {
                                    res = res.replace(/\n/g, "");
                                    res = res.replace(/  /g, "");
                                    if (res.search("A felhasználó ki van tiltva") >= 0) {
                                        mehet = false;
                                    }
                                });

                            if (mehet) {
                                uName = uName.replace(' ', '');
                                var uid = link.attr('href').replace('/felhasznalo/', '');
                                var banStr = '<a href="' + document.URL + '" class="jsg-hekked-ban jsg-cset-ban" data-info={"nick":"' + uName + '","user_id":' + uid + '}>[5p]</a>';
                                banStr += '<a href="' + document.URL + '" class="jsg-vegleges-ban jsg-hekked-ban jsg-cset-ban" data-info={"nick":"' + uName + '","user_id":' + uid + '}>[k]</a>';
                                if (!modLoggedIn) {
                                    banStr = '<a class="jsg-hekked-mod jsg-cset-mod" href="' + document.URL + '">[m]</a>';
                                }
                                if ($(this).find('a.jsg-hekked-ban').length == 0 && $(this).find('a.jsg-hekked-mod').length == 0) {
                                    $(this).append('<td>' + banStr + '</td>');
                                }
                            } else {
                                link.addClass('jsg-athuzott');
                                $(this).addClass('jsg-bannolt');
                                if ($(this).find('a.jsg-hekked-ban').length == 0 && $(this).find('a.jsg-hekked-mod').length == 0) {
                                    $(this).append('<td></td>');
                                }
                            }
                        }
                    });

                    var resultHtml = '<table id="jsg-lastusers">' + result.html() + '</table>';
                    var newDiv = '<section id="jsg-utolso-regisztraltak" class=""><ul class="list-unstyled">' + resultHtml + '</ul></section>';
                    $(newDiv).insertAfter('#forum-wrap');
                });
        }

        addBanEvent(true);
        addModEvent($('.jsg-hekked-mod'));
    };

    setTimeout(csetBan, 2000);
    setInterval(csetBan, 10000);

    function addBanEvent(reload) {
        $('.jsg-hekked-ban').on('click', function (e) {
            e.preventDefault();
            var info = $(this).data('info');
            var cause = '<a style="color: #660000; font-weight: bold; padding: 5px;" href="http://kocsog.eu/ban/" target="_blank">http://kocsog.eu/ban/</a>';

            if ($(this).hasClass('jsg-vegleges-ban')) {
                if (confirm('Biztos bannolod véglegesen: ' + info.nick + '-t?')) {
                    $.ajax({
                        url: '/moderator/kitiltas/' + info.user_id,
                        type: 'GET'
                    }).done(function (res) {
                        if (res.status == 'error') {
                            alert(res.msg);
                            if (reload) {
                                location.reload();
                            }
                            return;
                        } else {
                            if (reload) {
                                location.reload();
                            }
                        }
                    });

                    return false;
                }
                else {
                    return false;
                }
            }
            else {
                if (confirm('Biztos adsz 5 pontot neki: ' + info.nick + ' ?')) {
                    $.ajax({
                        url: '/moderator/pontosztas/' + info.user_id,
                        type: 'POST',
                        data: {
                            'points': 5,
                            'cause': cause
                        }
                    }).done(function (res) {
                        if (res.status == 'error') {
                            alert(res.msg);
                            if (reload) {
                                location.reload();
                            }
                            return;
                        } else {
                            if (reload) {
                                location.reload();
                            }
                        }
                    });

                    return false;
                }
                else {
                    return false;
                }
            }
        });
    }
}

console.log('%c Mod stuff loaded. ', log_style_done);