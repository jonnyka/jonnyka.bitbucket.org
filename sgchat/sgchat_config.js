/**
 * console.log style
 */
var log_style_start = [
    'background-color: #222',
    'color: orange',
    'display: inline-block',
    'border-left: 5px solid orange',
    'border-radius: 0 17px 17px 0',
    'padding: 10px 20px 10px 10px',
    'line-height: 35px',
    'font-weight: bold'
].join(';');

var log_style_done = [
    'background-color: #222',
    'color: lime',
    'display: inline-block',
    'border-left: 5px solid lime',
    'border-radius: 0 17px 17px 0',
    'padding: 10px 20px 10px 10px',
    'line-height: 35px',
    'font-weight: bold'
].join(';');

console.log('%c Config loading started... ', log_style_start);

/**
 * Audio files
 * http://www.myinstants.com/
 *
 * @type {Array}
 */
var songs = [
    { str: 'badumtss', mp3: 'badumtss' },
    { str: 'bored', mp3: 'bored' },
    { str: 'bye', mp3: 'bye' },
    { str: 'cs_ban', mp3: 'cs_ban' },
    { str: 'cs_gore', mp3: 'cs_gore' },
    { str: 'cs_jajkitlatok', mp3: 'cs_jajkitlatok' },
    { str: 'cs_jovan', mp3: 'cs_jovan' },
    { str: 'cs_kinyirni', mp3: 'cs_kinyirni' },
    { str: 'cs_menje', mp3: 'cs_menje' },
    { str: 'developers', mp3: 'developers' },
    { str: 'fart', mp3: 'dry-fart' },
    { str: 'fuck-you', mp3: 'fuck-you' },
    { str: 'gay', mp3: 'gay' },
    { str: 'goodnight', mp3: 'goodnight' },
    { str: 'gtfo', mp3: 'gtfo' },
    { str: 'haha', mp3: 'ha_ha' },
    { str: 'happytree', mp3: 'happytree' },
    { str: 'hello-faggots', mp3: 'hello-faggots' },
    { str: 'holyshit', mp3: 'holy_shit' },
    { str: 'hots', mp3: 'hots' },
    { str: 'iifp', mp3: 'inet_is_for_porn' },
    { str: 'monsterkill', mp3: 'monster_kill' },
    { str: 'nazimodz', mp3: 'nazis' },
    { str: 'nein', mp3: 'nein_nein' },
    { str: 'noooo', mp3: 'star_wars_noooo' },
    { str: 'okay', mp3: 'okay' },
    { str: 'over9000', mp3: 'over_9000' },
    { str: 'punch', mp3: 'punch' },
    { str: 'sigh', mp3: 'sigh' },
    { str: 'stfu', mp3: 'shut-up' },
    { str: 'taps', mp3: 'applause' },
    { str: 'thanks', mp3: 'thanks' },
    { str: 'welcome', mp3: 'welcome' },
    { str: 'who-cares', mp3: 'who-cares' },
    { str: 'wololo', mp3: 'wololo' },
    { str: 'yes', mp3: 'yes' }
];

/**
 * BBcodes
 *
 * @type {Array}
 */
var bbcodes = [
    { bb: 'b', html: 'b' },
    { bb: 'i', html: 'i' },
    { bb: 'u', html: 'u' },
    { bb: 'small', html: 'small' },
    { bb: 'del', html: 'del'},
    { bb: 'sub', html: 'sub' },
    { bb: 'sup', html: 'sup' },
    { bb: 'mark', html: 'mark' }
];

/**
 * We can add pictures to the smileys.
 * Put the image in the images/ folder with .png format,
 * or if you use a different format, add key 'ext',
 * and use the image's filename in the 'mire' key,
 * and add 'kep': true.
 * Add key 'ujsor' if you want a new line in the smiley list.
 *
 * @type {Array}
 */
var smileys = [
    { mit: ':pixxel', mire: 'pixxel', kep: true, sgteam: true },
    { mit: ':dzsini', mire: 'dzsini', kep: true, sgteam: true },
    { mit: ':neo', mire: 'neo', kep: true, sgteam: true },
    { mit: ':merlin', mire: 'merlin', kep: true, sgteam: true },
    { mit: ':maximus', mire: 'maximus', kep: true, sgteam: true },
    { mit: ':cater', mire: 'cater', kep: true, sgteam: true },
    { mit: ':jonny', mire: 'jonny', kep: true, sgteam: true },
    { mit: ':galamb', mire: 'galamb', kep: true, sgteam: true },
    { mit: ':kata', mire: 'kata_lego', kep: true, sgteam: true },
    { mit: ':kulonc', mire: 'kulonc', kep: true, sgteam: true },
    { mit: ':tugyi', mire: 'tugyi', kep: true, sgteam: true },
    { mit: ':peet', mire: 'peet', kep: true, sgteam: true },
    { mit: ':byz', mire: 'byz', kep: true, sgteam: true },
    { mit: ':lasden', mire: 'lasden', kep: true, sgteam: true },
    { mit: ':sonic', mire: 'sonic', kep: true, sgteam: true },
    { mit: ':nihilus', mire: 'bohoc', kep: true, sgteam: true },
    { mit: ':konti', mire: 'konti', kep: true, sgteam: true },
    { mit: ':narxis', mire: 'narxis', kep: true, sgteam: true },
    { mit: ':szabi', mire: 'szabi', kep: true, sgteam: true },
    { mit: ':jim', mire: 'jim', kep: true, sgteam: true },

    { mit: '#nazimodz', mire: 'nazimodz', kep: true, ujsor: true, ext: 'gif' },
    { mit: ':cicapacsi', mire: 'cicapacsi', kep: true},
    { mit: ':popcorncica', mire: 'popcorn2', kep: true },
    { mit: ':popcorn', mire: 'popcorn', kep: true },
    { mit: ':kaktusz', mire: 'kaktusz', kep: true },
    { mit: ':kave', mire: 'kave', kep: true },
    { mit: ':ok', mire: 'ok', kep: true },
    { mit: ':no', mire: 'no', kep: true },
    { mit: ':batman', mire: 'batman', kep: true },
    { mit: ':superman', mire: 'superman', kep: true },
    { mit: ':lovag', mire: 'lovag', kep: true },
    { mit: ':sith', mire: 'sith', kep: true },
    { mit: ':robot', mire: 'robot', kep: true },
    { mit: ':csirke', mire: 'csirke', kep: true },
    { mit: ':unikornyan', mire: 'unikornyan', kep: true },
    { mit: ':kocsis', mire: 'kocsis', kep: true },
    { mit: ':troll', mire: 'troll', kep: true },
    { mit: ':ork', mire: 'ork', kep: true },
    { mit: ':negro', mire: 'negro', kep: true },
    { mit: ':transzi', mire: 'transzi', kep: true },
    { mit: ':gay', mire: 'gay', kep: true },
    { mit: ':idiota', mire: 'idiota', kep: true },
    { mit: ':merges', mire: 'merges', kep: true },
    { mit: ':truestory', mire: 'truestory', kep: true },
    { mit: ':uol', mire: 'uol', kep: true },
    { mit: ':tits', mire: 'tits', kep: true, ext: 'gif'},
    { mit: ':dance', mire: 'dance', kep: true, ext: 'gif' },
    { mit: ':nyufitappancs', mire: 'nyufitappancs', kep: true, ext: 'gif' },

    { mit: ':hmm', mire: 'hmm', kep: true, ext: 'gif' },
    { mit: ':igaz', mire: 'igaz', kep: true, ext: 'gif' },
    { mit: ':mosoly', mire: 'mosoly', kep: true, ext: 'gif' },
    { mit: ':nnem', mire: 'nnem', kep: true, ext: 'gif' },
    { mit: ':nyalcsorg', mire: 'nyalcsorg', kep: true, ext: 'gif' },
    { mit: ':nyelv', mire: 'nyelv', kep: true, ext: 'gif' },
    { mit: ':smarty', mire: 'okoska', kep: true, ext: 'gif' },
    { mit: ':szetvert', mire: 'szetvert', kep: true, ext: 'gif' },

    { mit: '#szirena', mire: 'szirena', kep: true, ext: 'gif' },
    { mit: '#yolo', mire: 'yolo', kep: true, ext: 'gif' },
    { mit: '#swag', mire: 'swag', kep: true, ext: 'gif' },
    { mit: '#420blazeit', mire: '420blazeit', kep: true, ext: 'png' },
    { mit: '#snoop', mire: 'snoop', kep: true, ext: 'gif' },
    { mit: '#360noscope', mire: '360noscope', kep: true, ext: 'png' },

    { mit: '(y)', mire: 'like', kep: true, ujsor: true },
    { mit: ':android', mire: 'android' },
    { mit: ':apple', mire: 'apple' },
    { mit: ':)', mire: 'smile' },
    { mit: ':(', mire: 'sad' },
    { mit: ':d', mire: 'laugh' },
    { mit: '8)', mire: 'cool' },
    { mit: ';)', mire: 'wink' },
    { mit: ':o', mire: 'surprised' },
    { mit: ';(', mire: 'cry' },
    { mit: ':p', mire: 'tongue' },
    { mit: ':\\', mire: 'displeased' },
    { mit: ':|', mire: 'sleep' },
    { mit: '^^', mire: 'smile2' },
    { mit: ':@', mire: 'angry' },
    { mit: ':angel', mire: 'angel' },
    { mit: 'brb', mire: 'brb' },
    { mit: ':coffee', mire: 'coffee' },
    { mit: ':gamepad', mire: 'gamepad' },
    { mit: ':tap', mire: 'paw' },
    { mit: ':tel', mire: 'tel' },
    { mit: '<3', mire: 'hearth' },

    { mit: 'illuminati', mire: 'illuminati', kep: true },
    { mit: ':kappa', mire: 'kappa', kep: true },
    { mit: 'DansGame', mire: 'DansGame', kep: true },
    { mit: 'WutFace', mire: 'WutFace', kep: true },
    { mit: 'PogChamp', mire: 'PogChamp', kep: true },
    { mit: 'BabyRage', mire: 'BabyRage', kep: true },
    { mit: 'ResidentSleeper', mire: 'ResidentSleeper', kep: true },
    { mit: '4Head', mire: '4Head', kep: true },
    { mit: 'FailFish', mire: 'FailFish', kep: true },
    { mit: 'Kreygasm', mire: 'Kreygasm', kep: true },
    { mit: 'SourPls', mire: 'sourpls', kep: true, ext: 'gif' },
    { mit: 'ANELE', mire: 'ANELE', kep: true },
    { mit: 'BasedGod', mire: 'BasedGod', kep: true },
    { mit: 'CiGrip', mire: 'CiGrip', kep: true },
    { mit: 'cmonBruh', mire: 'cmonBruh', kep: true },
    { mit: 'TriHard', mire: 'TriHard', kep: true },
    { mit: 'haHAA', mire: 'haHAA', kep: true },
    { mit: 'KKona', mire: 'KKona', kep: true },
    { mit: 'MingLee', mire: 'MingLee', kep: true },
    { mit: 'NotLikeThis', mire: 'NotLikeThis', kep: true },
    { mit: 'PedoBear', mire: 'PedoBear', kep: true },
    { mit: 'FeelsBadMan', mire: 'FeelsBadMan', kep: true },
    { mit: 'FeelsGoodMan', mire: 'FeelsGoodMan', kep: true },
    { mit: 'FeelsAmazingMan', mire: 'FeelsAmazingMan', kep: true },
    { mit: 'FeelsBirthdayMan', mire: 'FeelsBirthdayMan', kep: true },
    { mit: ':LUL:', mire: 'LUL', kep: true },

    { mit: 'lirikTRASH', mire: 'lirikTRASH', kep: true },
    { mit: 'lirikOBESE', mire: 'lirikOBESE', kep: true },
    { mit: 'lirikNICE', mire: 'lirikNICE', kep: true },
    { mit: 'lirikPOOP', mire: 'lirikPOOP', kep: true },
    { mit: 'lirikAppa', mire: 'lirikAppa', kep: true },
    { mit: 'lirikHYPE', mire: 'lirikHYPE', kep: true },
    { mit: 'lirikMLG', mire: 'lirikMLG', kep: true },
    { mit: 'lirikTENK', mire: 'lirikTENK', kep: true },
    { mit: 'lirikTEN', mire: 'lirikTEN', kep: true },
    { mit: 'lirikCRY', mire: 'lirikCRY', kep: true },
    { mit: 'lirikHug', mire: 'lirikHug', kep: true },
    { mit: 'lirikGasm', mire: 'lirikGasm', kep: true },
    { mit: 'lirikRIP', mire: 'lirikRIP', kep: true },
    { mit: 'lirikCLENCH', mire: 'lirikCLENCH', kep: true },
    { mit: 'lirikThump', mire: 'lirikThump', kep: true },
    { mit: 'lirikDEAD', mire: 'lirikDEAD', kep: true },
    { mit: 'lirikGOTY', mire: 'lirikGOTY', kep: true },
    { mit: 'lirikCRASH', mire: 'lirikCRASH', kep: true },
    { mit: 'lirikREKT', mire: 'lirikREKT', kep: true },
    { mit: 'lirikPVP', mire: 'lirikPVP', kep: true },
    { mit: 'lirikWc', mire: 'lirikWc', kep: true },
    { mit: 'lirikBB', mire: 'lirikBB', kep: true },
    { mit: 'lirikBLIND', mire: 'lirikBLIND', kep: true },
    { mit: 'lirikCHAMP', mire: 'lirikCHAMP', kep: true },
    { mit: 'lirikCLAP', mire: 'lirikCLAP', kep: true },
    { mit: 'lirikDJ', mire: 'lirikDJ', kep: true },
    { mit: 'lirikFAKE', mire: 'lirikFAKE', kep: true },
    { mit: 'lirikFEELS', mire: 'lirikFEELS', kep: true },
    { mit: 'lirikFR', mire: 'lirikFR', kep: true },
    { mit: 'lirikGREAT', mire: 'lirikGREAT', kep: true },
    { mit: 'lirikLEAN', mire: 'lirikLEAN', kep: true },
    { mit: 'lirikLEWD', mire: 'lirikLEWD', kep: true },
    { mit: 'lirikLUL', mire: 'lirikLUL', kep: true },
    { mit: 'lirikNON', mire: 'lirikNON', kep: true },
    { mit: 'lirikNOT', mire: 'lirikNOT', kep: true },
    { mit: 'lirikOHGOD', mire: 'lirikOHGOD', kep: true },
    { mit: 'lirikOK', mire: 'lirikOK', kep: true },
    { mit: 'lirikPOOL', mire: 'lirikPOOL', kep: true },
    { mit: 'lirikPUKE', mire: 'lirikPUKE', kep: true },
    { mit: 'lirikSALT', mire: 'lirikSALT', kep: true },
    { mit: 'lirikSCARED', mire: 'lirikSCARED', kep: true },
    { mit: 'lirikSHUCKS', mire: 'lirikSHUCKS', kep: true },
    { mit: 'lirikWEEB', mire: 'lirikWEEB', kep: true },
    { mit: 'lirikSS', mire: 'lirikS', kep: true },
    { mit: 'lirikPP', mire: 'lirikP', kep: true },
    { mit: 'lirikHH', mire: 'lirikH', kep: true },
    { mit: 'lirikCC', mire: 'lirikC', kep: true },
    { mit: 'lirikBB', mire: 'lirikB', kep: true },
    { mit: 'lirikDD', mire: 'lirikD', kep: true },
    { mit: 'lirikLL', mire: 'lirikL', kep: true },
    { mit: 'lirikMM', mire: 'lirikM', kep: true },
    { mit: 'lirikWW', mire: 'lirikW', kep: true },
    { mit: 'lirikFF', mire: 'lirikF', kep: true },
    { mit: 'lirikOO', mire: 'lirikO', kep: true }
];

/**
 * Custom css definitions
 * The keys must be the file names like this: /custom_css/{key}.css
 * The values are the values of the select option.
 *
 * @type {Array}
 */
var customCssFiles = [
    { 'lines': 'Lines' },
    { 'material_empty' : 'Material' },
    { 'material_empty_dark' : 'Material Dark' }
];

// All the css files, including the default.
var allCssFiles;
allCssFiles = customCssFiles;
allCssFiles.unshift({'default': 'Világos'});

/**
 * Localstorage settings for the options div with default values.
 * Type is the form element type.
 * Id is the id used in the localstorage and the form element.
 * Default is the default initial value of the setting.
 * Name is the label of the form element.
 * If its a select element, options stores the options.
 * Help is well.. help.
 * Separator is needed if you want a divider on top of the form element
 *
 * @type {Array}
 */
var localStoreSettings = [
    { type: 'disabled', id: 'favs', "default": 1, name: 'Kedvencek', help: '' },
    { type: 'select', id: 'custom_css', "default": 'material_empty', name: 'Kinézet', help: 'A főoldal kinézete.', options: allCssFiles },
    { type: 'checkbox', id: 'responsive', "default": 1, name: 'Reszponzív', help: 'Ha be van kapcsolva, a megjelenés fluid lesz, azaz kitölti az egész képernyőt.' },
    { type: 'checkbox', id: 'header', "default": 0, name: 'SG fejléc', help: 'Ha be van kapcsolva, megjelenik felül az SG-s kék fejléc.' },
    { type: 'checkbox', id: 'smiley', "default": 1, name: 'Szmájlik', help: 'Ha be van kapcsolva, a cseten megjelennek a szmájlik.' },
    { type: 'checkbox', id: 'embed', "default": 1, name: 'Beágyazott tartalmak', help: 'Ha be van kapcsolva, a csetre linkelt youtube, soundcloud és vimeo tartalmak beágyazódnak.' },
    { type: 'select', id: 'chatnum', "default": 300, name: 'Chat üzenetek száma', help: 'Hány hozzászólást jelenjen meg a cset ablakban. Kell neki egy F5!', options: [
        { 100: '100' },
        { 200: '200' },
        { 300: '300' },
        { 400: '400' },
        { 500: '500' }
    ]},
    { type: 'select', id: 'gifnum', "default": 25, name: 'Gif api találatok száma', help: 'Amikor a gif apit használod keresésre, hány találat jöjjön vissza.', options: [
        { 10: '10' },
        { 25: '25' },
        { 50: '50' },
        { 100: '100' }
    ]},
    { type: 'checkbox', id: 'autoplay', "default": 1, name: 'Auto zene lejátszás', help: 'Ha be van kapcsolva, automatikusan lejátszódnak a csetre érkezett hangok.' },
    { type: 'checkbox', id: 'autoscroll', "default": 1, name: 'Auto chat görgetés', help: 'Ha be van kapcsolva, a csetablak automatikusan az újonnan érkezett üzenetekre (alulra) ugrik.' },
    { type: 'checkbox', id: 'chatshow', "default": 1, name: 'Cset megjelenítése', help: 'Ha be van kapcsolva, látszik a cset ablak. Ugyanaz a funkciója, mint a duplanyilas ikonnak.' },
    { type: 'checkbox', id: 'fbbox', "default": 0, name: 'Facebook box megjelenítése', help: 'Ha be van kapcsolva, látszik a facebook-os like doboz.' },
    { type: 'checkbox', id: 'thread', "default": 1, name: 'Thread sáv', help: 'Ha be van kapcsolva, a cset felső részében megjelenik egy téma sáv, hasonlóan mint IRC-en.' },
    { type: 'checkbox', id: 'compact', "default": 0, name: 'Kompakt nézet', help: 'Ha be van kapcsolva, sűríti az azonos kommentelőtől jövő kommenteket 1-1 dobozba.' },
    { type: 'checkbox', id: 'jedi', "default": 1, name: 'Jedi Mester megemlékezés', help: 'Fejlécben lévő apró megemlékező rész ki-be kapcsolása. R.I.P. Mester!' },
    { type: 'checkbox', id: 'snow', "default": 0, name: 'Hóesés', help: 'Ha be van kapcsolva, lesz hóesés az oldalon.' },
    { type: 'checkbox', id: 'candles', "default": 0, name: 'Gyertyák', help: 'Ha be van kapcsolva, lesznek gyertyák a cset ablakban.' },
    { type: 'checkbox', id: 'monster', "default": 1, name: 'Tóbiás', help: 'Ha be van kapcsolva, lesz egy lógó szörny, Tóbiás a cset ablakban.' },
    { type: 'checkbox', id: 'haloween', "default": 0, name: 'Halloween szörny', help: 'Ha be van kapcsolva, lesz egy halloweeni szörny a jobb alsó sarokban.' },
    { type: 'checkbox', id: 'macska', "default": 0, name: 'Rohanó macska', help: 'Ha be van kapcsolva, lesz egy rohanó macska animáció az oldal alján.' },
    { type: 'checkbox', id: 'kata', "default": 0, name: 'Kata nyuszi', help: 'Ha be van kapcsolva, működik a !katanyuszi parancs.' },
    { type: 'select', id: 'timeformat', "default": 'seconds', name: 'Idő formátum', help: 'A hozzászólások előtti idő formátuma. Ó = óra, P = perc, M = másodperc.', options: [
        { 'minutes': 'ÓÓ:PP' },
        { 'seconds': 'ÓÓ:PP:MM' }
    ]},
    { type: 'select', id: 'sgiframe', "default": 2, name: 'SG linkek', help: 'A bal oldali sávon és a topic listában lévő linkek, illetve a csetre linkelt sg topicok működését állítja.<br /><br />Normál: az alap működés, azaz rányílik az aktuális ablakra.<br /><br />Új oldalra: új oldalra nyílik.<br /><br />Iframe-be: a cset alatt egy iframe-be nyílik.', options: [
        { 0: 'Normál' },
        { 2: 'Új oldalra' },
        { 1: 'Iframe-be' }
    ]},

    { type: 'checkbox', id: 'twitch', "default": 0, name: 'Twitch beágyazás', help: 'Ha be van kapcsolva, a cset alatt megjelenik egy beágyazott twitch ablak a lenti twitch felhasználó stream-jével.', separator: true },
    { type: 'text', id: 'twitchuser', "default": 'lirik', name: 'Twitch felhasználó', help: 'Ha be van kapcsolva a Twitch beágyazás, az ide beírt twitch felhasználó streamje lesz látható a cset alatt. (pl. <b>lirik</b>)' },

    { type: 'checkbox', id: 'sidebar_newtopics', "default": 1, name: 'Legújabb témák', help: 'Ha be van kapcsolva, látható a sidebaron a legújabb témák doboz a bal oldali sávban', separator: true },
    { type: 'checkbox', id: 'sidebar_cooltopics', "default": 1, name: 'Népszerű témák', help: 'Ha be van kapcsolva, látható a népszerű témák doboz a bal oldali sávban' },
    { type: 'checkbox', id: 'sidebar_birthdays', "default": 1, name: 'Mai szülinaposok', help: 'Ha be van kapcsolva, látható a szülinaposok doboz a bal oldali sávban' },
    { type: 'checkbox', id: 'articles', "default": 0, name: 'Főoldali cikkek', help: 'Ha be van kapcsolva, látható a cikkeink doboz a cset alatt' },
    { type: 'checkbox', id: 'topics', "default": 0, name: 'Témák listája', help: 'Ha be van kapcsolva, láthatóak a témák a cset alatt' },
    { type: 'checkbox', id: 'gallery', "default": 0, name: 'Legújabb galériák', help: 'Ha be van kapcsolva, látható a Legújabb galériák menüpont bal oldalt.' },

    { type: 'checkbox', id: 'modstuff', "default": 0, mod: true, separator: true, name: 'Mod cuccok', help: 'Ha be van kapcsolva, aktiválódnak a moderátor funkciók. Újratölti az oldalt az átállítása.' },
    { type: 'select', id: 'utolsoreg', "default": 5, mod: true, name: 'Utolsó regisztrálók', help: 'Hányat mutasson az utoljára regisztráltak közül a cset alatt. Kikapcsolnál nem lesz ilyen táblázat, egyébként minél nagyobb a szám, annál lassabb.', options: [
        { 0: 'Kikapcsol' },
        { 5: '5' },
        { 10: '10' },
        { 25: '25' },
        { 50: '50' }
    ]}
];

/**
 * Admin users. Not used at the moment.
 *
 * @type {Array}
 */
var admins = [
    { name: 'j0nNyKa', url: '1333095063' },
    { name: 'Neocortex', url: '1411563165' },
    { name: 'pxxl', url: '1404811522' }
];

/**
 * Global variables.
 */
var pUrl = '//sg.hu/chat/php/',
    phpUrl = pUrl + 'init.php',
    readUserUrl = pUrl + 'read_user.php',
    readThreadUrl = pUrl + 'thread.php',
    gifSearchUrl = pUrl + 'gifsearch.php',
    gifApikey = 'Basic bE5kNTo0OTI3YWZmMDQwYzIwOTU2YTdkZWNiNmEzMTdkZjI3MzdhNjllNmVlNGUyZjMwMTY1ZDI1NTQzMTA0ZjQwZTIw',
    giphyApiKey = 'qln0TIONQJHYmJCmK01r2FiMrgOPJ6nS',
    ipUrl = pUrl + 'ip.php',
    sgNodeUrl = 'https://chat.sg.hu:18080',
    focused = true,
    msg_counter = 0,
    new_topics = 0,
    settings = [],
    age = [],
    threadData = [],
    mute_list = [],
    sidebar = "",
    clearName = "",
    initialized = false,
    interval = '',
    isResponsive = false,
    socketOnline = true,
    socket = null,
    socketl = null,
    sgTitle = "SG fórum",
    forumChatInput = $('#forum-chat-input'),
    identid = '',
    floodTime = '2000', // 3 sec
    clearNameTime = 5000,
    threadLength = 255,
    os = 'windows',
    userid = 0,
    videow = "476",
    videoh = "267",
    headerTop = $('#header-top'),
    $textInput = document.getElementById('forum-chat-input'),
    $chatListFrame = document.getElementById('list-frame'),
    $chatList = document.getElementById('forum-chat-list'),
    $userList = document.getElementById('forum-chat-user-list'),
    $users = {};

var activeMods = [
    "j0nNyKa",
    "pxxl",
    "Efreet",
    "Maximus4",
    "MerlinW",
    "Jim Morrison",
    "SZABl"
];
var mods = [
    "Efreet",
    "Mortimer",
    "Lteebee",
    "Pheel",
    "Kandurex",
    "Hmuda",
    "Sir Quno Jedi",
    "[NST]Cifu",
    "Cat",
    "Maximus4",
    "MerlinW",
    "pedrohsi",
    "Monoton",
    "j0nNyKa",
    "Jim Morrison",
    "domby",
    "dyy",
    "SHADOWEYES7",
    "SZABl",
    "Dzsini",
    "SuhiP"
];
var gods = [
    "Neocortex",
    "j0nNyKa",
    "pxxl"
];
var girls = [
    "Kata"
];
var retards = [
    "Caterpillar",
    "jazzy",
    "( ° ͜ʖ͡°)╭∩╮"
];
var gays = [
    "DeveloperZ",
    "_Galamb_",
    "Maala",
    "Különc",
    "PeeTeR",
    "byzhouse",
    "LasDen",
    "Botond",
    "smog15",
    "Terawatt",
    "Narxis",
    "Kontrakcio",
    "passatgt"
];
var hotsPlayers = [
    "Neocortex",
    "Caterpillar",
    "byzhouse",
    "j0nNyKa"
];

var videoTypes = {
    'webm': 'webm',
    'mp4': 'mp4',
    'ogv': 'ogg'
};

var is_chrome = false;
var isChromium = window.chrome,
    vendorName = window.navigator.vendor;
if (isChromium !== null && isChromium !== undefined && vendorName === "Google Inc.") {
    is_chrome = true;
}
var neoScrollSpeed = 8;
var otherScrollSpeed = 15;
if (is_chrome) {
    neoScrollSpeed = 1;
    otherScrollSpeed = 6;
}

/**
 * Current user's name.
 */
var userTextDiv = $('header.user-hello')[0];
var userText = $(userTextDiv).text();
var userTextParts = userText.split(',');
var replacedUserStr = userTextParts[1].replace('!', '');
var currentUser = replacedUserStr.replace('\n', '').trim();
var lastUser = 'xxx';

console.log('%c Config loaded. ', log_style_done);