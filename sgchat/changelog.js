console.log('%c Changelogs loading started... ', log_style_start);

/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Az új bejegyzést mindig a végére, mert fordított sorrendben listázza! *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

var changelogs = [
    { ver: '0.0.4', date: '2015.01.05', msg: [
        {row: 'Modal ablak bezáró gomb fix' },
        {row: 'Headerben "developers" hang csak "Auto zene lejátszás" bekapcsolásánál hallható' },
        {row: 'Changelog' }
    ]},
    { ver: '0.0.4', date: '2015.01.06', msg: [
        {row: 'font BBCode-nál kiválasztható a font típusa' }
    ]},
    { ver: '0.0.4', date: '2015.01.07', msg: [
        {row: 'új hangok:<br />&nbsp;&nbsp;&nbsp;&nbsp;!cs_ban, !cs_jajkitlatok, !cs_jovan, !cs_kinyirni, !cs_menje' },
        {row: 'autoscroll fix' },
        {row: 'Safari extension fix' },
        {row: 'beállítások fix' }
    ]},
    { ver: '0.0.4', date: '2015.01.08', msg: [
        {row: 'console.log stílusok' },
        {row: 'TimeToColor téma' }
    ]},
    { ver: '0.0.5', date: '2015.01.11', msg: [
        {row: 'Új kép: :negro' },
        {row: 'input mező, küldés gomb átrendezés' }
    ]},
    { ver: '0.0.5', date: '2015.01.12', msg: [
        {row: 'Typeahead funkció' },
        {row: 'Typeahead funkció kivéve (bug)' },
        {row: 'Safari extension fix' }
    ]},
    { ver: '0.0.5', date: '2015.01.12', msg: [
        {row: 'pár fix a kódban' },
        {row: 'pár fix a css-ben' }
    ]},
    { ver: '0.0.5', date: '2015.01.15', msg: [
        {row: 'Új kép: :truestory' }
    ]},
    { ver: '0.0.5', date: '2015.01.16', msg: [
        {row: 'Modal ablak fix' }
    ]},
    { ver: '0.0.5', date: '2015.01.17', msg: [
        {row: 'online-offline jelzőre title' }
    ]},
    { ver: '0.0.5', date: '2015.01.18', msg: [
        {row: '"Nem írhatsz a csetre" fix' },
        {row: 'Értesítés küldése, ha frissült az addon' }
    ]},
    { ver: '0.0.5', date: '2015.01.19', msg: [
        {row: '"Ne floodolj" fix' },
        {row: 'Changelog frissítés jelzése' }
    ]},
    { ver: '0.0.5', date: '2015.01.20', msg: [
        {row: 'Modal ablak fix' },
        {row: 'Új képek:<br />&nbsp;&nbsp;&nbsp;&nbsp;:hmm, :igaz, :mosoly, :nnem, :nyalcsorg, :nyelv, :okoska, :szetvert' }
    ]}
];

function changelog() {
	var changelogsDiv = '<div class="changelogs">';
		changelogsDiv += '<h3 class="title">ChangeLogs</h3>';
		changelogsDiv += '<ul>';

	for (i in changelogs) {
		var ver	= changelogs[i]['ver'],
			date = changelogs[i]['date'],
			msg = changelogs[i]['msg'],
            msg_m = '<ul>',
            msg_cont = '<div class="msg">',
			header = '<h3 class="changes"><span class="ver">v' + ver + '</span>' + '<span class="date">' + date + '</span></h3>';

        for (j in msg) {
            var row = msg[j]['row'];
            msg_m += '<li><span class="c-row">' + row + '</span></li>';
        }

        msg_m += '</ul>';

        msg_cont += msg_m;
        msg_cont += '</div>';

		var str = '<li>' + header + msg_cont + '</li>';

		changelogsDiv += str;
	}

	changelogsDiv += '</ul></div>';

	return changelogsDiv;
}

/*
 * How many new log added...
 */
var newLogs = 0,
    oldLogs = getSetting('oldLogs');

if (!oldLogs) {
    setSetting('oldLogs', '0');
}

for (i in changelogs) {
    var msg = changelogs[i]['msg'];
    for (j in msg) {
        newLogs ++
    }
}

if (newLogs != oldLogs) {
    var diff = newLogs - oldLogs;
    //$('<span class="badge">' + diff + '</span>').prependTo('#sgcset-header a.logs');
}

$('#sgcset-header a.logs').click(function(){
    setSetting('oldLogs', newLogs);
    $('#sgcset-header a.logs .badge').fadeOut(500);
});


console.log('%c Changelogs loaded. ', log_style_done);