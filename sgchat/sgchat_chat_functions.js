console.log('%c Chat command functions loading started... ', log_style_start);

/**
 * Responsive blocks resize.
 */
function blocksResponsive() {
    if ($(window).width() < 1880) {
        $('#forum-wrap .forums-block .col1').attr('style', 'width: 42% !important');
        $('#forum-wrap .forums-block > header .col1').attr('style', 'font-size: 13px !important; width: 42% !important');
    }
    if ($(window).width() < 1590) {
        $('#forum-wrap .forums-block').removeAttr('style');
        $('#forum-wrap .forums-block header').removeAttr('style');
        $('#forum-wrap .forums-block .col1').removeAttr('style');
        $('#forum-wrap .forums-block .col2').removeAttr('style');
        $('#forum-wrap .forums-block .col3').removeAttr('style');
        $('#forum-wrap .forums-block > header .col1').removeAttr('style');
    }
}

/**
 * Sets responsive design.
 */
function setResponsive() {
    if (!isResponsive) {
        var csetUsers = $('.online-wrap');
        csetUsers.appendTo('body');
        csetUsers.attr('id', 'sgcset-lista');
        var winWidth = $(window).width();

        function resizeChatArea() {
            winWidth = $(window).width();
            var outerWidth = winWidth - 433;
            var innerWidth = outerWidth - 20;
            var chatInputWidth = innerWidth - 272;
            $('#header').css('width', outerWidth);
            $('#content').css('width', outerWidth);
            //$('#jsg-utolso-regisztraltak').css('width', innerWidth);
            $('#forum-chat').css('width', innerWidth);
            $('#forum-chat > .main-window').css('width', innerWidth);
            $('#forum-chat-list').css('width', innerWidth);
            $('#forum-chat > #inputs').css('width', innerWidth + 2);
            $('#sgTopicFrameBtn').css('width', innerWidth);
            $('#sgTopicFrame').css('width', innerWidth);
            $('#forum-chat-input').css('width', chatInputWidth + 49);
            $('.tt-dropdown-menu').css('width', chatInputWidth + 94);
            $('.jsg-csetname-old').css('max-width', '180px');

            $('#forum-wrap .forums-block').attr('style', 'width: 32% !important; margin: 20px .6% !important; float: left !important');
            $('#forum-wrap .forums-block header').attr('style', 'padding: 10px !important; background-color: rgba(29, 162, 200, .2)');
            $('#forum-wrap .forums-block .col1').attr('style', 'width: 45% !important; box-sizing: border-box');
            $('#forum-wrap .forums-block .col2').attr('style', 'width: 29% !important; box-sizing: border-box');
            $('#forum-wrap .forums-block .col3').attr('style', 'width: 22% !important; box-sizing: border-box');
            $('#forum-wrap .forums-block > header .col1').attr('style', 'width: 45% !important; box-sizing: border-box');
            $('#forum-wrap .forums-block > header .col2').attr('style', 'width: 34% !important; box-sizing: border-box');
            $('#forum-wrap .forums-block > header .col3').attr('style', 'width: 18% !important; box-sizing: border-box');

            blocksResponsive();
        }

        $(window).resize(resizeChatArea);

        resizeChatArea();
    }
    else {
        $(window).unbind('resize');
        $(window).resize(function () {
            return true;
        });
        isResponsive = false;
    }
}

/**
 * Resets responsive design.
 */
function resetResponsive() {
    $('.jsg-csetname-old').css('max-width', '100px');
    $('#forum-chat-input').css('width', 735);
    $('.tt-dropdown-menu').css('width', 750);
    $('#forum-chat > .main-window').css('width', 675);
    $('#forum-chat > #inputs').css('width', '100%');
    $('#forum-chat').css('width', '900px');
    $('#sgTopicFrameBtn').css('width', '900px');
    $('#sgTopicFrame').css('width', '900px');
    //$('#jsg-utolso-regisztraltak').css('width', '900px');
    $('#content').css('width', '');
    $('#header').css('width', '');

    $('#forum-wrap .forums-block').removeAttr('style');
    $('#forum-wrap .forums-block header').removeAttr('style');
    $('#forum-wrap .forums-block .col1').removeAttr('style');
    $('#forum-wrap .forums-block > header .col1').removeAttr('style');

    var csetUsers = $('.online-wrap');
    csetUsers.removeAttr('id');
    csetUsers.insertAfter('.main-window');

    $(window).unbind('resize');
    $(window).resize(function () {
        return true;
    });

    isResponsive = false;
}

/**
 * Removes rave function.
 */
function removeRave() {
    var elems = ['body', '#sgcset-header', '#sgcset-header a', '#neosidebar', '#breadcrumb', '#content', '.main-window', '.online-wrap', '#forum-chat-input', '#forum-chat-send-message'];
    for (var i = 0; i < elems.length; i++) {
        var elem = elems[i];
        $(elem).removeAttr('style');
    }
}

/**
 * !sgteam
 *
 * @returns {string}
 */
function sgteam() {
    var str = '';

    if (getSetting('smiley') == 1) {
        for (var i in smileys) {
            var mire = smileys[i]['mire'];
            var sgteam = smileys[i]['sgteam'];
            var ext = smileys[i]['ext'];
            if (!ext) {
                ext = 'png';
            }

            if (sgteam) {
                str += '<span class="szmajlivagyok sgteam" original="' + mire + '"><img src=\'' + baseUrl + 'images/' + mire + '.' + ext + '\' /></span>';
            }
        }
    }
    else {
        str = '!sgteam';
    }

    return str;
}

/**
 * Raves. Lol.
 */
function rave() {
    var elems = ['body', '#sgcset-header', '#sgcset-header a', '#neosidebar', '#breadcrumb', '#content', '.main-window', '.online-wrap', '#forum-chat-input', '#forum-chat-send-message'];
    for (var i = 0; i < elems.length; i++) {
        var elem = elems[i];
        $(elem).attr('style', 'background-color: #' + Math.floor(Math.random() * 16777215).toString(16) + ' !important;' + 'color: #' + Math.floor(Math.random() * 16777215).toString(16) + ' !important');
    }

    if (getSetting('responsive') == 1) {
        $(window).trigger('resize');
        setResponsive();
    }
}

/**
 * PWT Timer.
 */
function toSeconds(time_str) {
    var parts = time_str.split(':');
    return parts[0] * 3600 + /* an hour has 3600 seconds */ parts[1] * 60 + /* a minute has 60 seconds */ +parts[2] /* seconds */;
}

/**
 * !pwt function
 */
function pwt() {
    var now = new Date();
    var a = now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds(),
        b = "15:45:00",
        c = "8:45:00",
        d = "20:00:00";

    var difference = Math.abs(toSeconds(a) - toSeconds(b));

    var result = [
        Math.floor(difference / 3600),
        Math.floor((difference % 3600) / 60),
        difference % 60
    ];

    if (toSeconds(a) < toSeconds(c)) {
        result = "Korai még dolgozni...";
    }

    else if (toSeconds(a) > toSeconds(b)) {
        if (toSeconds(a) > toSeconds(d)) {
            result = "Normális ember ilyenkor nem dolgozik..."
        }
        else {
            result = "Ideje hazamenni..."
        }
    }

    else {
        result = result.map(function (v) {
            return v < 10 ? '0' + v : v;
        }).join(':');
    }

    //console.log(result);
    $('span.pwt').html(result);
    setTimeout('pwt()', 1000);
}

/**
 * Replace function for smileys.
 * @param text
 * @returns {string}
 */
function smileyReplace(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}

/**
 * Auto make clickable links.
 * @param text
 * @returns {string}
 */
function Linkify(text) {
    var replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;()]*[-A-Z0-9+&@#\/%=~_|()])/gim;
    var replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;

    var replacedText = '';
    var split = text.split(/(<img[^>]*>)/g);

    for (var i in split) {
        if (split[i].indexOf('<img') != 0) {
            split[i] = linkif(split[i]);
        }
        replacedText += split[i];
    }

    return replacedText;
}

/**
 * Text to speech function.
 * @param text
 */
function textToSpeech(text) {
	responsiveVoice.speak(text);
}

/**
 * Music playa.
 * @param song
 */
function player(song) {
    var audiotag = '<audio id="myAudio" class="' + song + '" autoplay="autoplay"></audio>',
        src = baseUrl + 'audio/' + song,
        source_mp3 = '<source src="' + src + '.mp3?rand=' + Math.random() + '" type="audio/mp3"></source />',
        source_ogg = '<source src="' + src + '.ogg?rand=' + Math.random() + '" type="audio/ogg"></source />';

    var myAudio = $('#myAudio');
    $('body').append(audiotag);
    var thisSong = $('#myAudio.' + song);
    thisSong.append(source_mp3);
    thisSong.append(source_ogg);

    thisSong[0].play();

    thisSong.on('ended', function () {
        $('a.' + song).removeClass('playing');
        resetplayer(song);
    });
}

/**
 * Stop song.
 * @param song
 */
function resetplayer(song) {
    $('audio').remove('.' + song);
    $('.player_btn.' + song).removeClass('playing');
}

/**
 * Audio player button.
 * @param song
 */
function player_btn(song) {
    if (!$('#myAudio.' + song).length) {
        player(song);
        $('.player_btn.' + song).addClass('playing');
    }
    else {
        resetplayer(song);
        //$('.player_btn.' + song).removeClass('playing');
    }
}

/**
 * Get topic name in chat links.
 */
function sgTopicLinks() {
    $('.msg').each(function () {
        var dat = $(this);
        if (dat.text().indexOf('sg.hu/forum/tema/') >= 0) {
            if (!dat.hasClass('sglinked')) {
                var topicLinks = dat.text().match(/sg\.hu\/forum\/tema\/(\d+)/g);

                for (var i in topicLinks) {
                    var topicLink = topicLinks[i];
                    var topicId = topicLink.replace('sg.hu/forum/tema/', '');
                    $.ajax({
                        url: '//sg.hu/forum/tema/' + topicId,
                        type: 'GET',
                        async: false
                    }).done(function (res) {
                        res = res.replace(/\n/g, "");
                        res = res.replace(/  /g, "");
                        var result = $(res).filter('title').text();
                        result = result.replace('SG.hu - Fórum - ', '');
                        dat.html(dat.html().replace('sg.hu/forum/tema/' + topicId + '</a>', 'sg.hu/forum/tema/' + topicId + ' (' + result + ')</a> '));
                    }).error(function (e) {
                        dat.html(dat.html().replace('sg.hu/forum/tema/' + topicId + '</a>', 'sg.hu/forum/tema/' + topicId + ' (TÖRÖLT TOPIC)</a> '));
                    });
                }
                dat.addClass('sglinked');
            }

            updateSgLinks();
        }
    });
}

/**
 * Creates iframe for links.
 */
function createSgIframe() {
    $('#forum-chat').after('<div id="sgTopicFrameBtn" class="open"><i></i></div><iframe id="sgTopicFrame"></iframe>');

    $('#sgTopicFrameBtn').click(function () {
        $('#sgTopicFrame').slideToggle(1000);
        $('#sgTopicFrameBtn').toggleClass('open');
    });

    bindNoScroll(document.getElementById('sgTopicFrame'), otherScrollSpeed);
}

/**
 * Creates div for thread modal.
 */
function threadsDiv() {
    var threadsDiv = '<div class="changelogs">';
    threadsDiv += '<h3 class="title">Threads</h3>';
    threadsDiv += '<ul class="normal">';

    for (var i in threadData) {
        var name = showSmileysInText(threadData[i]['name']);
        var info = threadData[i]['info'];
        var text = '<li><h3 class="changes"><span class="ver">' + name + '</span>' + '<span class="date">' + info + '</span></h3></li>';

        threadsDiv += text;
    }

    threadsDiv += '</ul></div>';

    return threadsDiv;
}

/**
 * Creates thread bar.
 */
function createThread() {
    var name = showSmileysInText(threadData[0]['name']);
    var info = threadData[0]['info'];

    if (!$('#currentThread').length) {
        $('<a id="threadModalLink"><div id="currentThread"><span id="threadName"></span><span id="threadInfo"></span></div></a>').prependTo('.main-window')
    }

    $('#threadName').html(name);
    $('#threadInfo').html(info);

    $('#threadModalLink').click(function () {
        createModal(threadsDiv, null);
    });

    linkifyThread();
}

function showSmileysInText(text) {
    if (getSetting('smiley') == 1) {
        for (var i in smileys) {
            var mit = smileys[i]['mit'];
            var mitUp = mit.toUpperCase();
            var mire = smileys[i]['mire'];
            var ext = smileys[i]['ext'];
            if (!ext) {
                ext = 'png';
            }

            var str = '<span class="szmajlivagyok" original="' + mire + '"><i class="smiley ' + mire + '" ></i></span>';
            if (smileys[i]['kep']) {
                str = '<span class="szmajlivagyok" original="' + mire + '"><img src=\'' + baseUrl + 'images/' + mire + '.' + ext + '\' /></span>';
            }

            if (text.indexOf(mit) >= 0) {
                text = text.replace(new RegExp(smileyReplace(mit), 'gi'), str);
            }
            if (text.indexOf(mitUp) >= 0) {
                text = text.replace(new RegExp(smileyReplace(mitUp), 'gi'), str);
            }
        }
    }

    return text;
}

/**
 * Show sg iFrame.
 *
 * @param href
 */
function showFrame(href) {
    var frameBtn = $('#sgTopicFrameBtn');
    var topicFrame = $("#sgTopicFrame");

    //topicFrame.attr("src", '');
    topicFrame.attr("src", href + "#forum-wrap");
    topicFrame.show();
    frameBtn.css('display', 'block');
    if (!frameBtn.hasClass('open')) {
        frameBtn.addClass('open');
    }
}

/**
 * Open sg links in iFrame.
 */
function sgLinksToIframe(clickables) {
    clickables.click(function (e) {
        e.preventDefault();
        showFrame($(this).attr('href'));
    });
    $('#favorites-list a').click(function () {
        updateSidebar();
    });
}

/**
 * Open sg links in self.
 *
 * @param clickables
 */
function sgLinksToSelf(clickables) {
    sgLinksReset(clickables);

    clickables.each(function () {
        $(this).attr('target', '_self');
    });
}

/**
 * Open sg links in blank background.
 *
 * @param clickables
 */
function sgLinksToBlank(clickables) {
    sgLinksReset(clickables);

    clickables.each(function () {
        $(this).attr('target', '_blank');
    });
}

/**
 * Resets sg links.
 */
function sgLinksReset(clickables) {
    $("#sgTopicFrame").hide();
    $('#sgTopicFrameBtn').hide();
    clickables.unbind('click');
    $('#favorites-list a').click(function () {
        updateSidebar();
    });
}

/**
 * Updates sg links realtime.
 */
function updateSgLinks() {
    var clickables = $('.sglinked > a, #favorites-list > span > a, .forum-topics-block li > a, #sidebar-user a:not(".notClickable"), .forums-block a, #birthday-wrap > a, #sidebar-sg-new-galleries > nav > a');

    if (getSetting('sgiframe') == 1) {
        sgLinksToIframe(clickables);
    }
    else if (getSetting('sgiframe') == 2) {
        sgLinksToBlank(clickables);
    }
    else {
        sgLinksToSelf(clickables, '_self');
    }
}

/**
 * Shows / hides chat.
 */
function chatToggle() {
    $("#forum-chat").slideToggle();
    $("#breadcrumb").slideToggle();
}

/**
 * Shows / hides fb box.
 */
function fbToggle() {
    $('#forum-fb-likebox').slideToggle();
}

/**
 * Shows / hides thread bar.
 */
function threadToggle(val) {
    if (val == 1) {
        createThread();
    }
    else {
        $('#currentThread').remove();
    }
}

/**
 * Sets / resets compact mode.
 */
function compactToggle(val) {
    var body = $('body');
    if (val == 1) {
        body.addClass('compact');
    }
    else {
        body.removeClass('compact');
    }
}

/**
 * Shows / hides Jedi Mester remembering.
 */
function jediToggle() {
    $('#jedimester').slideToggle();
}

/**
 * Starts bomberman.
 */
function startBomb() {
    window.open('http://sg.hu:8082/bomberman.html','_blank','width=992,height=588,location=0,menubar=0,resizable=0,scrollbars=0,status=0,titlebar=0,toolbar=0');
}

/**
 * Gets the birthday boyz and girlz.
 * @returns {string}
 */
function bszn() {
    var bsznArray = [];
    $('#birthday-wrap > a').each(function () {
        if (!inArray($(this).text(), bsznArray)) {
            bsznArray.push($(this).text());
        }
    });

    bsznArray.sort;
    var bszn = bsznArray.join(', ');

    return 'Boldog szülinapot: ' + bszn + '!';
}

/**
 * Updates sidebar boxes realtime.
 */
function updateBoxes() {
    var topics = $('#neosidebar > .forum-topics-block');
    var newTopics = $(topics.find('section')[0]);
    var coolTopics = $(topics.find('section')[1]);
    var birthdays = $('#neosidebar #sidebar-birthday');
    var articles = $('.articles-block');

    if (getSetting('sidebar_newtopics') == 0) {
        newTopics.hide();
    }
    else {
        newTopics.show();
    }

    if (getSetting('sidebar_cooltopics') == 0) {
        coolTopics.hide();
    }
    else {
        coolTopics.show();
    }

    if (getSetting('sidebar_birthdays') == 0) {
        birthdays.hide();
    }
    else {
        birthdays.show();
    }

    if (getSetting('articles') == 0) {
        articles.hide();
    }
    else {
        articles.show();
    }
}

/**
 * Update topic boxes.
 */
function updateTopics(update) {
    curVal = getSetting('topics');

    if (update) {
        if (curVal == 0) {
            curVal = 1;
        }
        else {
            curVal = 0;
        }
        setSetting('topics', curVal);
    }

    var topicBoxes = $('#forums-block-wrapper');
    var topicButton = $('#forum-block-wrapper-button');
    var txt = 'Témák listája ';

    if (curVal == 0) {
        topicBoxes.hide();
        topicButton.text(txt + '\u2193');
    }
    else {
        topicBoxes.show();
        topicButton.text(txt + '\u2191');
    }
}

/**
 * Toggle monster
 */
function toggleMonster(checkSetting) {
    var name = 'chat-monster',
        selector = $('#' + name),
        go = checkSetting ? getSetting('monster') == 1 : true,
        hanging = 'https://sg.hu/chat/sgchat/images/hanging.png',
        hangingLaser = 'https://sg.hu/chat/sgchat/images/hanging_laser.png',
        laserTime,
        boo,
        booPos = 0,
        monsterPos = 0;

    if (!selector.length && go) {
        $('<img id="chat-monster-crack" src="https://sg.hu/chat/sgchat/images/crackinwall.png" /><img id="' + name + '" src="https://sg.hu/chat/sgchat/images/hanging.png" />').prependTo('.main-window');

        selector = $('#' + name);
        selector.click(function () {
            selector.attr('src', hangingLaser);
            laserTime = 200;
            boo = $('.boo');
            if (!boo.length) {
                laserTime = 1000;
            }
            setTimeout(function () {
                selector.attr('src', hanging);
            }, laserTime);

            if (boo.length) {
                monsterPos = selector.css('transform').split(/[()]/)[1].split(',')[2];
                booPos = boo.css('transform').split(/[()]/)[1].split(',')[5];

                if (monsterPos > 0 && booPos < 0) {
                    $('#boo').hide('slow', function() {
                        $('#boo').show('slow');
                    });
                }
            }

        });
    }
    else {
        $('#chat-monster-crack').remove();
        selector.remove();
    }
}

/**
 * Toggle snow effect
 */
function toggleSnow(checkSetting) {
    var name = 'snow',
        selector = $('#' + name),
        go = checkSetting ? getSetting('snow') == 1 : true;

    if (!selector.length && go) {
        $('<div id="snow"></div>').appendTo('#header-bottom');
    }
    else {
        selector.remove();
    }
}

/**
 * Toggle candles
 */
function toggleCandles(checkSetting) {
    var name = 'candles',
        selector = $('#' + name),
        go = checkSetting ? getSetting('candles') == 1 : true;

    if (!selector.length && go) {
        $('<div id="candles"><div class="candle"><div class="flamegroup"><div class="flame" id="fl1"></div><div class="flame" id="fl2"></div><div class="flame" id="fl3"></div></div></div><div class="candle"><div class="flamegroup"><div class="flame" id="fl1"></div><div class="flame" id="fl2"></div><div class="flame" id="fl3"></div></div></div><div class="candle"><div class="flamegroup"><div class="flame" id="fl2"></div><div class="flame" id="fl1"></div><div class="flame" id="fl3"></div></div></div><div class="candle"><div class="flamegroup "><div class="flame" id="fl1"></div><div class="flame" id="fl2"></div><div class="flame" id="fl3"></div></div></div></div>').appendTo('.main-window');
    }
    else {
        selector.remove();
    }
}

/**
 * Toggle haloween monster
 */
function toggleHaloween(checkSetting) {
    var name = 'boo',
        selector = $('#' + name),
        go = checkSetting ? getSetting('haloween') == 1 : true;

    if (!selector.length && go) {
        $('<div id="boo"><div class="boo float"><span class="sg-text">sg.hu</span><div class="left-eye"></div><div class="right-eye"></div><div class="mouth"><div class="tooth onesc"></div><div class="tooth twosc"></div><div class="tooth threesc"></div><div class="tooth foursc"></div><div class="boo-tongue"></div></div></div><div class="right-arm float"></div><div class="tail float"></div></div>').appendTo('body');
    }
    else {
        selector.remove();
    }
}

/**
 * haloween cat
 */
function toggleCat(checkSetting) {
    var name = 'macska',
        selector = $('#' + name),
        go = checkSetting ? getSetting('macska') == 1 : true;

    if (!selector.length && go) {
        $('<div id="' + name + '"><div class="macska-foreground"></div><div class="macska-midground"><div class="macska-tuna"></div></div><div class="macska-background"></div></div>').appendTo('body');
    }
    else {
        selector.remove();
    }
}

/**
 * haloween cat in chat window
 */
function appendChatCat() {
    var name = 'macska-cset';
    if (!$('#' + name).length) {
        $('<div id="' + name + '"><div class="macska-foreground"></div><div class="macska-midground"><div class="macska-tuna"></div></div><div class="macska-background"></div></div>').appendTo('.main-window');
    }
    else {
        $('#' + name).remove();
    }
}

/**
 * Update gallery box.
 */
function updateGallery() {
    var gallery = $('#neosidebar > #sidebar-sg-new-galleries');

    if (getSetting('gallery') == 0) {
        gallery.hide();
    }
    else {
        gallery.show();
    }
}

/**
 * Embeding soundcloud.
 */
function soundcloud() {
    $('.msg a[href*="soundcloud.com"]').each(function () {
        var $link = $(this);
        $.getJSON('//soundcloud.com/oembed?format=js&url=' + $link.attr('href') + '&iframe=true&callback=?', function (response) {
            $link.replaceWith(response.html);
        });
    });
}

/**
 * Embeding youtube.
 */
function youtube() {
    $('.msg a[href*="youtube.com"]').each(function () {
        var ytid = 'ytplayer_' + Math.random();

        var videoid = $(this).html().split('v=')[1];
        if (videoid) {
            var ampersandPosition = videoid.indexOf('&');
            
            if (ampersandPosition != -1) {
                videoid = videoid.substring(0, ampersandPosition);
            }

            var timestamp = $(this).html().split('&t=')[1];
            var timestamptxt = '';
            
            if (timestamp) {
                timestamp = timestamp.replace('s', '');
                timestamptxt = '&start=' + timestamp;
            }

            var ytsrc = '//www.youtube.com/embed/' + videoid + '?autoplay=0&origin=origin=https://sg.hu';

            /*
            var youtube = '<div id="' + ytid + '" ytid="' + videoid + '"></div>' +
                '<script>' +
                '  var tag = document.createElement(\'script\');' +
                '  tag.src = "https://www.youtube.com/player_api";' +
                '  var firstScriptTag = document.getElementsByTagName(\'script\')[0];' +
                '  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);' +
                '  var player;' +
                '  function onYouTubePlayerAPIReady() {' +
                '    player = new YT.Player(\'' + ytid + '\', {' +
                '      height: \'200\',' +
                '      width: \'320\',' +
                '      videoId: \'' + videoid + '\'' +
                '    });\n' +
                '  }\n' +
                '</script>';
                */

            var youtube = '<iframe ytid="' + videoid + '" type="text/html" class="ytiframe" id="' + ytid + '" type="text/html" width="'+ videow + '" height="' + videoh + '" src="' + ytsrc + timestamptxt + '" frameborder="0" allowfullscreen />';
            $(this).replaceWith(youtube);
        }
    });

    $('.msg a[href*="youtu.be"]').each(function () {
        var ytid = 'ytplayer_' + Math.random();

        var videoid = $(this).html().split('.be/')[1];

        if (videoid) {
            var ampersandPosition = videoid.indexOf('&');
            
            if (ampersandPosition != -1) {
                videoid = videoid.substring(0, ampersandPosition);
            }

            var timestamp = $(this).html().split('?t=')[1];
            var timestamptxt = "";
            
            if (timestamp) {
                timestamp = timestamp.replace('s', '');
                timestamptxt = "&start=" + timestamp;
            }

            var ytsrc = '//www.youtube.com/embed/' + videoid + '?autoplay=0&origin=https://sg.hu';

            /*
            var youtube = '<div id="' + ytid + '" ytid="' + videoid + '"></div>' +
                '<script>' +
                '  var tag = document.createElement(\'script\');' +
                '  tag.src = "https://www.youtube.com/player_api";' +
                '  var firstScriptTag = document.getElementsByTagName(\'script\')[0];' +
                '  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);' +
                '  var player;' +
                '  function onYouTubePlayerAPIReady() {' +
                '    player = new YT.Player(\'' + ytid + '\', {' +
                '      height: \'200\',' +
                '      width: \'320\',' +
                '      videoId: \'' + videoid + '\'' +
                '    });\n' +
                '  }\n' +
                '</script>';
                */

            var youtube = '<iframe ytid="' + videoid + '" type="text/html" class="ytiframe" id="' + ytid + '" type="text/html" width="'+ videow + '" height="' + videoh + '" src="' + ytsrc + timestamptxt + '" frameborder="0" allowfullscreen />';
            $(this).replaceWith(youtube);
        }
    });
}


/**
 * Embedding webm, mp4, ogg.
 */
function videos() {
    for (type in videoTypes) {
        var ext = type;
        var typ = videoTypes[type];

        $('.msg a[href*=".' + ext + '"]').each(function () {
            var href = $(this).attr('href');
            var rand = Math.random();
            var videoPlayer = '<video id="webm_video_' + rand + '" class="video-js vjs-default-skin webm_video webm_video_' + typ + '" ' +
                'controls playsinline preload="none" ' +
                'data-setup=\'{ "aspectRatio":"' + videow + ':' + videoh + '", "playbackRates": [1, 1.5, 2] }\'' +
                'muted width="'+ videow + '" height="' + videoh + '" ' +
                'poster="//sg.hu/assets/img/video/' + typ + '.jpg">' +
                '<source src="' + href + '" type="video/' + typ + '" />' +
                '</video>';

            $(this).replaceWith(videoPlayer);
        });
    }

    $('.msg a[href*=".gifv"]').each(function () {
        var href = $(this).attr('href');
        href = href.replace('http:', '');
        href = href.replace('https:', '');
        href = href.replace('.gifv', '.mp4');

        if (href.indexOf('imgur') > -1) {
            var rand = Math.random();
            var videoPlayer = '<video id="webm_video_' + rand + '" class="video-js vjs-default-skin webm_video webm_video_webm" ' +
                'controls playsinline preload="none" ' +
                'data-setup=\'{ "aspectRatio":"640:267", "playbackRates": [1, 1.5, 2] }\'' +
                'muted width="'+ videow + '" height="' + videoh + '" ' +
                'poster="//sg.hu/assets/img/video/gifv.jpg">' +
                '<source src="' + href + '" type="video/mp4" />' +
                '</video>';

            $(this).replaceWith(videoPlayer);
        }
    });
}

/**
 * Embedding vimeo.
 */
function vimeo() {
    $('.msg a[href*="vimeo.com"]').each(function () {
        var vmid = 'vmplayer_' + Math.random();

        var videoid = $(this).html().split('/')[3];
        var vimeo = '<iframe vmid="' + videoid + '" class="vmiframe" id="' + vmid + '" src="//player.vimeo.com/video/' + videoid + '?badge=0&autoplay=0" width="'+ videow + '" height="' + videoh + '" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" />';
        $(this).replaceWith(vimeo);
    });
}

/**
 * Embedding vimeo.
 */
function coub() {
    $('.msg a[href*="coub.com"]').each(function () {
        var videoid = $(this).html().split('/')[4];

        var coub = '<iframe vmid="' + videoid +'" class="coub" src="//coub.com/embed/' + videoid + '?muted=false&autostart=false&originalSize=false&startWithHD=false" allowfullscreen frameborder="0" width="100%" height="100%" allow="autoplay" />';
        $(this).replaceWith($(coub));
    });
}
// <iframe vmid="uhq0f" class="coub" src="//coub.com/embed/uhq0f?muted=false&amp;autostart=false&amp;originalSize=false&amp;startWithHD=false" frameborder="0" width="346" height="195" allow="autoplay"></iframe>
// <iframe src="//coub.com/embed/uhq0f?muted=false&amp;autostart=false&amp;originalSize=false&amp;startWithHD=false" frameborder="0" width="346" height="195" allow="autoplay"></iframe>
// <iframe src="//coub.com/embed/uhq0f?muted=false&autostart=false&originalSize=false&startWithHD=false" allowfullscreen frameborder="0" width="346" height="195" allow="autoplay"></iframe>

/**
 * Embeding facebook.
 */
function facebook() {
    $('.msg a[href^="https://www.facebook.com"][href*="videos"]').each(function () {
        var href = $(this).attr('href');

        var fb = '<div class="fb-video" data-href="' + href + '" data-allowfullscreen="true" data-width="400"></div>';
        $(this).replaceWith(fb);
    });
}

/**
 * Remove embeding soundcloud.
 */
function removeSoundcloud() {
    $('.msg iframe[src*="soundcloud"]').each(function () {
        var href = $(this).attr('src');
        href = href.replace('https://w.soundcloud.com/player/?visual=true&url=', '');
        href = href.split('&')[0];
        href = decodeURIComponent(href);

        var parentMsg = $(this).parent();

        $.getJSON(href + '.json?client_id=32ea61c5b631c7a58018223c7aae5534', function (response) {
            parentMsg.html(Linkify(response.permalink_url));
        });
    });
}

/**
 * Remove embeding youtube.
 */
function removeYoutube() {
    $('.msg iframe.ytiframe').each(function () {
        var ytid = $(this).attr('ytid');
        var newLink = 'https://www.youtube.com/watch?v=' + ytid;
        $(this).parent().html(Linkify(newLink));
    });
}

/**
 * Remove embeding webm, mp4, ogg.
 */
function removeVideos() {
    $('.msg video.webm_video').each(function () {
        var href = $(this).attr('src');
        $(this).parent().html(Linkify(href));
    });
}

/**
 * Remove embeding vimeo.
 */
function removeVimeo() {
    $('.msg iframe.vmiframe').each(function () {
        var vmid = $(this).attr('vmid');
        var newLink = 'https://vimeo.com/' + vmid;
        $(this).parent().html(Linkify(newLink));
    });
}

/**
 * Remove embeding coub.
 */
function removeCoub() {
    $('.msg iframe.coub').each(function () {
        var vmid = $(this).attr('vmid');
        var newLink = 'https://coub.com/view/' + vmid;
        $(this).parent().html(Linkify(newLink));
    });
}

/**
 * Scroll to chat window bottom.
 */
function scrollToBottom() {
    var listFrame = document.getElementById('list-frame');
    listFrame.scrollTop = listFrame.scrollHeight;
}

function addEvent(el, type, handler) {
    if (el.attachEvent) el.attachEvent('on'+type, handler); else el.addEventListener(type, handler);
}
function removeEvent(el, type, handler) {
    if (el.detachEvent) el.detachEvent('on' + type, handler); else el.removeEventListener(type, handler);
}

function bindNoScroll(element, speed) {
    removeEvent(element, 'mousewheel DOMMouseScroll');
    addEvent(element, 'mousewheel DOMMouseScroll', function (e) {
        var scrollTo = null;

        if (e.type == 'mousewheel') {
            scrollTo = (e.originalEvent.wheelDelta * -1);
        }
        else if (e.type == 'DOMMouseScroll') {
            scrollTo = speed * e.originalEvent.detail;
        }

        if (scrollTo) {
            e.preventDefault();
            this.scrollTop = this.scrollTop + scrollTo;
        }
    });
}

/**
 * !flip
 *
 * @returns {string}
 */
function flipcoin() {
    var str = '';
    var items = ['fej', 'írás'];

    function rand(min, max) {
        var range = (max - min) + 1;
        return Math.floor(Math.random() * range) + min;
    }

    randomNumber = rand(0, items.length - 1);

    randomItem = items[randomNumber];

    str = '<span>' + randomItem + '</span>';
    return str;
}

/**
 * Chat user name links.
 *
 * @param name
 */
function userClicked(name) {
    // try to find it in the user list
    $("#forum-chat").find(".online-wrap li a:nth-child(2)").each(function () {
        if ($(this).html() == name) {
            window.open($(this).attr("href"));
        }
    });
    // couldn't find
    window.open("//sg.hu/kereses?s=" + name + "&u=1&sh=");
}

/**
 * Show clock for the bitches
 */
function showTime() {
    var date = new Date();
    var h = date.getHours(); // 0 - 23
    var m = date.getMinutes(); // 0 - 59
    var s = date.getSeconds(); // 0 - 59

    if (h === 0){
        h = 12;
    }

    h = (h < 10) ? '0' + h : h;
    m = (m < 10) ? '0' + m : m;
    s = (s < 10) ? '0' + s : s;

    var time = h + ":" + m + ":" + s;
    document.getElementById('clock').innerText = time;
    document.getElementById('clock').textContent = time;

    setTimeout(showTime, 1000);
}

/**
 * Live url preview stuff
 */
function updatePrews() {
    /*
    $("#forum-chat").find("#forum-chat-list a").each(function () {
        if ($(this).attr("preved") != "true") {
            $(this).miniPreview({ prefetch: 'none' });
            $(this).attr("preved", "true");
        }
    });
    */

    $("#forum-chat").find("#forum-chat-list a").each(function () {
        var href = $(this).attr("href");

        if ($(this).attr("preved") != "true") {
            $(this).qtip({
                hide: {
                    effect: false,
                    delay: 0
                },
                show: {
                    effect: false,
                    delay: 0
                },
                content: {
                    text: "<div style=\"text-align: center;\"><img src=\"" + baseUrl + "images/pekmen.gif\"><br>Pekmen...</div>"
                },
                position: {
                    effect: false,
                    delay: 0,
                    my: 'middle left',
                    at: 'middle right'
                },
                events: {
                    render: function () {
                        var ext = (href.split('.').pop()).toLowerCase();
                        var parent;
                        if (ext == "jpg" || ext == "gif" || ext == "png" || ext == "jpeg" || ext == "webp") {
                            $(this).html("<img class='preview_img' src='" + href + "'>");
                            $(this).qtip('reposition');
                        }
                        else if (ext == "webm" || ext == "mp4") {
                            $(this).html('<video width="250" height="250" controls="false" autoplay="autoplay" loop="true" muted="true"><source src="' + href + '" type="video/' + ext + '"></video>');
                            $(this).qtip('reposition');
                        }
                        else {
                            $(this).html("");
                            $(this).qtip('hide');
                        }
                        /*else {
                            parent = this;
                            // yahú apiból kiolvassuk a titlet!
                            $.get("https://query.yahooapis.com/v1/public/yql",
                                {q: 'select * from html where url="' + href + '" and xpath="*"', format: 'json'},
                                function (result) {
                                    if (result.query.results != null) {
                                        var title = result.query.results.html.head.title;
                                        var desc = "";
                                        var img = "";

                                        // iter?ljunk v?gig a meta resultokon
                                        for (var meta in result.query.results.html.head.meta) {
                                            meta = result.query.results.html.head.meta[meta];
                                            if (meta.content) {
                                                var ext = (meta.content.split('.').pop()).toLowerCase();

                                                if (meta.name == "description")
                                                    desc = meta.content;
                                                else if (meta.name == "title")
                                                    title = meta.content;
                                                else if (ext == "jpg" || ext == "gif" || ext == "png" || ext == "jpeg")
                                                    img = meta.content;
                                            }
                                        }

                                        if (title != null) {
                                            $(parent).html("");
                                            if (img != "")
                                                $(parent).append("<img class='chat_prev_img' src='" + img + "'>");
                                            if (title != "")
                                                $(parent).append("<h3 class='chat_prev_title'>" + title + "</h3>");
                                            if (desc != "")
                                                $(parent).append("<h2 class='chat_prev_desc'>" + desc + "</h2>");
                                        }
                                        else
                                            $(parent).html("<img class='preview_img' src='" + href + "'>");

                                    } else {
                                        $(parent).html("<img class='preview_img' src='" + href + "'>");
                                    }

                                    $(parent).qtip('reposition');
                                },
                                'JSON');
                        }*/
                    }
                }
            });
        }
        $(this).attr("preved", "true");
    });
}

/**
 * Check if the user is still focused on the chat.
 */
function checkOnlineStatus() {
    if (socket) {
        //socket.emit('onlineStatus', focused);
        //console.debug('enkuldom onsocket', currentUser + ' - ' + focused);
        socket.emit('request', {'type': 'onlineStatus', 'data': {'user': currentUser, 'status': focused, 'real': true}});

        var afk = 'noafk';
        if (getSetting('afk') == 1) {
            afk = 'afk';
        }

        socket.emit('request', {'type': 'onlineStatus', 'data': {'user': currentUser, 'status': afk, 'real': true}});
    }
}

/**
 * Plays the KataNyuszi.
 */
function kataNyuszi() {
    if (getSetting('autoplay') == 1 && focused) {
        player('happytree');
    }
    $('<div class="rabbit"></div><div class="clouds"></div>').appendTo('#forum-wrap');
    setTimeout(function () {
        $('.rabbit').remove();
        $('.clouds').remove();
    }, 6000);
}

/**
 * Updates a user's status in the chat list.
 *
 * @param username
 * @param status
 */
function updateUser(username, status) {
    var userDiv = $('#forum-chat-user-list > li > a.' + fixUserName(username));
    var usernameDiv = $("#forum-chat-user-list a:contains('" + username + "')");
    var usernameDivv = $(".sender:contains('" + username + "')");

    //console.log('Username: ' + username + ', status: ' + status);

    if (status === 'mobileapp') {
        userDiv.removeClass('chatStatusOffline');
        userDiv.removeClass('chatStatusOnline');
        userDiv.addClass('chatStatusMobileApp');
        userDiv.attr('title', 'MobileApp');
    }
    else if (status === 'afk') {
        usernameDiv.each(function(i, v) {
            $(v).contents().eq(2).wrap('<span class="afkk afk-' + username + '"/>');
        });
        usernameDivv.addClass('afkk afk-' + username);
    }
    else if (status === 'noafk') {
        $('.afk-' + username).removeClass('afkk').removeClass('.afk-' + username);
    }
    else if (status === true) {
        userDiv.removeClass('chatStatusOffline');
        userDiv.removeClass('chatStatusMobileApp');
        userDiv.addClass('chatStatusOnline');
        userDiv.attr('title', 'Online');
    }
    else {
        userDiv.removeClass('chatStatusOnline');
        userDiv.removeClass('chatStatusMobileApp');
        userDiv.addClass('chatStatusOffline');
        userDiv.attr('title', 'Offline');
    }

    if(getSetting('custom_css') == 'material_empty') {
        chatnameColorReplace();    
    }
    
}

/**
 * Checks if various jQuery plugins are loaded.
 */
function checkJquery() {
    if (jQuery().spectrum) {
        addSpectrum();
    }
    else {
        includeJs('//cdnjs.cloudflare.com/ajax/libs/spectrum/1.3.0/js/spectrum.min.js');
    }
}

/**
 * Updates jScrollPane
 */
function updateJscroll() {
    var chatWindow = $($('.main-window')[0]);
    var neoside = $('#neosidebar');

    chatWindow.jScrollPane({
        mouseWheelSpeed: 50,
        arrowButtonSpeed: 50
    });

    neoside.jScrollPane({
        mouseWheelSpeed: 50,
        arrowButtonSpeed: 50
    });
    var neoApi = neoside.data('jsp');
    if (neoApi) {
        neoApi.destroy();
    }
    $('#neosidebar').jScrollPane({
        mouseWheelSpeed: 50,
        arrowButtonSpeed: 50
    });

    scrollToBottom();
}

/**
 * Adds colorpicker to the chat color stuff in options.
 */
function addSpectrum() {
    var oc = $("#options_color");
    if (!oc.hasClass('spectrumed')) {
        var color = null;
        if (settings[currentUser]) {
            color = settings[currentUser];
        }

        oc.spectrum({
            color: color,
            flat: false,
            showInput: true,
            showInitial: true,
            allowEmpty: true,
            showAlpha: false,
            disabled: false,
            showPalette: true,
            showPaletteOnly: false,
            togglePaletteOnly: false,
            showSelectionPalette: true,
            cancelText: 'Mégsem',
            chooseText: 'Kiválaszt',
            preferredFormat: "hex"
        });
        oc.on("dragstop.spectrum", function (e, color) {
            oc.attr('col', color.toHexString());
            fast_name_color();
        });
        oc.addClass('spectrumed');
    }

    var olc = $("#options_linescolor");
    if (!olc.hasClass('spectrumed')) {
        var lcolor = getSetting('lines_color');

        olc.spectrum({
            color: lcolor,
            flat: false,
            showInput: true,
            showInitial: true,
            allowEmpty: true,
            showAlpha: false,
            disabled: false,
            showPalette: true,
            showPaletteOnly: false,
            togglePaletteOnly: false,
            showSelectionPalette: true,
            cancelText: 'Mégsem',
            chooseText: 'Kiválaszt',
            preferredFormat: "hex"
        });
        olc.on("dragstop.spectrum", function (e, lcolor) {
            olc.attr('col', lcolor.toHexString());
            lines_color(true);
        });
        olc.addClass('spectrumed');
    }
}

/**
 * Gets current user's colors.
 *
 * @returns {Array}
 */
function getUserColors() {
    var color = null;
    var bgcolor = null;
    for (var key in settings) {
        if (currentUser == settings[key][0]) {
            color = settings[key][1];
            if (color.indexOf('#') !== 0) {
                color = '#' + color;
            }

            if (settings[key][2] != "false") {
                bgcolor = settings[key][2];
                if (bgcolor) {
                    if (bgcolor.indexOf('#') !== 0) {
                        bgcolor = '#' + bgcolor;
                    }
                }
            }
        }
    }

    return [color, bgcolor];
}

/**
 * Realtime lines recolor.
 */
function lines_color(fromSettings) {
    var c = getSetting('lines_color');
    if (fromSettings) {
        c = $("#options_linescolor").attr('col');
    }

    if (getSetting('custom_css') == 'lines') {
        var fcs = $('#forum-chat');
        var sgl = $('#sgcset-lista').find('header');
        var suf = $('#sidebar-user-favorites');
        var sch = $('#sgcset-header');
        var fbwb = $('#forum-block-wrapper-button');
        var bbt = $('.blue-border-top');
        var con = $('#content');
        var fw = $('#forum-wrap');
        var inp = $('#inputs #input_cont #forum-chat-send-message');
        var eag = $('.easteregg');
        var gif = $('.gif_tag');
        var bbcs = $('.bbcs');
        var gifs = $('#gif_search');
        var obn = $('.optionsButton');
        var fav = $('#sidebar-user-favorites a.category');

        // color
        var sColor = [
            $('h2.sgneojonny'),
            $('span.row'),
            $('.options_chat_label'),
            $('h3.title'),
            $('.modal-close'),
            $('.player_btn'),
            $('h4.alt_title'),
            $('div.player_cont'),
            $('span.title'),
            fbwb,
            inp,
            eag,
            gif,
            bbcs,
            gifs,
            obn,
            fav,
            $('#forum-wrap .forums-block > header .col1'),
            fcs.find('.main-window a'),
            sch.find('a.lefthead'),
            sch.find('a.righthead'),
            $('.extras_label'),
            $('.extras_desc')
        ];
        for (var i in sColor) {
            sColor[i].each(function () {
                this.style.setProperty('color', c, '');
            });
        }

        // border 1px solid
        var sBorder1 = [
            fbwb,
            eag,
            gif,
            bbcs,
            gifs,
            obn
        ];
        for (var i in sBorder1) {
            sBorder1[i].each(function () {
                this.style.setProperty('border', '1px solid ' + c, '');
            });
        }

        // border 1px solid important
        var sBorder1Important = [
            //$('#inputs'),
            $('#input_cont input'),
            $('#smileyButton'),
            $('#songButton'),
            $('#gifButton'),
            $('#bbcodesButton'),
            $('#spoilerButton'),
            $('.spoilerText'),
            $('.spoilerContentBlock'),
            $('.player_cont'),
            $('#birthday-wrap a'),
            inp
        ];
        for (var i in sBorder1Important) {
            sBorder1Important[i].each(function () {
                this.style.setProperty('border', '1px solid ' + c, 'important');
            });
        }

        // border-bottom 1px solid important
        var sBorderBottom1Important = [
            sgl,
            fcs.find('.main-window'),
            fcs.find('#forum-chat-input'),
            fcs.find('#forum-chat-send-message'),
            $('#currentThread'),
            $('header'),
            suf.find('#favorites-open-close-button #icon'),
            $('.headers-medium, h3.title'),
            $('.headers-medium'),
            $('#sidebar-user-favorites #favorites-open-close-button #icon')
        ];
        for (var i in sBorderBottom1Important) {
            sBorderBottom1Important[i].each(function () {
                this.style.setProperty('border-bottom', '1px solid ' + c, 'important');
            });
        }

        // border-bottom 0 important
        var sBorderBottom0Important = [
            bbt,
            fw.find('.blue-border-top')
        ];
        for (var i in sBorderBottom0Important) {
            sBorderBottom0Important[i].each(function () {
                this.style.setProperty('border-bottom', '0', 'important');
            });
        }

        // border-bottom 1px dashed important
        var sBorderBottom1Dashed = [
            suf.find('a.category'),
            fav
        ];
        for (var i in sBorderBottom1Dashed) {
            sBorderBottom1Dashed[i].each(function () {
                this.style.setProperty('border-bottom', '1px dashed ' + c, 'important');
            });
        }

        // border-top 1px solid
        var sBorderTop1 = [
            con
        ];
        for (var i in sBorderTop1) {
            sBorderTop1[i].each(function () {
                this.style.setProperty('border-top', '1px solid ' + c, '');
            });
        }

        // border-top 2px solid
        var sBorderTop2 = [
            sgl,
            bbt
        ];
        for (var i in sBorderTop2) {
            sBorderTop2[i].each(function () {
                this.style.setProperty('border-top', '2px solid ' + c, '');
            });
        }

        // border-top 1px solid important
        var sBorderTop1Important = [
            fcs.find('.main-window'),
            fw.find('.blue-border-top')
        ];
        for (var i in sBorderTop1Important) {
            sBorderTop1Important[i].each(function () {
                this.style.setProperty('border-top', '1px solid ' + c, 'important');
            });
        }

        // background-color
        var sBgcolor = [
            $('.separator')
        ];
        for (var i in sBgcolor) {
            sBgcolor[i].each(function () {
                this.style.setProperty('background-color', c, '');
            });
        }

        if (c) {
            var rgb = hexToRgb(c);
            var al = 'rgba(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ', ';
            var track = al + '.2)';
            var thumb = al + '.5)';
            var thumbHover = al + '1)';

            /*
            for (var i = 0; i < document.styleSheets.length; i++) {
                var sheet = document.styleSheets[i];

                if (sheet.href) {
                    if (sheet.href.indexOf('linesScrollbar.css') > -1) {
                        if (sheet.cssRules) {
                            if (sheet.cssRules.length === 3) {
                                sheet.deleteRule(2);
                                sheet.deleteRule(1);
                                sheet.deleteRule(0);
                            }
                        }
                        sheet.insertRule("::-webkit-scrollbar-track {background-color: " + track + " !important;}", 0);
                        sheet.insertRule("::-webkit-scrollbar-thumb {background-color: " + thumb + " !important;}", 1);
                        sheet.insertRule("::-webkit-scrollbar-thumb:hover {background-color: " + thumbHover + " !important;}", 2);
                    }
                }
            }
            */
        }
    }
}


/**
 * Realtime name color.
 */
function fast_name_color() {
    //settings.push([currentUser, $("#options_color").attr('col'), $("#options_bgcolor").attr('col')]);
    settings[currentUser] = $("#options_color").attr('col');
    recolorAllNames();
}

/**
 * Recolor names in chat and in user list.
 */
function recolorAllNames() {
    $(".sender").each(function () {
        colorName($(this));
    });

    $('.chatname').each(function () {
        colorListName($(this).attr('nick'), $(this));
    });

    /*
     $('.age').each(function () {
     readAge($(this).attr('alt'), $(this).get(0));
     });
     */
}

/**
 * Get color for user.
 *
 * @param elem
 */
function getColorByName(elem) {
    elem = elem.html ? elem.html() : elem;
    var name = elem.replace(': ', '');
    name = name.replace('* ', '');

    if (settings[name]) {
        return settings[name];
    }

    return false;
}

/**
 * Color one nick with its previously set color.
 *
 * @param elem
 */
function colorName(elem) {
    var color = getColorByName(elem);

    if (color && elem.html().indexOf('pxxl') === -1) {
        elem.css("color", color);
    }
}

/**
 * Chat nick list recolor.
 *
 * @param name
 * @param elem
 */
function colorListName(name, elem) {
    if (settings[name]) {
        elem.css("color", settings[name]);
    }
}

/**
 * Reads a user's age and gender.
 *
 * @param userid
 * @param element
 */
function readAge(userid, element) {
    if (initialized) {
        if (age[userid] !== undefined) {
            element.innerHTML = formatAge(age[userid][0], age[userid][1]);
            //element.className = element.className + " gender_" + age[userid][1];
        }
        else {
            element.innerHTML = "[?/?]";
            $.get(readUserUrl + "?userid=" + userid, function (response) {
                age[userid] = new Array(response[0], response[1]);
                element.innerHTML = formatAge(age[userid][0], age[userid][1]);
            }, 'JSON');
        }
    }
}

/**
 * Formats a user's age and gender.
 *
 * @param age
 * @param gender
 * @returns {string}
 */
function formatAge(age, gender) {
    if (age > 0 && gender != "") {
        return "[" + age + "/<span class='gender_" + gender + "'>" + gender.toUpperCase() + "</span>]";
    }
    else if (age == 0 && gender != "") {
        return "[" + "<span class='gender_" + gender + "'>" + gender.toUpperCase() + "</span>]";
    }
    else if (age > 0 && gender == "") {
        return "[" + age + "]";
    }
    else {
        return "";
    }
}

/**
 * Update sidebar with favourites and private messages.
 */
function updateSidebar() {
    $.get("//sg.hu/forum/", function (response) {
        $('#sidebar-forum').hide();
        sidebar = $(response).find('#sidebar-forum').html();
        var neosidebar = $('#neosidebar');
        neosidebar.html(sidebar);
        var icon = neosidebar.find('#favorites-open-close-button #icon');
        neosidebar.find('.user-hello').after('<a href="/"><span>Főoldal</span></a>');

        var sidebarupdate = getSetting('sidebarupdate', 0);
        var newsidebarupdate = sidebarupdate + 1;
        if (newsidebarupdate == 3) {
            newsidebarupdate = 0;
        }
        setSetting('sidebarupdate', newsidebarupdate);

        if (newsidebarupdate == 0) {
            $.get("//sg.hu/uzenetek", function (resp) {
                var privik = $(resp).find('span.new').length;
                // <span>Üzeneteim <span class="hilight opacity-pulse">(1)</span></span>
                var uzeneteim = '<span>Üzeneteim</span>';
                if (privik > 0) {
                    uzeneteim = '<span>Üzeneteim <span class="hilight opacity-pulse">(' + privik + ')</span></span>';
                }
                neosidebar.find('#sidebar-user').find('a[href$="uzenetek"]').html(uzeneteim);
            });
        }

        if (getSetting('favs') == 1) {
            icon.html('+');

            neosidebar.find('#favorites-list .fav-not-new-msg').css({'display': 'none'});
        }

        neosidebar.find('#sidebar-user a').first().addClass('notClickable');
        neosidebar.find('#sidebar-user a').last().addClass('notClickable');

        neosidebar.find('#icon').click(function () {
            var sign = $(this).text();
            if (sign == '+') {
                setSetting('favs', 0);
            }
            if (sign == '-') {
                setSetting('favs', 1);
            }
        });

        $('.forum-topics-block').remove();
        $(response).find('.forum-topics-block').insertAfter('#neosidebar > #sidebar-user-favorites');

        var topics = $('#neosidebar > .forum-topics-block');
        topics.find('header').addClass('headers-medium blue-border-bottom');
        topics.find('section').removeClass('pull-left pull-right');
        //topics.find('ul').removeClass('blue-plus-icon');

        // new fav topics
        var new_topics2 = 0;
        neosidebar.find(".new").each(function () {
            new_topics2++;
        });

        if (new_topics2 != new_topics) {
            new_topics = new_topics2;
            updateTitle();
        }

        updateSgLinks();
        updateBoxes();
        updateTopics();
        updateGallery();
        toggleSections();
        lines_color();
        //bindNoScroll($('#neosidebar'), neoScrollSpeed);
        //bindNoScroll($('#sgcset-lista .frame'), neoScrollSpeed);
    });
}

/**
 * Update window title with unread chat messages and new favourite topics
 */
function updateTitle() {
    var new_topics_text;
    if (new_topics > 0) {
        new_topics_text = "[" + new_topics + "] ";
    }
    else {
        new_topics_text = "";
    }

    var lik = $('#forum-chat-list > li');
    if (focused) {
        document.title = new_topics_text + sgTitle;
        setTimeout(function () {
            lik.removeClass('unread', 1000);
        }, 2000);
    }
    else {
        if (msg_counter > 0) {
            document.title = "(" + msg_counter + ") " + new_topics_text + sgTitle;
            lik.removeClass('unread');
            var e = lik.slice(-(msg_counter));
            e.addClass('unread');
        }
        else {
            document.title = new_topics_text + sgTitle;
            lik.removeClass('unread');
        }
    }
}

/**
 * Inits data saved in the outside server. Ages, genders, and colors.
 * Also inits colorpicker.
 */
function initServerData() {
    var color = '';
    if (settings[currentUser]) {
        color = settings[currentUser];
        var oc = $('#options_color');
        oc.val(color);
        oc.attr('col', color);
    }

    checkJquery();
    //recolorAllNames();
    getThreads();
}

/**
 * Get threads from server.
 */
function getThreads() {
    $.ajax({
        url: readThreadUrl,
        dataType: 'json',
        type: 'GET',
        async: false,
        success: function (response) {
            threadData = response;
            createThread();
        }
    });
}

/**
 * Update sidebar and responsive design every 10 seconds.
 */
function updateStuff() {
    updateSidebar();
    if (!$('#options_color').hasClass('spectrumed') && jQuery().spectrum) {
        addSpectrum();
    }
    if (getSetting('responsive') == 1) {
        if (!isResponsive) {
            setResponsive();
            isResponsive = true;
        }
    }
    checkJquery();
    checkOnlineStatus();
    hideMutedUsers();
    //checkSockets();
    socket.emit('requestUsers');
}

/**
 * Checks sockets and connects to them.
 */
function checkSockets() {
    require([sgNodeUrl + '/socket.io/socket.io.js'], function (io) {
        window.io = io;
        socket = window.io.connect(sgNodeUrl, {secure: true});
    });
}

/**
 * Scrolls chatlist to the bottom.
 */
function scrollChat() {
    var $chatListFrame = document.getElementById('list-frame'),
        $chatList = document.getElementById('forum-chat-list');

    if ($chatListFrame.scrollHeight - 65 < $chatListFrame.scrollTop + $chatListFrame.offsetHeight && getSetting('autoscroll') == 1) {
        $chatList.parentNode.scrollTop = $chatList.parentNode.scrollHeight;
    }
}

/**
 * Initializes some basic browser shit.
 */
function initData() {
    if (window.attachEvent && !window.addEventListener) {
        document.getElementById('forum-chat').innerHTML = 'HIBA: Túl régi a böngésződ a cset használatához...';
        return;
    }

    document.title = sgTitle;

    if (document.cookieHack) {
        document.cookie = document.cookieHack;
    }

    var match = document.cookie.match(new RegExp('identid' + '=([^;]+)'));
    if (match) {
        identid = match[1];
    }

    if (identid === undefined || identid.length == 0) {
        document.getElementById('forum-chat').innerHTML = 'HIBA: cookie hiba';
        return;
    }

    $('.col1').each(function () {
        $(this).text($(this).text().replace('Számítástechnikai fórumok', 'Számtech fórumok'));
        $(this).text($(this).text().replace(' és gazdaság', ''));
    });

    lines_color();

    return identid;
}

/**
 * Creates the hacked sidebar.
 */
function createNeoSidebar() {
    sidebar = document.createElement("div");
    sidebar.setAttribute('id', 'neosidebar');
    $(sidebar).html("<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>");
    document.body.appendChild(sidebar);

    updateSidebar();
}

/**
 * Creates the currently writing... div.
 */
function currentlyWriting() {
    $('#forum-wrap').prepend('<div id="breadcrumb"></div>');
    var bc = $('#breadcrumb');
    bc.html('');
    bc.prepend('<div id="currentlyWriting" class="currentlyWriting"><p id="eppenGepelnek">Éppen gépelnek:</p> <p id="currents" class="currents"></p></div>');
}

/*
 * Create BBCodes after the input
 */
function createBBCodes() {
    var bbcodesDiv = '<div id="BBCodes">';
    for (var i in bbcodes) {
        var bb = bbcodes[i]['bb'],
            html = bbcodes[i]['html'],
            str = '<span class="bbcs" onclick="iBBCodes(\'' + bb + '\')" ><' + html + '>' + bb + '</' + html + '></span>';

        bbcodesDiv += str
    }

    bbcodesDiv += '<span class="bbcs" style="color: red !important" onclick="iBBCodes(\'color\')">color</span>';
    // bbcodesDiv += '<span class="bbcs" style="font-family: Comic Sans MS !important" onclick="iBBCodes(\'font\')">font</span>';
    bbcodesDiv += '<span class="bbcs font" style="font-family: Comic Sans MS !important">font</span>';

    var fonts = ['Arial', 'Comic Sans MS', 'Courier New', 'Georgia', 'Impact', 'Tahoma', 'Times', 'Verdana'],
        menu = '<ul class="font-menu">';

    for (i in fonts) {
        var font = fonts[i];

        menu += '<li onclick="iBBCodes(\'font=' + font + '\')"><span class="fonts" style="font-family: ' + font + ' !important;">' + font + '</span></li>';
    }
    menu += '</ul>';
    bbcodesDiv += menu;

    bbcodesDiv += '<span class="bbcs img" onclick="iBBCodes(\'img\')">img</span>';
    bbcodesDiv += '<span class="bbcs burning" onclick="iBBCodes(\'burn\')">burn</span>';
    bbcodesDiv += '</div>';

    $('#inputs').append(bbcodesDiv);

    forumChatInput.after('<span id="bbcodesButton"><i></i></span>');

    var bbcodesButton = $('#bbcodesButton');
    var bbcodesDiv = $('#BBCodes');
    bbcodesButton.click(function () {
        bbcodesDiv.fadeToggle();
    });

    $('span.bbcs.font').click(function () {
        $('ul.font-menu').toggleClass('font-menu-open');
    });

    $('ul.font-menu li').click(function () {
        $('ul.font-menu').removeClass('font-menu-open');
    });
}

/**
 * Creates the smileys and songs at various places :D
 */
function createSmileysAndSongs() {
    var szmajliDiv = '<div id="smileys">';
    for (var i in smileys) {
        var mit = smileys[i]['mit'];
        var mire = smileys[i]['mire'];
        var kep = smileys[i]['kep'];
        var newLine = smileys[i]['ujsor'];
        var ext = smileys[i]['ext'];
        if (!ext) {
            ext = 'png';
        }

        var str = '<i alt="' + mit + '" smile="' + mit + '" class="paddingos smiley ' + mire + '"></i>';
        if (kep) {
            str = '<img title="' + mit + '" alt="' + mit + '" smile="' + mit + '" class="paddingos" src="' + baseUrl + 'images/' + mire + '.' + ext + '" />';
        }

        if (newLine) {
            szmajliDiv += '<br />';
        }

        szmajliDiv += str;
    }
    szmajliDiv += '</div>';

    var songsDiv = '<div id="songs">' + row_b;
    for (var i in songs) {
        var str = '<span class="easteregg">!' + songs[i]['str'] + '</span>';

        songsDiv += str;
    }

    songsDiv += row_e + '</div>';

    gif_tags = new Array("Gay", "Thumbs Up", "Fuck You", "WTF", "LOL", "OMG", "I Don't Care", "Yes", "Nope", "Mind Blown", "Right In The Feels", "Deal with it", "Facepalm", "Thank You", "You're Awesome", "Haters Gonna Hate", "Party Hard", "That's Racist", "Fail");

    var gifsDiv = '<div id="gifs">' + row_b;
    for (var i in gif_tags) {
        gifsDiv += '<span class="gif_tag">' + gif_tags[i] + '</span>';
    }

    gifsDiv += '<form style="display:block;" action="javascript:loadMemefulGifs($(\'#gif_search\').val());"> Search: <input type="text" id="gif_search"></form>' + row_e + '<div id="gif_results"></div></div>';

    $('div.extras div.songs').append(songsDiv);

    forumChatInput.after('<span id="smileyButton"><i></i></span>');
    forumChatInput.after('<span id="songButton"><i></i></span>');
    forumChatInput.after('<span id="gifButton"><i></i></span>');
    forumChatInput.after('<span id="spoilerButton" onclick="iBBCodes(\'spoiler\')"><i></i></span>');
    $('#inputs').append(szmajliDiv);

    $('#inputs').append(songsDiv);
    $('#inputs').append(gifsDiv);
    $('#inputs #songs').addClass('fadeSongs');

    var smileyButton = $('#smileyButton');
    var smileDiv = $('#smileys');
    smileyButton.click(function () {
        smileDiv.fadeToggle();
    });
    smileDiv.find('.paddingos').click(function () {
        forumChatInput.val(forumChatInput.val() + $(this).attr('smile'));
        forumChatInput.focus();
    });

    $('div.extras').find('.easteregg').click(function () {
        forumChatInput.val(forumChatInput.val() + $(this).text());
        forumChatInput.focus();
    });

    $('.fadeSongs').find('.easteregg').click(function () {
        forumChatInput.val(forumChatInput.val() + $(this).text());
        forumChatInput.focus();
    });

    $('#gifs').find('.gif_tag').click(function () {
        loadMemefulGifs($(this).html());
    });

    var songButton = $('#songButton');
    var songsDiv = $('.fadeSongs');
    songButton.click(function () {
        songsDiv.fadeToggle();
    });

    $('#gifButton').click(function () {
        $('#gifs').fadeToggle();
        $('#gif_search').val("");
        $('#gif_search').focus();
    });

    var timeoutId;
    $('#songs .easteregg').hover(function () {
            var btn = $(this).text();
            btn = btn.replace('!', '');

            if (!timeoutId) {
                timeoutId = window.setTimeout(function () {
                    timeoutId = null;

                    for (var i in songs) {
                        var str = songs[i]['str'],
                            mp3 = songs[i]['mp3'];

                        if (btn == str) {
                            player(mp3);
                        }
                    }

                }, 2500);
            }
        },
        function () {
            if (timeoutId) {
                window.clearTimeout(timeoutId);
                timeoutId = null;
            }
            else {
                $('#myAudio').remove();
            }
        }
    );

    $('#developers').hover(function () {
            if (!timeoutId) {
                timeoutId = window.setTimeout(function () {
                    timeoutId = null;
                    if (getSetting('autoplay') == 1) {
                        player('developers');
                    }

                }, 100);
            }
        },
        function () {
            if (timeoutId) {
                window.clearTimeout(timeoutId);
                timeoutId = null;
            }
            else {
                $('#myAudio').remove();
            }
        }
    );
}

/**
 * Load gifs from memeful.com -> ajax -> JSON -> #gif_results
 */
function loadMemefulGifs(tag) {
    if (tag) {
        $("#gif_results").html("Loading...");

        tag = tag.replace(/ /g, "+");
        var limit = getSetting('gifnum');
        //var url = '//api.giphy.com/v1/gifs/search?q=' + tag + '&limit=' + limit + '&api_key=' + gifApikey;

        var imgUrl = 'https://api.giphy.com/v1/gifs/search?api_key=' + giphyApiKey + '&q=' + tag + '&limit=' + limit + '&offset=0&lang=en';
        //var imgUrl = 'https://api.giphy.com/v1/gifs/search?api_key=' + giphyApiKey + '&limit=' + limit + '&tag=' + tag;
        //var imgUrl = 'https://api.popkey.co/v2/media/search?sort=popular&page_size=' + limit + '&q=' + tag;
        $.ajax({
            url: imgUrl,
            /*headers: {
                'Authorization': gifApikey
            },*/
            type: 'GET',
            success: function (data) {
                var d = data['data'];

                $("#gif_results").html("");
                console.log(d);

                for (var i in d) {
                    //var url = data[i]['images']['small']['gif'];
                    var url = d[i]['images']['downsized']['url'];
                    var jpg = d[i]['images']['fixed_height_still']['url'];
                    //var jpg = data[i]['images']['small']['jpg'];

                    $("#gif_results").append('<div class="gif_block" data-jpg="' + jpg + '" data-url="' + url + '" style="background-image:url(\'' + jpg + '\');" ><span></span></div>');
                }

                $(".gif_block").each(function () {
                    $(this).click(function () {
                        forumChatInput.val(forumChatInput.val() + '[img]' + $(this).data('url') + '[/img]');
                        forumChatInput.focus();
                        $("body").scrollTop();
                        $('#gifs').fadeToggle();
                    });

                    $(this).hover(function () {
                        $(this).css("background-image", "url('" + $(this).data('url') + "')");
                    }, function () {
                        $(this).css("background-image", "url('" + $(this).data('jpg') + "')");
                    });
                });
            }
        });
    }

    /*
    var imgUrl = '//api.imgur.com/3/gallery/t/' + t;
    $.ajax({
        url: imgUrl,
        headers: {
            'Authorization': 'Client-ID ' + gifClientId
        },
        type: 'GET',
        success: function(data) {
            console.log(data)

            $("#gif_results").html("");
            data = data['data']['items'];

            for (var i in data) {
                var url = data[i]['link'];
                var animated = data[i]['animated'] ? 'ANIMATED' : 'JPG';
                var animatedUrl = data[i]['animated'] ? data['i']['gifv'] : url;

                $("#gif_results").append("<div class='gif_block' type='" + animated + "' anim='" + animatedUrl + "' normal='" + url + "' style='background-image:url(\"" + animatedUrl + "\");' ><span>" + animated + "</span></div>");
            }

            $(".gif_block").each(function () {
                $(this).click(function () {
                    forumChatInput.val(forumChatInput.val() + '[img]' + $(this).attr('anim') + '[/img]');
                    forumChatInput.focus();
                    $("body").scrollTop();
                    $('#gifs').fadeToggle();
                });

                if ($(this).attr('type') == "ANIMATED") {
                    $(this).hover(function () {
                        $(this).css("background-image", "url('" + $(this).attr('anim') + "')");
                    }, function () {
                        $(this).css("background-image", "url('" + $(this).attr('normal') + "')");
                    });
                }
            });

        }
    });
    */

    /*
    $.get(url, function(data) {
        $("#gif_results").html("");
        data = data['data'];

        for (var i in data) {
            var url = '//media.giphy.com/media/' + data[i]['id'] + '/giphy.gif';
            url = data[i]['images']['fixed_width_downsampled']['url'];
            $("#gif_results").append("<div class='gif_block' type='" + data[i]['type'] + "' anim='" + url + "' normal='" + url + "' style='background-image:url(\"" + url + "\");' ><span>" + data[i]['type'] + "</span></div>");
        }

        $(".gif_block").each(function () {
            $(this).click(function () {
                forumChatInput.val(forumChatInput.val() + '[img]' + $(this).attr('anim') + '[/img]');
                forumChatInput.focus();
                $("body").scrollTop();
                $('#gifs').fadeToggle();
            });

            if ($(this).attr('type') == "ANIMATED") {
                $(this).hover(function () {
                    $(this).css("background-image", "url('" + $(this).attr('anim') + "')");
                }, function () {
                    $(this).css("background-image", "url('" + $(this).attr('normal') + "')");
                });
            }
        });

    });

    /*
    $.post(gifSearchUrl, {tags: tag}, function (data) {
        $("#gif_results").html("");
        data = data['data'];
        for (var i in data) {
            $("#gif_results").append("<div class='gif_block' type='" + data[i]['type'] + "' anim='" + data[i]['animatedUrl'] + "' normal='" + data[i]['imageUrl'] + "' style='background-image:url(\"" + data[i]['imageUrl'] + "\");' ><span>" + data[i]['type'] + "</span></div>");
        }

        $(".gif_block").each(function () {
            $(this).click(function () {
                forumChatInput.val(forumChatInput.val() + '[img]' + $(this).attr('anim') + '[/img]');
                forumChatInput.focus();
                $("body").scrollTop();
                $('#gifs').fadeToggle();
            });

            if ($(this).attr('type') == "ANIMATED") {
                $(this).hover(function () {
                    $(this).css("background-image", "url('" + $(this).attr('anim') + "')");
                }, function () {
                    $(this).css("background-image", "url('" + $(this).attr('normal') + "')");
                });
            }
        });
    }, 'JSON');
    */
}

/**
 * Shows error message in chat window.
 *
 * @param err
 */
function showError(err) {
    var chat = document.getElementById('forum-chat');
    var div = document.createElement('div');
    div.id = 'forum-chat-error';
    div.innerHTML = 'HIBA<br/>' + err;

    chat.appendChild(div);

    $(div).fadeOut(2000);
}

/**
 * ABC ordering the chat users.
 */
function ABCcset() {
    var mylist = $('#forum-chat-user-list');
    //noinspection JSValidateTypes
    var listItems = mylist.children('li').get();
    listItems.sort(function (a, b) {
        return $(a).text().toUpperCase().trim().localeCompare($(b).text().toUpperCase().trim());
    });
    $.each(listItems, function (index, item) {
        mylist.append(item);
    });

    $('.jsg-csetname-new').each(function () {
        colorListName($(this).attr('nick'), $(this));
    });
}

/**
 * Mutes a user.
 *
 * @param elem
 * @param user
 * @returns {*}
 */
function mute(elem, user) {
    if (inArray(user, gods)) {
        return alert("A topik házigazdáját biztonsági okokból nem lehet mute-olni, szíves elnézésedet kérem az esetleges kellemetlenségekért!");
    }

    if (inArray(user, getSetting('mute'))) {
        $(elem).removeClass("kussolsz");
        mute_list.splice(mute_list.indexOf(user), 1);
        removeSetting('mute', user);
    }
    else {
        $(elem).addClass("kussolsz");
        mute_list.push(user);
        addSetting('mute', user);
    }

    scrollToBottom();
    hideMutedUsers();
    return true;
}

/**
 * Hides a muted user's messages.
 */
function hideMutedUser(user) {
    if (inArray(user, getSetting('mute'))) {
        $('a.hangszoro.' + user).addClass('kussolsz');
        $(elem).addClass("kussolsz");
    }
}

/**
 * Hides muted users' messages.
 */
function hideMutedUsers() {
    var mutedUsers = getSetting('mute');

    for (var i in mutedUsers) {
        var mutedUser = mutedUsers[i];

        $('a.hangszoro.' + mutedUser).addClass('kussolsz');
    }

    $("#forum-chat").find("li").each(function () {
        var sender = $(this).find(".sender").html();

        if (sender) {
            if (inArray(sender, mutedUsers) || inArray(sender.replace('* ', ''), mutedUsers)) {
                $(this).hide();
            }
            else {
                $(this).show();
            }
        }
    });
}

/**
 * Sends a message.
 *
 * @param msg
 * @param socket
 */
function sendMessage(msg, socket) {
    if (msg === undefined || msg.length == 0) {
        alert('Hiba! Üres az üzeneted!');

        return;
    }

    if (msg == '!bovitmeny') {
        msg = 'https://sg.hu/chat/reklam_ban/index.html';
    }

    if (msg == '!bszn') {
        msg = bszn();
    }

    var date = new Date();
    var now = date.getTime();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    var time = ('0'  + hours).slice(-2) + ':' + ('0'  + minutes).slice(-2) + ':' + ('0' + seconds).slice(-2);

    if (msg == 'ping') {
        socket.emit('pong');
    }

    if (msg == '/afk') {
        socket.emit('request', {'type': 'onlineStatus', 'data': {'user': currentUser, 'status': 'afk', 'real': true}});
        msg = '/me afk ' + time + ' óta!';
        setSetting('afk', 1);
    }
    else if (msg.match(/\/afk/)) {
        var afkString = msg.split(/\/afk(.+)?/)[1];
        afkString = strip(afkString.trim());
        var reason = '';
        if (afkString) {
            reason = ' (' + afkString + ')';
        }
        socket.emit('request', {'type': 'onlineStatus', 'data': {'user': currentUser, 'status': 'afk', 'real': true}});
        msg = '/me afk ' + time + ' óta!' + reason;
        setSetting('afk', 1);
    }
    else {
        socket.emit('request', {'type': 'onlineStatus', 'data': {'user': currentUser, 'status': 'noafk', 'real': true}});
        setSetting('afk', 0);
    }

    if (msg.match(/\!xkcd [\d]+/)) {
        var xid = msg.split(/\!xkcd(.+)?/)[1];
        xid = strip(xid.trim());

        msg = 'https://xkcd.com/' + xid + '/';
    }

    if (msg.match(/\/xkcd [\d]+/)) {
        var xid = msg.split(/\/xkcd(.+)?/)[1];
        xid = strip(xid.trim());

        msg = 'https://xkcd.com/' + xid + '/';
    }

    if ((msg == getSetting('lastMessage')) && ((now - getSetting('lastMessageTime')) < floodTime)) {
        forumChatInput.val('');
        return;
    }
    else {
        socket.emit('input', {msg: msg, user: currentUser});

        setSetting('lastMessage', msg);
        setSetting('lastMessageTime', now);
    }

    /*
    if (msg.toLowerCase() == 'love is in the air') {
        socket.emit('request', {'type': 'love', 'data': {'user': currentUser}});
    }
    */

    if (msg == "!loál") {
        var nck = currentUser;

        $.getJSON("https://api.icndb.com/jokes/random?firstName=" + nck + "&lastName=", function (response) {
            var joke = response.value.joke;
            // console.log(joke);
            // joke = 'ennyire kemény vagyok: ' + joke;
            socket.emit('input', {msg: joke, user: currentUser});
        });

        /*
        $.ajax({
            url:"http://api.icndb.com/jokes/random?firstName=" + nck + "&lastName=",
            dataType: 'jsonp',
            success: function(dataWeGotViaJsonp){
                console.log(dataWeGotViaJsonp);
            }
        });
        */
    }

    if (msg == "!cat") {
        appendChatCat();
    }

    if (msg == "!rave") {
        if ($('body').attr('rave') == 'on') {
            clearInterval(interval);

            if (getSetting('responsive') == 1) {
                $(window).trigger('resize');
                setResponsive();
            }

            removeRave();
            $('body').attr('rave', 'off');
        }
        else {
            interval = setInterval("rave()", 2000);

            if (getSetting('responsive') == 1) {
                $(window).trigger('resize');
                setResponsive();
            }

            $('body').attr('rave', 'on');
        }
    }

    if (msg == "!nyan") {
        $('body').prepend('<div class="nyan"></div>');
        includeCss('nyan.css', baseUrl + 'nyan/');
        includeJs('nyan.js?rand=' + Math.random(), baseUrl + 'nyan/');
        player('nyancat');
        $('audio.nyancat').attr('loop', 'loop');
    }
    if (msg == "!barrelroll") {
        $('body').addClass('barrelroll').delay(2500).queue(function () {
            $('body').removeClass('barrelroll');
            $('body').dequeue();
        });
    }
    if (msg == "!grayscale") {
        if ($('html').hasClass('grayscale')) {
            $('html').removeClass('grayscale');
        }
        else {
            $('html').addClass('grayscale');
        }
    }

    if (msg == "!invert") {
        if ($('html').hasClass('invert')) {
            $('html').removeClass('invert');
        }
        else {
            $('html').addClass('invert');
        }
    }

    if (msg == "!monsterkill") {
        //player('monster_kill');
    }

    if (msg.toLowerCase() == "hots") {
        if (inArray(currentUser, hotsPlayers)) {
            player('hots');
        }
    }

    if (msg == '!katanyuszi' && getSetting('kata') == 1) {
        kataNyuszi();
    }

    socket.emit('request', {'type': 'notification', 'data': msg});

    for (var i in songs) {
        var str = songs[i]['str'],
            mp3 = songs[i]['mp3'];

        if (msg.indexOf('!' + str) == 0) {
            socket.emit('request', {'type': 'mp3', 'data': mp3});
        }
    }

    if (getSetting('thread') == 1 && currentUser != 'byzhouse') {
        if (msg.match(/.+ \/thread/)) {

            var thread = msg.split(/ \/thread(.+)?/)[0];
            thread = strip(thread.trim());
            var info = 'by ' + currentUser + ' ' + getCurrentDate();
            if (thread.length > threadLength) {
                thread = thread.substr(0, threadLength);
                thread = thread.substr(0, thread.lastIndexOf(" ")) + "\u2026";
            }

            if (thread.length > 0) {
                $.post(readThreadUrl, {
                    name: thread,
                    info: info
                }, function (response) {
                    socket.emit('request', {'type': 'newThread', 'data': currentUser});
                });
            }
        }
    }
}

/**
 * Set spoiler click event.
 */
function setSpoiler() {
    $('.spoilerText').off('click').on('click', function() {
        $(this).toggleClass('spoilerTextBold');
        $(this).parent().find('.spoilerContent').first().toggleClass('spoilerContentBlock');
        lines_color();
    });
}

/**
 * Writes a message to the chat window.
 *
 * @param msg
 * @param noscroll
 */
function msgToList(msg, noscroll) {
    var shadows = false;
    var time = new Date();
    //noinspection JSUnresolvedVariable
    if (msg.date) {
        //noinspection JSUnresolvedVariable
        time = new Date(msg.date);
    }

    var hours = time.getHours();
    if (hours < 10) {
        hours = '0' + hours;
    }

    var mins = time.getMinutes();
    if (mins < 10) {
        mins = '0' + mins;
    }

    var secs = time.getSeconds();
    if (secs < 10) {
        secs = '0' + secs;
    }

    var perMeString = msg.nick;
    var perMe = false;
    if (msg.text.indexOf('/me ') == 0) {
        perMeString = '* ' + msg.nick;
        msg.text = msg.text.replace('/me', '');
        perMe = true;
    }

    if (msg.text == '!bszn') {
        msg.text = bszn();
    }

    if (msg.text.indexOf('!rave') == 0) {
        perMeString = '* ' + msg.nick;
        if ($('body').attr('rave') == 'off') {
            msg.text = msg.text.replace('!rave', ' has stopped raving HARD!');
        }
        else {
            msg.text = msg.text.replace('!rave', ' is raving HARD!');
        }
        perMe = true;
    }

    if (msg.text.indexOf('!nyan') == 0) {
        perMeString = '* ';
        msg.text = msg.text.replace('!nyan', 'Nyan Cat is following ' + msg.nick + '!');
        perMe = true;
    }

    if (msg.text.indexOf('!grayscale') == 0) {
        perMeString = '* ' + msg.nick;
        if ($('html').hasClass('grayscale')) {
            msg.text = msg.text.replace('!grayscale', ': Grayscale mode ON');
        }
        else {
            msg.text = msg.text.replace('!grayscale', ': Grayscale mode OFF');
        }
        perMe = true;
    }

    if (msg.text.indexOf('!invert') == 0) {
        perMeString = '* ' + msg.nick;
        if ($('html').hasClass('invert')) {
            msg.text = msg.text.replace('!invert', ': Invert mode ON');
        }
        else {
            msg.text = msg.text.replace('!invert', ': Invert mode OFF');
        }
        perMe = true;
    }

    if (msg.text.indexOf('!barrelroll') == 0) {
        perMeString = '* ' + msg.nick;
        msg.text = msg.text.replace('!barrelroll', ' rolled like a barrel');
        perMe = true;
    }

    if (msg.text.indexOf('!pwt') == 0) {
        pwt();
        perMeString = '* ';
        msg.text = msg.text.replace('!pwt', '<span style="font-weight:bold; color:red">PWT -> <span class="pwt"></span></span>');
        perMe = true;
    }

    if (msg.text.indexOf('!sgteam') == 0 && getSetting('smiley') == 1) {
        msg.text = msg.text.replace('!sgteam', sgteam());
    }

    if (msg.text.indexOf('!flipcoin') == 0) {
        perMeString = '* ';
        msg.text = msg.text.replace('!flipcoin', flipcoin());
        perMe = true;
    }

    while (msg.text.indexOf('[burn]') >= 0 && msg.text.indexOf('[/burn]') >= 0) {
        msg.text = msg.text.replace('[burn]', '<span class="burning">');
        msg.text = msg.text.replace('[/burn]', '</span>');
    }

    while (msg.text.indexOf('[spoiler][/spoiler]') >= 0) {
        msg.text = msg.text.replace('[spoiler][/spoiler]', '[spoiler]Béna vagyok és csak úgy üresen ideraktam egy spoiler taget :([/spoiler]');
    }

    while (msg.text.indexOf('[spoiler]') >= 0 && msg.text.indexOf('[/spoiler]') >= 0) {
        msg.text = msg.text.replace('[spoiler]', '<span class="spoiler"><span class="spoilerText">--- Spoiler ---</span><span class="spoilerContent">');
        msg.text = msg.text.replace('[/spoiler]', '</span></span>');
    }

    if (msg.text.match(/\!xkcd [\d]+/)) {
        var xid = msg.text.split(/\!xkcd(.+)?/)[1];
        xid = strip(xid.trim());

        msg.text = 'https://xkcd.com/' + xid + '/';
    }

    if (msg.text.match(/\/xkcd [\d]+/)) {
        var xid = msg.text.split(/\/xkcd(.+)?/)[1];
        xid = strip(xid.trim());

        msg.text = 'https://xkcd.com/' + xid + '/';
    }

    if (getSetting('smiley') == 1) {
        for (var i in smileys) {
            var mit = smileys[i]['mit'];
            var mitUp = mit.toUpperCase();
            var mire = smileys[i]['mire'];
            var ext = smileys[i]['ext'];
            if (!ext) {
                ext = 'png';
            }

            var str = '<span class="szmajlivagyok" original="' + mire + '"><i class="smiley ' + mire + '" ></i></span>';
            if (smileys[i]['kep']) {
                str = '<span class="szmajlivagyok" original="' + mire + '"><img src=\'' + baseUrl + 'images/' + mire + '.' + ext + '\' /></span>';
            }

            if (msg.text.indexOf(mit) >= 0) {
                msg.text = msg.text.replace(new RegExp(smileyReplace(mit), 'gi'), str);
            }
            if (msg.text.indexOf(mitUp) >= 0) {
                msg.text = msg.text.replace(new RegExp(smileyReplace(mitUp), 'gi'), str);
            }
        }
    }

    for (var i in bbcodes) {
        var bb = bbcodes[i]['bb'];
        var bb_f = '[' + bb + ']';
        var bb_fc = '[/' + bb + ']';
        var html = bbcodes[i]['html'];
        var html_f = '<' + html + '>';
        var html_fc = '</' + html + '>';

        while (msg.text.indexOf(bb_f) >= 0 && msg.text.indexOf(bb_fc) >= 0) {
            msg.text = msg.text.replace(bb_f, html_f);
            msg.text = msg.text.replace(bb_fc, html_fc);
        }
    }

    while (msg.text.indexOf('[color=') >= 0 && msg.text.indexOf('[/color]') >= 0) {
        var start_pos = msg.text.indexOf('[color=') + 7;
        var end_pos = msg.text.indexOf(']', start_pos);
        var color = msg.text.substring(start_pos, end_pos);
        //console.log(color);

        var bb_f = '[color=' + color + ']';
        var bb_fc = '[/color]';
        var html_f = '<span style="color:' + color + '">';
        var html_fc = '</span>';

        msg.text = msg.text.replace(bb_f, html_f);
        msg.text = msg.text.replace(bb_fc, html_fc);
    }

    while (msg.text.indexOf('[font=') >= 0 && msg.text.indexOf('[/font]') >= 0) {
        var start_pos = msg.text.indexOf('[font=') + 6;
        var end_pos = msg.text.indexOf(']', start_pos);
        var font = msg.text.substring(start_pos, end_pos);
        //console.log(font);

        var bb_f = '[font=' + font + ']';
        var bb_fc = '[/font]';
        var html_f = '<span style="font-family:' + font + '">';
        var html_fc = '</span>';

        msg.text = msg.text.replace(bb_f, html_f);
        msg.text = msg.text.replace(bb_fc, html_fc);
    }

    for (var i in songs) {
        var str = songs[i]['str'],
            mp3 = songs[i]['mp3'];

        if (msg.text.indexOf('!' + str) == 0) {
            msg.text = msg.text.replace('!' + str, '<br /><div class="player_cont"><div class="player_btn ' + mp3 + '" onclick="player_btn(\'' + mp3 + '\');"><span class="title">' + str + '</span></div></div>');
        }
    }

    var dont_linkify = false;

    while (msg.text.indexOf('[img]') >= 0 && msg.text.indexOf('[/img]') >= 0) {
        var bbc_img_b = '[img]',
            bbc_img_e = '[/img]';

        if (msg.text.indexOf('.webm') >= 0) { // webm
            html_img_b = '<video width="120" height="120" controls="false" autoplay="autoplay" loop="true" muted="true"><source src="';
            html_img_e = '" type="video/webm"></video>';
            dont_linkify = true; // mert elbassza a linket
        }
        else if (msg.text.indexOf('.mp4') >= 0) { // mp4
            html_img_b = '<video width="120" height="120" controls="false" autoplay="autoplay" loop="true" muted="true"><source src="';
            html_img_e = '" type="video/mp4"></video>';
            dont_linkify = true; // mert elbassza a linket
        }
        else { // Sima kép
            html_img_b = '<img src="';
            html_img_e = '">';
        }

        msg.text = msg.text.replace(bbc_img_b, html_img_b);
        msg.text = msg.text.replace(bbc_img_e, html_img_e);
    }

    if (dont_linkify == false)
        msg.text = Linkify(msg.text);

    var row = document.createElement('li');

    var timeTile = document.createElement('time');
    if (getSetting('timeformat') == 'seconds') {
        timeTile.innerHTML = '[' + hours + ':' + mins + ':' + secs + '] ';
    }
    else {
        timeTile.innerHTML = '[' + hours + ':' + mins + '] ';
    }
    timeTile.setAttribute('data-hour', hours.toString());
    timeTile.setAttribute('data-min', mins.toString());
    timeTile.setAttribute('data-sec', secs.toString());

    var senderTile = document.createElement('span');
    senderTile.className = 'sender';
    senderTile.addEventListener("click", function () {
        userClicked(msg.nick);
    });

    senderTile.innerHTML = perMeString;

    /*
    if (msg.nick == 'LasDen') {
        if (Math.floor(Math.random() * 10) > 4) {
            msg.text = 'ezt a hozzászólást Tóbiás megette';
        }
    }
    */

    var msgTile = document.createElement('span');
    msgTile.className = 'msg text';
    msgTile.innerHTML = msg.text;

    if (perMe) {
        row.classList.add('perme');
    }
    row.appendChild(timeTile);
    row.appendChild(senderTile);

    if (!perMe) {
        /*
        var dotTile = document.createElement('span');
        dotTile.className = 'msg kettospoint';
        dotTile.innerHTML = ': ';
        row.appendChild(dotTile);
        */
        senderTile.innerHTML += ': ';
    }

    row.appendChild(msgTile);

    if (msg.nick === currentUser) {
        row.classList.add('own');
    }
    if (msg.nick !== lastUser) {
        row.classList.add('new-person');

    }
    else {
        row.classList.add('old-person');
    }
    lastUser = msg.nick;

    $chatList.appendChild(row);

    if (inArray(msg.nick, getSetting('mute'))) {
        $(row).css('display', 'none');
        if ($chatListFrame.scrollHeight - 65 < $chatListFrame.scrollTop + $chatListFrame.offsetHeight && getSetting('autoscroll') == 1) {
            $chatList.parentNode.scrollTop = $chatList.parentNode.scrollHeight;
        }
    }

    if (getSetting('embed') == 1) {
        soundcloud();
        youtube();
        vimeo();
        coub();
        videos();
        //facebook();
    }
    sgTopicLinks();
    setSpoiler();

    /**
     * Pixx :D
     */
    $('a[href="/felhasznalo/1221317696"]').css('color', getColorByName('pxxl'));
    $('a[href="/felhasznalo/1221317696"]').css('text-shadow', 'rgb(254, 253, 200) 0em 0em 0.1em, rgb(254, 236, 134) 0.0227403680991153em -0.02em 0.15em, rgb(255, 174, 51) 0.0431892248175139em -0.04em 0.2em, rgb(239, 117, 11) 0.0986992126983819em -0.08em 0.25em, rgb(203, 68, 6) 0.186357970147938em -0.15em 0.3em, rgb(152, 54, 21) 0.250047841466772em -0.2em 0.4em, rgb(67, 26, 13) 0.569757964914258em -0.5em 0.5em');
    $('a[href="/felhasznalo/1101142569"]').css('text-shadow', '1px 1px 1px rgba(0, 0, 0, 0.5)');

    $('span.sender:contains("pxxl")').each(function () {
        //$(this).css('color', '#fdfdfd');
        $(this).css('text-shadow', 'rgb(254, 253, 200) 0em 0em 0.1em, rgb(254, 236, 134) 0.0227403680991153em -0.02em 0.15em, rgb(255, 174, 51) 0.0431892248175139em -0.04em 0.2em, rgb(239, 117, 11) 0.0986992126983819em -0.08em 0.25em, rgb(203, 68, 6) 0.186357970147938em -0.15em 0.3em, rgb(152, 54, 21) 0.250047841466772em -0.2em 0.4em, rgb(67, 26, 13) 0.569757964914258em -0.5em 0.5em');
    });
    /*
    $('span.sender:contains("j0nNyKa")').each(function () {
        $(this).css('text-shadow', '1px 1px 1px rgba(0, 0, 0, 0.5)');
    });
    */

    /*
    if (jQuery().burn) {
        $('.burning').burn();
    }
    */

    //noinspection JSValidateTypes
    if (mute_list.indexOf(msg.nick) != -1 || mute_list.indexOf('* ' + msg.nick) != -1) // muteolva van
        $(row).hide();

    // color it
    colorName($(senderTile));

    limitMessages();

    if (!noscroll && getSetting('autoscroll') == 1) {
        scrollToBottom();
        //setTimeout(scrollToBottom, 500);
    }
}

/**
 * Limit chat messages.
 */
function limitMessages() {
    var list = $chatList.getElementsByTagName('li'),
        chatLimit = getSetting('chatnum') || 500;

    while (list.length > chatLimit) {
        $chatList.removeChild(list[0]);
    }
}

/**
 * Linkify thread text.
 */
function linkifyThread() {
    var tn = $('#threadName');
    tn.html(Linkify(tn.text()));

    tn.find('a').click(function (e) {
        //e.preventDefault();
        e.stopPropagation();
    })
}

/**
 * Fires when a new user joins the chat.
 *
 * @param user
 *
 function userToList(user) {
    if (!user) {
        return;
    }

    if ($users[user] > 0) {
        ++$users[user];
        return;
    }

    $users[user] = 1;

    var kuss = document.createElement('a');
    kuss.addEventListener("click", function () {
        mute(this, user);
    }, false);
    kuss.innerHTML = " ";
    kuss.className = "hangszoro " + user;

    var link = document.createElement('a');
    //link.href = '/felhasznalo/' + user['userid'];
    link.innerHTML = user;

    var status = document.createElement('a');
    status.innerHtml = " ";
    status.className = "chatStatus " + user;
    status.title = "Nem használja az addont.";

    var row = document.createElement('li');
    row.id = 'userlist-user-' + user;

    if (inArray(user, gods)) {
        if (!$(row).hasClass('sg-bold')) {
            $(row).addClass('sg-bold');
            link.innerHTML = '<span style="display:none">000</span><i class="gods"> </i>' + link.innerHTML;
        }
    }
    else if (inArray(user, mods)) {
        if (!$(row).hasClass('sg-bold')) {
            $(row).addClass('sg-bold');
            link.innerHTML = '<span style="display:none">001</span><i class="mods"> </i>' + link.innerHTML;
        }
    }
    else if (inArray(user, girls)) {
        if (!$(row).hasClass('sg-bold')) {
            $(row).addClass('sg-bold');
            link.innerHTML = '<span style="display:none">002</span><i class="girls"> </i>' + link.innerHTML;
            //<img src="http://kocsog.eu/sgchat/kata.png" />
        }
    }
    else if (inArray(user, gays)) {
        if (!$(row).hasClass('sg-bold')) {
            $(row).addClass('sg-bold');
            link.innerHTML = '<span style="display:none">003</span><i class="gays"> </i>' + link.innerHTML;
        }
    }
    else {
        link.innerHTML = '<span style="display:none">999</span>' + link.innerHTML;
    }

    $(link).attr('nick', user);
    colorListName(user, $(link));

    row.appendChild(kuss);
    row.appendChild(link);
    row.appendChild(status);

    var age_element = document.createElement('span');
    age_element.className = "age";
    age_element.alt = user;
    row.appendChild(age_element);

    //readAge(user, age_element);

    $userList.insertBefore(row, $userList.firstChild);
    $(row).css("display", "none");
    $(row).slideDown();
}
 */

function userToList(user) {
    if (!user || parseInt(user['userid']) == 0) {
        return;
    }

    if ($users[user['userid']] > 0) {
        ++$users[user['userid']];
        return;
    }

    $users[user['userid']] = 1;

    var chatcolor = settings[user];
    var szulinap = user['szulinap'];
    var nem = user['nem'];
    var age = 0;
    var szd = szulinap.split('-');

    if (szd.length == 3) {
        if (szd[0] > 1900) {
            age = calculateAge(szd[0], szd[1], szd[2]);
        }
    }

    var kuss = document.createElement('a');
    kuss.addEventListener("click", function () {
        mute(this, user['nick']);
    }, false);
    kuss.innerHTML = " ";
    kuss.className = "hangszoro " + fixUserName(user['nick']);

    var link = document.createElement('a');
    link.href = '/felhasznalo/' + user['userid'];
    link.innerHTML = user['nick'];
    link.className = "chatname";
    link.setAttribute('target', '_blank');

    if (chatcolor) {
        if (chatcolor.indexOf('#') !== 0) {
            chatcolor = '#' + chatcolor;
        }
        link.style.color = chatcolor;
        settings[user['nick']] = chatcolor;
    }

    var status = document.createElement('a');
    status.innerHtml = " ";
    status.className = "chatStatus " + fixUserName(user['nick']);
    status.title = "Nem használja az addont.";

    var ops = user['os'];
    var opsystem = document.createElement('a');
    opsystem.innerHtml = ' ';
    opsystem.className = "chatOpsystem " + ops;
    opsystem.title = ops;

    var row = document.createElement('li');
    row.id = 'userlist-user-' + user['userid'];

    if (inArray(user['nick'], gods)) {
        if (!$(row).hasClass('sg-bold')) {
            $(row).addClass('sg-bold');
            link.innerHTML = '<span style="display:none">000</span><i class="gods"> </i>' + link.innerHTML;
        }
    }
    else if (inArray(user['nick'], mods)) {
        if (!$(row).hasClass('sg-bold')) {
            $(row).addClass('sg-bold');
            link.innerHTML = '<span style="display:none">001</span><i class="mods"> </i>' + link.innerHTML;
        }
    }
    else if (inArray(user['nick'], girls)) {
        if (!$(row).hasClass('sg-bold')) {
            $(row).addClass('sg-bold');
            link.innerHTML = '<span style="display:none">002</span><i class="girls"> </i>' + link.innerHTML;
            //<img src="http://kocsog.eu/sgchat/kata.png" />
        }
    }
    else if (inArray(user['nick'], retards)) {
        if (!$(row).hasClass('sg-bold')) {
            $(row).addClass('sg-bold');
            link.innerHTML = '<span style="display:none">004</span><i class="retards"> </i>' + link.innerHTML;
            //<img src="http://kocsog.eu/sgchat/kata.png" />
        }
    }
    else if (inArray(user['nick'], gays)) {
        if (!$(row).hasClass('sg-bold')) {
            $(row).addClass('sg-bold');
            link.innerHTML = '<span style="display:none">003</span><i class="gays"> </i>' + link.innerHTML;
        }
    }
    else {
        link.innerHTML = '<span style="display:none">999</span>' + link.innerHTML;
    }

    $(link).attr('nick', user['nick']);
    colorListName(user['nick'], $(link));

    row.appendChild(kuss);
    row.appendChild(link);
    row.appendChild(status);
    row.appendChild(opsystem);

    var age_element = document.createElement('span');
    age_element.className = "age";
    age_element.alt = user['userid'];
    age_element.innerHTML = formatAge(age, nem.toUpperCase());
    row.appendChild(age_element);

    //readAge(user['userid'], age_element);

    $userList.insertBefore(row, $userList.firstChild);
    /*$(row).css("display", "none");
    $(row).slideDown();*/
}

/**
 * Set caret function in a textfield
 *
 * @param elemId
 * @param caretPos
 */
function setCaretPosition(elemId, caretPos) {
    var elem = document.getElementById(elemId);

    if (elem != null) {
        if (elem.createTextRange) {
            var range = elem.createTextRange();
            range.move('character', caretPos);
            range.select();
        }
        else {
            if (elem.selectionStart) {
                elem.focus();
                elem.setSelectionRange(caretPos, caretPos);
            }
            else {
                elem.focus();
            }
        }
    }
}

/**
 * img spam filter or what
 */
function spamImg() {
    $('.msg').each(function () {
        var img = $(this).find('img').length;
        var sgteam = $(this).find('.sgteam').length;
        if (img > 3 && sgteam == 0) {
            $(this).html('<span style="color:red; font-weight:bold">Ne spam-elj képeket! Egy üzenetbe maximum 3 kép szúrható be.</span>');
        }
    });
    setTimeout('spamImg()', 1000);
}
spamImg();

console.log('%c Chat command functions loaded. ', log_style_done);