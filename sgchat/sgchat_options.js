console.log('%c Options loading started... ', log_style_start);

var header_b = '<div id="sgcset-header" class="cf">',
    div_e = '</div>',
    row_b = '<span class="row">',
    row_e = '</span>',
    options_b = '<div class="options cf">',
    h2_header = '<h2 id="sgneojonny" class="sgneojonny"><a class="lefthead" href="https://sg.hu/chat/reklam_ban/index.html" target="_blank">SG tuning</a> by <span id="developers"><span class="jonnyh2">j0nNyKa</span> &amp; <span class="pixxelh2">pxxl</span> &amp; <span class="neoh2">Neocortex</span></span><h2 id="clock" class="sgneojonny"></h2>';
    if (getSetting('jedi') == 1) {
        h2_header += '<span id="jedimester" style="margin-left: 50px;">R.I.P. <img src="//sg.hu/kep/nicklogok/10167231261404452237.gif" title="Jedi Mester" height="13px" style="margin-bottom: 3px;"></span>';
    }
    h2_header += '</h2>',
    twitch_btn = '<a class="righthead twitch" onclick=\'toggleTwitch()\'><i></i></a>',
    bomb_btn = '<a class="righthead bomb" onclick=\'startBomb()\'><i></i></a>',
    openclose_btn = '<a class="righthead openclose" onclick=\'openclose()\'><i></i></a>',
    //codes_btn = '<a class="righthead code" onclick=\'$(".extras").slideToggle(1000);\'><i></i></a>',
    //opt_btn = '<a class="righthead option" onclick=\'$(".options").slideToggle(1000);\'><i></i></a>',
    logs_btn = '<a class="righthead logs" onclick=\'createModal(changelog, null)\'><i></i></a>',
    codes_btn = '<a class="righthead code" onclick=\'optionsModal(extrasToModal)\'><i></i></a>',
    opt_btn = '<a class="righthead option" onclick=\'optionsModal(optionsToModal)\'><i></i></a>',

    h3_cset_opt = '<h3 class="title">Cset beállítások</h3>',

    name_color_label = '<div class="options_chat_label">Neved színe:</div>',
    name_bg_color_label = '<div class="options_chat_label">Neved háttér színe:</div>',
    lines_color_label = '<div class="options_chat_label">Lines téma alap szín:</div>',
    name_color_input = '<input id="options_color" class="options_chat_input" type="text">',
    name_bg_color_input = '<input onkeyup="fast_name_color();" id="options_bgcolor" class="options_chat_input" type="text">',
    lines_color_input = '<input id="options_linescolor" class="options_chat_input" type="text">',
    name_row = row_b + name_color_label + name_color_input + row_e,
    name_bg_row = row_b + name_bg_color_label + name_bg_color_input + row_e,
    lines_color_row = row_b + lines_color_label + lines_color_input + row_e,

    separator = '<div class="separator"></div>',

    save_btn = '<input type="button" value="Mentés" class="optionsButton" id="options_save_button" onclick="options_save();">',

    extras_b = '<div class="extras cf">',
    h3_extras = '<h3 class="title">Extrák</h3>',
    codes_b = '<div class="codes">',
    h4_codes = '<h4 class="alt_title">Használható BBCode-ok</h4>',
    extras_label = '<div class="extras_label">',
    extras_desc = '<div class="extras_desc">',
    easters_b = '<div class="easters">',
    h4_easters = '<h4 class="alt_title" style="padding-top: 0 !important;">Easter Egg-ek</h4>',
    songs_b = '<div class="songs">',
    h4_songs = '<h4 class="alt_title" style="padding-top: 0 !important;">Hangok</h4>',
    easters_row = row_b + '<span class="easteregg">/me Lorem</span><span class="easteregg">!loál</span><span class="easteregg">!rave</span><span class="easteregg">!nyan</span><span class="easteregg">!grayscale</span><span class="easteregg">!invert</span><span class="easteregg">!barrelroll</span><span class="easteregg">!flipcoin</span><span class="easteregg">!pwt</span>' + row_e;

var songsString = row_b;
for (var i in songs) {
    var song = songs[i].str;
    songsString += '<span class="easteregg">!' + song + '</span>';
}
songsString += row_e;

function BBCodes() {
    var bbcodesDiv = '<div id="bbcodes">';
    for (var i in bbcodes) {
        var bb = bbcodes[i]['bb'];
        var bb_f = '[' + bb + ']';
        var bb_fc = '[/' + bb + ']';
        var html = bbcodes[i]['html'];
        var html_f = '<' + html + '>';
        var html_fc = '</' + html + '>';

        var str = row_b + extras_label + bb_f + 'Lorem ipsum' + bb_fc + div_e + extras_desc + html_f + 'Lorem ipsum' + html_fc + div_e + row_e;

        bbcodesDiv += str;
    }
    var str_color = row_b + extras_label + '[color=red]Lorem ipsum[/color] <br />&nbsp;' + div_e + extras_desc + '<span style="color:red">Lorem ipsum</span>  // színként használható: red, green, blue stb., <br /> #ff0000, rgb(255,0,0), rgba(255,0,0,.5)' + div_e + row_e;
    var str_font = row_b + extras_label + '[font=Comic Sans MS]Lorem ipsum[/font]' + div_e + extras_desc + '<span style="font-family:Comic Sans MS">Lorem ipsum</span>  // <a href="http://www.w3schools.com/cssref/css_websafe_fonts.asp" target="_blank">CSS Websafe fonts</a>' + div_e + row_e;
    var str_img = row_b + extras_label + '[img]http://www.link.hu/img.jpg[/img]' + div_e + extras_desc + '<img src="http://imagerz.com/QBVOCUtvAwMMUApOEQVR">' + div_e + row_e;
    var str_burn = row_b + extras_label + '[burn]Lorem ipsum[/burn]' + div_e + extras_desc + '<span class="burning">Lorem ipsum</span>' + div_e + row_e;

    bbcodesDiv += str_color;
    bbcodesDiv += str_font;
    bbcodesDiv += str_img;
    bbcodesDiv += str_burn;
    bbcodesDiv += '</div>';

    return bbcodesDiv;
}

var bbcodesString = row_b;
bbcodesString += BBCodes();
bbcodesString += row_e;

//$('#content').prepend(header_b + h2_header + opt_btn + codes_btn + openclose_btn + div_e + options_b + h3_cset_opt + name_row + name_bg_row + save_btn + separator + optionsString + div_e);
$('#content').prepend(header_b + h2_header + opt_btn + codes_btn + logs_btn + openclose_btn + twitch_btn + /*bomb_btn +*/ div_e);

//$('div.options').after(extras_b + h3_extras + codes_b + h4_codes + div_e + separator + easters_b + h4_easters + easters_row + div_e + separator + songs_b + h4_songs + div_e + div_e);

function extrasToModal() {
    var str = extras_b + h3_extras + codes_b + h4_codes + bbcodesString + div_e + separator + easters_b + h4_easters + easters_row + div_e + separator + songs_b + h4_songs + songsString + div_e + div_e;
    return str;
}

function optionsToModal() {
    var optionsString = '';

    for (var op in localStoreSettings) {
        var currentSetting = localStoreSettings[op];
        var type = currentSetting['type'];
        var id = currentSetting['id'];
        var name = currentSetting['name'];
        var help = currentSetting['help'];
        var string = '';

        if (currentSetting['separator']) {
            string += separator;
        }

        string += '<span class="row"><div class="options_chat_label">' + name + ':</div>';

        if (type == 'select') {
            string += '<select id="options_' + id + '">';
            var options = currentSetting['options'];
            var selectedValue = getSetting(id);

            for (var i in options) {
                var option = options[i];
                for (var j in option) {
                    var opt = option[j];
                    var selected = '';
                    if (j == selectedValue) {
                        selected = ' selected';
                    }
                    string += '<option value="'+ j +'"' + selected + '>' + opt + '</option>';
                }
            }

            string += '</select>';
        }
        if (type == 'checkbox') {
            var checkedValue = checkedVal(id);
            string += '<label><input id="options_' + id + '" type="checkbox" ' + checkedValue +'><i></i></label>';
        }
        if (type == 'text') {
            string += '<label><i></i></label><input type="text" class="optionsText" id="options_' + id + '" value="' + getSetting(id) + '">';
        }

        string += '<i class="help"></i>';
        string += '<span class="help-msg">' + help + '</span>';
        string += '</span>';

        if (id == 'twitchuser') {
            string += '<span class="row"><input type="button" value="Twitch felhasználó mentése" id="options_twitch_button" class="optionsButton" onclick="options_twitch_save();"></span>';
        }

        if (type != 'disabled' && (modUser || currentSetting['mod'] != true)) {
            optionsString += string;
        }
    }
    var str = options_b + h3_cset_opt + name_row + /*name_bg_row*/ lines_color_row + save_btn + separator + optionsString + div_e;
    return str;
}

function optionsModal(data) {
    createModal(data, null);

    optionsAllIn();
    addSpectrum();
    $('.modal-cont .easteregg').click(function() {
        $textInput.value = $textInput.value + $(this).text();
        removeModal();
        $textInput.focus();
    });

    //$('.burning').burn();
}

function toggleTwitch() {
    var twitchUser = getSetting('twitchuser');
    var twitchSrc = '//www.twitch.tv/' + twitchUser + '/embed';
    var twitchDiv = '<div id="twitchPlayer"><iframe src="' + twitchSrc + '" frameborder="0" scrolling="no" height="378" width="620"></iframe></div>';
    var twitchPlayer = $('#twitchPlayer');
    var lis = $('#forum-chat li');
    var newWidth = $('#forum-chat > .main-window').width() - 640;

    if (twitchPlayer.length > 0) {
        twitchPlayer.remove();
        lis.attr('style', 'width: 100%');
        scrollToBottom();
    }
    else if (getSetting('twitch') == 1) {
        $('.main-window.pull-left').append(twitchDiv);
        lis.attr('style', 'width: ' + newWidth + 'px');
        scrollToBottom();
    }
}

function openclose() {
    var val = 1;
    if ($("#forum-chat").is(":visible") == true) {
        val = 0;
    }

    setSetting('chatshow', val);

    chatToggle();
}

function deleteSmileys() {
    $('#forum-chat-list').find('.szmajlivagyok').each(function () {
        var mire = $(this).attr('original');
        var tombben = smileys.filter(function (sm) {
            return sm.mire == mire
        });
        $(this).replaceWith(tombben[0].mit.toUpperCase());
    });
}

function redoSmileys() {
    $('#forum-chat-list').find('.msg').each(function () {
        for (var i in smileys) {
            var mit = smileys[i]['mit'];
            var mitUp = mit.toUpperCase();
            var mire = smileys[i]['mire'];
            var ext = smileys[i]['ext'];
            if (!ext) {
                ext = 'png';
            }

            var str = '<span class=\'szmajlivagyok\' original=\'' + mire + '\'><i class=\'smiley ' + mire + '\' ></i></span>';
            if (smileys[i]['kep']) {
                str = '<span class="szmajlivagyok" original="' + mire + '"><img src=\'' + baseUrl + 'images/' + mire + '.' + ext + '\' /></span>';
            }

            var text = $(this).html();
            if (text.indexOf(mit) >= 0) {
                text = text.replace(new RegExp(smileyReplace(mit), 'gi'), str);
            }
            if (text.indexOf(mitUp) >= 0) {
                text = text.replace(new RegExp(smileyReplace(mitUp), 'gi'), str);
            }

            $(this).html(text);
        }
    });
}

function setTimes() {
    $('#forum-chat-list').find('time').each(function () {
        var hours = $(this).data('hour');
        var mins = $(this).data('min');
        var secs = $(this).data('sec');

        if (getSetting('timeformat') == 'seconds') {
            $(this).html('[' + hours + ':' + mins + ':' + secs + '] ');
        }
        else {
            $(this).html('[' + hours + ':' + mins + '] ');
        }
    });
}

function valueFromCheckbox(div) {
    var val = 0;
    var chkd = div.prop('checked');
    if (chkd) {
        val = 1;
    }

    return val;
}

function options_save() {
    $("#options_save_button").val("Mentés...");

    var cVal = $('#options_color').val();
    var lVal = $('#options_linescolor').val();

    if (cVal) {
        socket.emit('chatcolor', {identid: identid, color: cVal});
    }
    if (lVal) {
        setSetting('lines_color', lVal);
    }

    $("#options_save_button").val("Mentve!");
    /*
     $.post(phpUrl, {
     user: currentUser,
     color: $("#options_color").val(),
     bgcolor: $("#options_bgcolor").val(),
     side: sidebar
     }, function () {
     //initServerData();
     $("#options_save_button").val("Mentve!");
     });
     */
}

function options_twitch_save() {
    setSetting('twitchuser', $("#options_twitchuser").val());

    var twitchPlayer = $('#twitchPlayer');
    var lis = $('#forum-chat li');
    twitchPlayer.remove();
    lis.attr('style', 'width: 100%');
    scrollToBottom();
}

function optionsAllIn() {
    $("#options_favs").click(function (event) {
        var val = valueFromCheckbox($(this));

        setSetting('favs', val);
        updateSidebar();
    });


    $('#options_custom_css').change(function () {
        var val = $(this).val();
        setSetting('custom_css', val);
        loadCustomLocalCss();

        if (getSetting('responsive') == 1) {
            $(window).trigger('resize');
        }

        recolorAllNames();
    });

// waitUntilExists() neosidebar miatt...
    (function(e,f){var b={},g=function(a){b[a]&&(f.clearInterval(b[a]),b[a]=null)};e.fn.waitUntilExists=function(a,h,j){var c=this.selector,d=e(c),k=d.not(function(){return e(this).data("waitUntilExists.found")});"remove"===a?g(c):(k.each(a).data("waitUntilExists.found",!0),h&&d.length?g(c):j||(b[c]=f.setInterval(function(){d.waitUntilExists(a,h,!0)},500)));return d}})(jQuery,window);

    $("#options_asidebar").waitUntilExists(function() {
        $("#options_asidebar").click(function (event) {
            var val = valueFromCheckbox($(this));

            setSetting('asidebar', val);

            if (val == 1) {
                $('#asidebar').appendTo($('#grid'));
            }
            else {
                $('#asidebar').prependTo($('#grid'));
            }
        });    
    });

    $("#options_responsive").click(function (event) {
        var val = valueFromCheckbox($(this));

        setSetting('responsive', val);

        if (val == 1) {
            setResponsive();
        }
        else {
            resetResponsive();
        }
    });

    $("#options_header").click(function (event) {
        var val = valueFromCheckbox($(this));

        if (val == 1) {
            headerTop.show();
        }
        else {
            headerTop.hide();
        }

        setSetting('header', val);
    });

    $("#options_smiley").click(function (event) {
        var val = valueFromCheckbox($(this));

        setSetting('smiley', val);

        var smileyButton = $('#smileyButton');
        var smileysDiv = $('#smileys');

        if (val == 1) {
            smileyButton.show();
            redoSmileys();
        }
        else {
            smileyButton.hide();
            smileysDiv.hide();
            deleteSmileys();
        }
    });

    $("#options_embed").click(function (event) {
        var val = valueFromCheckbox($(this));

        setSetting('embed', val);

        if (val == 1) {
            soundcloud();
            youtube();
            vimeo();
            coub();
            videos();
            //facebook();
        }
        else {
            removeYoutube();
            removeSoundcloud();
            removeVimeo();
            removeCoub();
            removeVideos();
        }
    });

    $("#options_autoplay").click(function (event) {
        var val = valueFromCheckbox($(this));

        setSetting('autoplay', val);
    });

    $("#options_autoscroll").click(function (event) {
        var val = valueFromCheckbox($(this));

        setSetting('autoscroll', val);
    });

    $("#options_chatshow").click(function (event) {
        var val = valueFromCheckbox($(this));

        if (val != getSetting('chatshow')) {
            chatToggle();
        }

        setSetting('chatshow', val);
    });

    $("#options_fbbox").click(function (event) {
        var val = valueFromCheckbox($(this));

        if (val != getSetting('fbbox')) {
            fbToggle();
        }

        setSetting('fbbox', val);
    });

    $("#options_thread").click(function (event) {
        var val = valueFromCheckbox($(this));

        if (val != getSetting('thread')) {
            threadToggle(val);
        }

        setSetting('thread', val);
    });

    $("#options_compact").click(function (event) {
        var val = valueFromCheckbox($(this));

        if (val != getSetting('compact')) {
            compactToggle(val);
        }

        setSetting('compact', val);
    });

    $("#options_jedi").click(function (event) {
        var val = valueFromCheckbox($(this));

        if (val != getSetting('jedi')) {
            jediToggle();
        }

        setSetting('jedi', val);
    });

    $("#options_snow").click(function (event) {
        var val = valueFromCheckbox($(this));

        toggleSnow(false);

        setSetting('snow', val);
    });

    $("#options_candles").click(function (event) {
        var val = valueFromCheckbox($(this));

        toggleCandles(false);

        setSetting('candles', val);
    });

    $("#options_monster").click(function (event) {
        var val = valueFromCheckbox($(this));

        toggleMonster(false);

        setSetting('monster', val);
    });

    $("#options_haloween").click(function (event) {
        var val = valueFromCheckbox($(this));

        toggleHaloween(false);

        setSetting('haloween', val);
    });

    $("#options_macska").click(function (event) {
        var val = valueFromCheckbox($(this));

        toggleCat(false);

        setSetting('macska', val);
    });

    $("#options_kata").click(function (event) {
        var val = valueFromCheckbox($(this));

        setSetting('kata', val);
    });

    $("#options_timeformat").change(function (event) {
        var val = $(this).val();

        setSetting('timeformat', val);

        setTimes();
    });

    $("#options_sgiframe").change(function (event) {
        var val = $(this).val();

        setSetting('sgiframe', val);

        updateSgLinks();
    });

    $("#options_twitch").click(function (event) {
        var val = valueFromCheckbox($(this));

        setSetting('twitch', val);
    });

    $("#options_sidebar_newtopics").click(function (event) {
        var val = valueFromCheckbox($(this));

        setSetting('sidebar_newtopics', val);

        updateBoxes();
    });

    $("#options_sidebar_cooltopics").click(function (event) {
        var val = valueFromCheckbox($(this));

        setSetting('sidebar_cooltopics', val);

        updateBoxes();
    });

    $("#options_sidebar_birthdays").click(function (event) {
        var val = valueFromCheckbox($(this));

        setSetting('sidebar_birthdays', val);

        updateBoxes();
    });

    $("#options_topics").click(function (event) {
        /*
        var val = valueFromCheckbox($(this));
        setSetting('topics', val);
        */

        updateTopics(true);
    });

    $("#options_articles").click(function (event) {
        var val = valueFromCheckbox($(this));

        setSetting('articles', val);

        updateBoxes();
    });

    $("#options_gallery").click(function (event) {
        var val = valueFromCheckbox($(this));

        setSetting('gallery', val);

        updateGallery();
    });

    $("#options_modstuff").click(function (event) {
        var val = valueFromCheckbox($(this));

        setSetting('modstuff', val);

        setTimeout(function () {
            window.location.reload();
        }, 1000);
    });

    $("#options_utolsoreg").change(function (event) {
        var val = $(this).val();

        setSetting('utolsoreg', val);

        updateLastRegTable();
    });

    $("#options_gifnum").change(function (event) {
        var val = $(this).val();

        setSetting('gifnum', val);
    });

    $("#options_chatnum").change(function (event) {
        var val = $(this).val();

        setSetting('chatnum', val);
        limitMessages();
    });
}

console.log('%c Options loaded. ', log_style_done);