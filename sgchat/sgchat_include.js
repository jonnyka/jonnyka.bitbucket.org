var requireQueue = function(modules, callback) {
    function load(queue, results) {
        if (queue.length) {
            require([queue.shift()], function(result) {
                results.push(result);
                load(queue, results);
            });
        } else {
            callback.apply(null, results);
        }
    }

    load(modules, []);
};

var baseUrl = 'https://sg.hu/chat/sgchat/';
//var baseUrl = 'https://skyball.tk/sgcset/sgchat/';
requireQueue([
    baseUrl + 'sgchat_config.js?rand=' + Math.random(),
    baseUrl + 'sgchat_common_functions.js?rand=' + Math.random(),
    baseUrl + 'sgchat_chat_functions.js?rand=' + Math.random(),
    baseUrl + 'sgchat_mod.js?rand=' + Math.random(),
    baseUrl + 'sgchat_options.js?rand=' + Math.random(),
    '//cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.12/jquery.mousewheel.min.js',
    '//cdnjs.cloudflare.com/ajax/libs/jScrollPane/2.0.22/script/jquery.jscrollpane.min.js',
    '//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/basic/jquery.qtip.js',
    '//cdnjs.cloudflare.com/ajax/libs/spectrum/1.3.0/js/spectrum.min.js',
    //'//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js',
    '//cdnjs.cloudflare.com/ajax/libs/jquery.selection/1.0.1/jquery.selection.min.js',
    baseUrl + 'lib/jquery.urlive.js',
    baseUrl + 'changelog.js?rand=' + Math.random()
], function() {
    require([sgNodeUrl + '/socket.io/socket.io.js'], function (io) {
        window.io = io;
        socket = window.io.connect(sgNodeUrl, {secure: true});
        require([baseUrl + 'sgchat_base.js?rand=' + Math.random()], function () {
            jChatLoad();
        });
    });
});