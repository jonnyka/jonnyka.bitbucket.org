console.log('%c Base loading started...', log_style_start);

// Some operations when the user comes back to the chat window
window.onfocus = function () {
    msg_counter = 0;
    focused = true;
    updateTitle();
    checkOnlineStatus();
    updateUser(currentUser, true);
    scrollChat();
};
window.onblur = function () {
    focused = false;
    checkOnlineStatus();
    updateUser(currentUser, false);
};

bindNoScroll(document.getElementById('list-frame'), otherScrollSpeed);

function preventDefault(event) {
    event = event || window.event;
    if (event.preventDefault) {
        event.preventDefault();
    }
    event.returnValue = false;
}

/**
 * Main function.
 */
function jChatLoad() {
    console.log('%c Chat loading started... ', log_style_start);

    try {
        checkSockets();
        var socket = window.socket ? window.socket : window.io.connect(sgNodeUrl, {secure: true});
        checkOnlineStatus();
    } catch (e) {
        showError('Nem sikerült csatlakozni a node szerverhez');
        console.error('socket error', e);
        throw '';
    }

    initServerData();
    identid = initData();
    createNeoSidebar();
    setTimeout(updateStuff, 2000);
    setInterval(updateStuff, 10000);
    checkSockets();
    currentlyWriting();
    createSmileysAndSongs();
    createBBCodes();
    createSgIframe();
    showTime();
    toggleCat(true);
    toggleMonster(true);
    toggleHaloween(true);
    toggleSnow(true);
    toggleCandles(true);
    compactToggle(getSetting('compact'));
    if (getSetting('thread') == 1) {
        createThread();
    }
    if (getSetting('fbbox') == 0) {
        elem('forum-fb-likebox').style.display = 'none';
    }

    /**
     * Socket functions from now on.
     */
    os = getOs();
    userid = elem('userdata').getAttribute('data-uid');
    var toLogin = {
        identid: identid,
        userid: userid,
        os: os
    };

    socket.emit('login', toLogin);
    /*
    setTimeout(function() {
        socket.emit('login', toLogin);
    }, 5000);
    */
    checkOnlineStatus();

    socket.on('newUser', function (user) {
        /*
        if (user == 'Kata' && getSetting('kata') == 1) {
            kataNyuszi();
        }
        */
        userToList(user);
        ABCcset();
        hideMutedUser(user);
    });

    socket.on('userList', function (list) {
        //list.clean();
        //console.log(list);
        if ($userList.innerHTML != '') {
            return;
        }

        for (var key in list) {
            if (list[key]) {
                userToList(list[key]);
            }
        }
        ABCcset();
        hideMutedUsers();
        //$('ul#forum-chat-user-list li').css('animation', 'none');
    });

    socket.on('userLogout', function (userId) {
        --$users[userId];

        if ($users[userId] == 0) {
            var userRow = elem('userlist-user-' + userId);
            userRow.style.display = 'none';
            $userList.removeChild(userRow);
            delete $users[userId];
        }
    });

    socket.on('messageList', function (list) {
        if ($chatList.innerHTML != '') {
            return;
        }

        for (var key in list) {
            msgToList(list[key], true);
        }
        if (getSetting('autoscroll') == 1) {
            scrollToBottom();
        }

        updatePrews();
    });

    socket.on('newMessage', function (msg) {
        msgToList(msg);

        updatePrews();

        if (!focused) {
            msg_counter++;
            updateTitle();
        }
    });

    /*
    socket.on('banned', function (msg) {
        console.log('banned', msg);
    });
    */

    socket.on('colors', function (msg) {
        //console.log(msg);
        settings = msg;

        for (var i = 0; i < elem('.neoh2').length; i++) {
            elem('.neoh2')[i].style.color = settings['Neocortex'];
        }
        for (var i = 0; i < elem('.jonnyh2').length; i++) {
            elem('.jonnyh2')[i].style.color = settings['j0nNyKa'];
        }
        for (var i = 0; i < elem('.pixxelh2').length; i++) {
            elem('.pixxelh2')[0].style.color = settings['pixxel'];
        }

        recolorAllNames();
    });

    socket.on('status', function (err) {
        console.log(err);

        if (err == 'Hiba :(') {
            console.log('Nigger sg hack activated.');
            var socket = window.io.connect(sgNodeUrl, {secure: true/*, 'forceNew': true, 'force new connection': true */});
            identid = initData();
            socket.emit('login', identid);

            sendMessage(getSetting('lastMessage'), socket);
            $textInput.value = '';
            //socket.emit('input', getSetting('lastMessage'));

            socket.emit('request', {'type': 'notWriting', 'data': currentUser});
        }
        else {
            showError(err);
        }
    });

    socket.on('debug', function (msg) {
        var sid = socket.io.engine.id;
        if (!msg.hasOwnProperty(sid)) {
            console.info('debug', msg);
            socket.emit('login', toLogin);
        }
    });

    /**
     * Currently writing functions.
     */
    $textInput.addEventListener('keydown', function (e) {
        if ($textInput.value.length == 0) {
            socket.emit('request', {'type': 'notWriting', 'data': currentUser});
        }
        else {
            socket.emit('request', {'type': 'writing', 'data': currentUser});
        }
        if (e.keyCode == 13) { //enter
            sendMessage(this.value, socket);
            socket.emit('request', {'type': 'notWriting', 'data': currentUser});
        }
        else if (e.keyCode == 38) { // arrow up
            e.preventDefault();
            this.value = getSetting('lastMessage');
        }
        else if (e.keyCode == 40) { // arrow down
            e.preventDefault();
            this.value = '';
        }
        else if (e.keyCode == 27) {
            e.preventDefault();
        }

    }, false);

    $textInput.addEventListener('keyup', function (e) {
        //if (e.keyCode == 8 || e.keyCode == 91) { // backspace
            if ($textInput.value.length == 0) {
                socket.emit('request', {'type': 'notWriting', 'data': currentUser});
            }
        //}
    }, false);

    socket.on('response', function (msg) {
        //console.log(msg);
        var type = msg.type;
        var data = msg.data;

        /*
        if (currentUser == 'j0nNyKa') {
            console.debug('Type & Data', type + ' - ' + data);
        }
        */

        if (type == 'debug' && currentUser == 'j0nNyKa') {
            console.debug('disconnected', data);
        }

        if (type == 'onlineStatus') {
            var username = data.user;
            var status = data.status;
            var real = data.real;
            if (real) {
                updateUser(username, status);
                //console.debug('onlineStatus onSocket', username + ' - ' + status + ' - ' + real);
            }
        }

        if (type == 'newThread') {
            getThreads();
        }

        if (type == 'mp3' && focused && getSetting('autoplay') == 1) {
            player(data);
        }
        if (type == 'notification' && data.toLowerCase().indexOf(currentUser.toLowerCase()) > -1 && !focused && getSetting('autoplay') == 1 && currentUser != " ") {
            player('notification');
        }

        var cur = $('#currents');
        var epp = $('p#eppenGepelnek');
        // epp.addClass('hide');
        epp.attr('hide', 'hide');
        var vanE = $('span.current:contains("' + data +'")');

        if (type == 'writing') {
            //console.log('socket writing');
            if (vanE.length != 1) {
                cur.append('<span class="current">' + data + '</span>');
                if (epp.attr('hide') == 'hide') {
                    epp.fadeTo(500, 1);
                    epp.attr('hide', 'show');
                }
            }

            clearTimeout(clearName);
            clearName = setTimeout(function() {
                if (vanE.length == 1) {
                    vanE.fadeOut(500, function() {
                        $(this).remove();
                        var curLen = $('.current').length;
                        if (curLen == 0) {
                            epp.attr('hide', 'hide');
                            epp.fadeTo(500, 0);
                        }
                    });
                }
            }, clearNameTime);
        }
        else if (type == 'notWriting') {
            //console.log('socket notWriting');
            if (vanE.length == 1) {
                vanE.fadeOut(500, function() {
                    $(this).remove();
                    var curLen = $('.current').length;
                    if (curLen == 0) {
                        epp.attr('hide', 'hide');
                        epp.fadeTo(500, 0);
                    }
                });
            }
        }
    });

    document.getElementById('forum-chat-send-message').addEventListener('click', function () {
        sendMessage($textInput.value, socket);
    });

    socket.on('sended', function () {
        $textInput.value = '';
    });

    updateSgLinks();

    $('.forums-block').wrapAll('<div id="forums-block-wrapper" />');
    $('#forums-block-wrapper').before('<div id="forum-block-wrapper-button">Témák listája</div>');
    $("#forum-block-wrapper-button").click(function (event) {
        updateTopics(true);
    });

    if (getSetting('responsive') == 1) {
        $(window).trigger('resize');
        setResponsive();
    }

    if (getSetting('header') == 0) {
        headerTop.hide();
    }

    if (getSetting('chatshow') == 0) {
        $("#forum-chat").slideToggle();
    }

    if (newSettings > 0) {
        $('<span class="badge">' + newSettings + '</span>').prependTo('#sgcset-header');
    }

    $('h4:contains("Hirdetés")').parent().remove();

    /*
    $("script").each(function () {
        if (this.innerHTML.length > 0) {
            var scriptRegExp = new RegExp("var gaJsHost|var pageTracker|GoogleAnalyticsObject|facebook");
            if (this.innerHTML.match(scriptRegExp) && this.innerHTML.indexOf("innerHTML") == -1) {
                $(this).remove();
            }
        }
    });
    */
   
    console.log('%c Chat loaded.', log_style_done);
}

forumChatInput.focus();
setTimeout(scrollToBottom, 4000);
setTimeout(function() {
    updateUser(currentUser, true);

    //console.debug('alul', focused);
}, 2000);
$('body').scrollTop(0);
setTimeout(checkJquery, 4000);

inputToDiv();
//typeahead();

console.log('%c Base loaded. ', log_style_done);