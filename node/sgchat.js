var SOCKET_IO_PORT_HTTP = 8000;
var SOCKET_IO_PORT_HTTPS = 8000;
var ioClients = [];
var protocol = 'http';

var fs = require('fs');
var http = require('http');
var https = require('https');
var io = require('socket.io');

if (protocol == 'http') {
    var httpServer = http.createServer(function(socket) {
        socket.on('request', function (msg) {
            //console.log('Data received: ' + msg);
            ioClients.forEach(function (ioClient) {
                ioClient.emit('response', msg);
            });
        });
    }).listen(SOCKET_IO_PORT_HTTP);
    var ioServerHttp = io.listen(httpServer);

    ioServerHttp.sockets.on('connection', function (socketIo) {
        //console.log("Client connected");
        ioClients.push(socketIo);
        //console.log("Number of clients: " + ioClients.length);

        socketIo.on('request', function (msg) {
            //console.log('Data received: ' + msg);
            ioClients.forEach(function (ioClient) {
                ioClient.emit('response', msg);
            });
        });

        socketIo.on('disconnect', function() {
            //console.log('Client disconnected');
            var i = ioClients.indexOf(socketIo);
            ioClients.splice(i, 1);
            //console.log("Number of clients: " + ioClients.length);
        });
    });

    httpServer.on('listening', function () {
        console.log("HTTP TCP server accepting connection on port: " + SOCKET_IO_PORT_HTTP);
    });
}
else {
    var httpsOptions = {
        key: fs.readFileSync('./cert/server.key'),
        cert: fs.readFileSync('./cert/server.crt'),
        ca: fs.readFileSync('./cert/ca.crt'),
        requestCert: true,
        rejectUnauthorized: false
    };

    var httpsServer = https.createServer(httpsOptions, function(socket) {
        socket.on('request', function (msg) {
            //console.log('Data received: ' + msg);
            ioClients.forEach(function (ioClient) {
                ioClient.emit('response', msg);
            });
        });
    }).listen(SOCKET_IO_PORT_HTTPS);
    var ioServerHttps = io.listen(httpsServer);

    ioServerHttps.sockets.on('connection', function (socketIo) {
        //console.log("Client connected");
        ioClients.push(socketIo);
        //console.log("Number of clients: " + ioClients.length);

        socketIo.on('request', function (msg) {
            //console.log('Data received: ' + msg);
            ioClients.forEach(function (ioClient) {
                ioClient.emit('response', msg);
            });
        });

        socketIo.on('disconnect', function() {
            //console.log('Client disconnected');
            var i = ioClients.indexOf(socketIo);
            ioClients.splice(i, 1);
            //console.log("Number of clients: " + ioClients.length);
        });
    });

    httpsServer.on('listening', function () {
        console.log("HTTPS TCP server accepting connection on port: " + SOCKET_IO_PORT_HTTPS);
    });
}
