// ==UserScript==
// @include http://sg.hu/forum
// @include http://sg.hu/forum/
// @include https://sg.hu/forum
// @include https://sg.hu/forum/
// ==/UserScript==

window.addEventListener('DOMContentLoaded', function() {
    if (document.getElementById('forum-chat')) {
        opera.postError('chat found');
        if (window.location.protocol != 'http:') {
            location.href = location.href.replace("https://", "http://");
        }

        var baseUrl = 'http://jonnyka.bitbucket.org/sgchat/';
        var socketOnline = true;

        if (socketOnline) {
            require(['http://lilab.net:8000/socket.io/socket.io.js'], function (iol) {
                window.iol = iol;
            });
        }

        require([baseUrl + 'sgchat_include.js?rand=' + Math.random()]);
    }
}, false);
