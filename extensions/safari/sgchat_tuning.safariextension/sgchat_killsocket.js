var loaded = false;
var readyStateCheckInterval = setInterval(function () {
    if (document && !loaded) {
        console.log('--- Socket hack loading started ---');

        document.cookieHack = document.cookie;
        document.cookie = null;
        Object.defineProperty(document, "cookie", {
            writable: true,
            value: 0
        });

        console.log('--- Socket hack loaded ---');
        console.log('--- Requirejs loading started ---');

        var scriptRequire = document.createElement("script");
        scriptRequire.type = "text/javascript";
        scriptRequire.src = safari.extension.baseURI + 'require.js';
        //scriptRequire.async = "async";
        document.getElementsByTagName('head')[0].appendChild(scriptRequire);

        scriptRequire.onload = function () {
            console.log('--- Requirejs loaded ---');
            console.log('--- Init load started ---');

            var scriptInit = document.createElement("script");
            scriptInit.type = "text/javascript";
            scriptInit.src = safari.extension.baseURI + 'sgchat_init.js';
            //scriptInit.async = "async";
            document.getElementsByTagName('head')[0].appendChild(scriptInit);

            scriptInit.onload = function () {
                console.log('--- Init loaded ---');
                clearInterval(readyStateCheckInterval);
            }
        };

        loaded = true;
    }
}, 10);
