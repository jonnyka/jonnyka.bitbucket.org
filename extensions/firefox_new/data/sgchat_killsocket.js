/*
Object.defineProperty(document, 'chatLoad', {
    value: function(){}
});Object.defineProperty(window, 'chatLoad', {
    value: function(){}
});
Object.defineProperty(unsafeWindow, 'chatLoad', {
    value: function(){}
});
*/

var fn = function(){};

unsafeWindow.chatLoad = cloneInto(false, unsafeWindow);
exportFunction(function(){return false;}, unsafeWindow, {defineAs: "chatLoad"});

var foo = createObjectIn(unsafeWindow, {defineAs: "load"});
exportFunction(function(){return false;}, foo, {defineAs: "chatLoad"});



function loadScripts() {
    var scriptRequire = document.createElement("script");
    scriptRequire.type = "text/javascript";
    scriptRequire.src = window.reqjs;
    scriptRequire.async = false;
    document.body.appendChild(scriptRequire);

    var scriptTopic = document.createElement("script");
    scriptTopic.type = "text/javascript";
    scriptTopic.src = window.initjs;
    scriptTopic.async = false;
    document.body.appendChild(scriptTopic);
}

window.addEventListener("load", function(e) { loadScripts(); }, false);