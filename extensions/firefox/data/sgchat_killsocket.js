Object.defineProperty(document, 'chatLoad', {
    value: function(){}
});Object.defineProperty(window, 'chatLoad', {
    value: function(){}
});
Object.defineProperty(unsafeWindow, 'chatLoad', {
    value: function(){}
});

function loadScripts() {
    var scriptRequire = document.createElement("script");
    scriptRequire.type = "text/javascript";
    scriptRequire.src = window.reqjs;
    scriptRequire.async = false;
    document.body.appendChild(scriptRequire);

    var scriptTopic = document.createElement("script");
    scriptTopic.type = "text/javascript";
    scriptTopic.src = window.initjs;
    scriptTopic.async = false;
    document.body.appendChild(scriptTopic);
}

window.addEventListener("load", function(e) { loadScripts(); }, false);