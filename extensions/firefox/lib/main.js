var pageMod = require("sdk/page-mod");
var data = require("sdk/self").data;

pageMod.PageMod(
    {
        include: [
            "http://www.sg.hu/*",
            "http://sg.hu/*",
            "https://www.sg.hu/*",
            "https://sg.hu/*"
        ],
        contentScriptWhen: 'start',
        contentScriptFile: [data.url("sgchat_killsocket.js")],
        contentScript: 'document.reqjs = window.reqjs = "' + data.url("require.js") + '"; document.initjs = window.initjs = "' + data.url("sgchat_init.js") + '";'
    }
);