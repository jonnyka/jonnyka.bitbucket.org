if (window.location.href === 'https://sg.hu/forum/' && window.location.pathname === '/forum/') {
    var rt = window.external.mxGetRuntime();

    Object.defineProperty(window, 'chatLoad', {
        value: function(){},
        configurable: true
    });

    window.addEventListener("load", myMain, false);

    function myMain(evt) {
        var jsInitChecktimer = setInterval (checkForJS_Finish, 111);

        function checkForJS_Finish () {
            if ($('#userdata').length) {
                clearInterval (jsInitChecktimer);

                if ($('#userdata').data('uid')) {
                    var baseUrl = 'https://sg.hu/chat/sgchat/';
                    require([baseUrl + 'sgchat_include_maxthon.js?rand=' + Math.random()]);
                }
            }
        }
    }
}
